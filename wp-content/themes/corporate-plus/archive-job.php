<?php
/**
 * The template for displaying archive pages.
 *
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
get_header();
global $corporate_plus_customizer_all_values;
$counter = 1;

$cat = get_the_category();
$parentCatName = get_cat_name($cat[0]->parent);

?>
    <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>
    <div class="container-fluid padding50">

        <div class=" col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <div class="">
                <div class="left-widget">
                    <?php
                    $postType = get_post_type_object(get_post_type());
                    if ($parentCatName == 'articles' || $postType->label === "Posts") { ?>
                        <?php if (is_active_sidebar('sidebar-22')) : ?>
                            <div id="secondary" class="widget-area" role="complementary">
                                <?php dynamic_sidebar('sidebar-22'); ?>
                            </div>
                        <?php endif; ?>
                    <?php }
                    if ($parentCatName == 'ebooks') {
                        ?>
                        <?php if (is_active_sidebar('sidebar-4')) : ?>
                            <div id="secondary" class="widget-area" role="complementary">
                                <?php dynamic_sidebar('sidebar-4'); ?>
                            </div>
                        <?php endif;
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="article-area col-lg-8 col-md-8 col-sm-8 col-xs-12">

            <div id="" class="content-area tagged-posts">
                <?php
                if (have_posts()) { ?>
                    <?php
                    /* Start the Loop */
                    while (have_posts()) {
                        the_post();
                        ?>
                        <?php if ($counter % 2 != 0) { ?>
                            <div class="front_page_article">
<!--                                archive-->
                                <article
                                    id="<?php echo $counter; ?>" <?php post_class('init-animate fadeInDown animated'); ?>>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            echo '<figure>';
                                            the_post_thumbnail($thumbnail);
                                            echo "</figure>";
                                        }

                                        ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                        <div class="content-wrapper">
                                            <header class="entry-header">
                                                <?php the_title(sprintf('<h2 class="entry-title text-center"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>

                                                <?php
                                                if ('post' === get_post_type()) : ?>
                                                    <div class="entry-meta">
                                                        <?php corporate_plus_posted_on(); ?>
                                                    </div><!-- .entry-meta -->
                                                    <?php
                                                endif; ?>
                                            </header>
                                            <!-- .entry-header -->

                                            <div class="entry-content">
                                                <?php
                                                the_excerpt();
                                                ?>
                                                <a class="read-more"
                                                   href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
                                                <?php
                                                wp_link_pages(array(
                                                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'corporate-plus'),
                                                    'after' => '</div>',
                                                ));
                                                ?>
                                            </div>
                                            <!-- .entry-content -->
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </article>
                                <!-- #post-## -->
                            </div>
                        <?php } else { ?>
                            <div class="front_page_article">
                                <article
                                    id="<?php echo $counter; ?>" <?php post_class('init-animate fadeInDown animated'); ?>>

                                    <div
                                        class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            echo '<figure>';
                                            the_post_thumbnail($thumbnail);
                                            echo "</figure>";
                                        }

                                        ?>
                                    </div>
                                    <div
                                        class="col-lg-6 col-lg-pull-6 col-md-pull-6 col-md-6  col-sm-6 col-sm-pull-6 col-xs-12">
                                        <div class="content-wrapper">
                                            <header class="entry-header">
                                                <?php the_title(sprintf('<h2 class="entry-title text-center"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>

                                                <?php
                                                if ('post' === get_post_type()) : ?>
                                                    <div class="entry-meta">
                                                        <?php corporate_plus_posted_on(); ?>
                                                    </div><!-- .entry-meta -->
                                                    <?php
                                                endif; ?>
                                            </header>
                                            <!-- .entry-header -->

                                            <div class="entry-content">
                                                <?php
                                                the_excerpt();
                                                ?>
                                                <a class="read-more"
                                                   href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
                                                <?php
                                                wp_link_pages(array(
                                                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'corporate-plus'),
                                                    'after' => '</div>',
                                                ));
                                                ?>
                                            </div>
                                            <!-- .entry-content -->
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </article>
                                <!-- #post-## -->
                            </div>
                        <?php }
                        $counter++;
                        ?>
                        <?php
                    }

                } else {

                    get_template_part('template-parts/content', 'none');

                } ?>
                <?php the_posts_pagination(array('mid_size' => 2)); ?>

            </div>
            <!-- #primary -->

        </div>
        <!-- #content -->
    </div>
<?php get_footer(); ?>