<?php /*Template Name: Tech Talent Template*/?>

<!doctype html>
<html lang="en">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        @media screen and (max-width: 500px) {
            p {
                font-size: 12px !important;
            }

            #unsubscribe {
                margin-right: 10px !important;
            }

            #table-content tr td {
                padding-left: 10px !important;
                padding-right: 10px !important;
            }

            a:-webkit-any-link {
                font-size: 12px;
            }
        }

        @media screen and (max-width: 799px) {
            #x_parent-table {
                width: 100% !important;
            }

            #table-content, #image-section {
                width: 90% !important;
                height: auto !important;
            }
        }
    </style>
</head>
<body style="margin: 0">
<div style="font-family:'Open Sans',Helvetica,Arial,sans-serif!important">
    <table align="center" bgcolor="#dbdada" cellspacing="0" id="x_parent-table" width="800">
        <tbody>
        <tr>
            <td style="border: 0 none!important;">
                <table align="center" bgcolor="#6e6b6b" id="x_header-table"
                       style="border-spacing:0; font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important"
                       width="100%">
                    <tbody>
                    <tr>
                        <td align="right" height="20" style="border: 0 none!important; padding:7px;">
                            &nbsp;
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding:20px 0; border: 0 none!important;"><span class="sg-image"
                                                                                       data-imagelibrary="%7B%22width%22%3A%22261%22%2C%22height%22%3A%2242%22%2C%22alignment%22%3A%22center%22%2C%22src%22%3A%22https%3A//develandoo.s3.amazonaws.com/email/develandoo-logo.png%22%2C%22alt_text%22%3A%22develandoo%20logo%22%2C%22link%22%3A%22https%3A//develandoo.com%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"
                                                                                       style="float: none; display: block; text-align: center;"><a
                    href="https://develandoo.com"><img alt="develandoo logo" height="42"
                                                       src="https://develandoo.s3.amazonaws.com/email/develandoo-logo.png"
                                                       style="width: 261px; height: 42px;" width="261"/></a></span></td>
        </tr>
        <tr>
            <td align="center">
                <span class="sg-image"
                      data-imagelibrary="%7B%22width%22%3A%22700%22%2C%22height%22%3A%22388%22%2C%22alt_text%22%3A%22develandoo%22%2C%22alignment%22%3A%22center%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//s3.eu-central-1.amazonaws.com/develandoo-img/email_templ.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"
                      style="float: none; display: block; text-align: center;">
                <img alt="develandoo"
                     height="388"
                     id="image-section"
                     src="https://s3.eu-central-1.amazonaws.com/develandoo-img/email_templ.png"
                     style="width: 700px; height: 388px;"
                     width="700"/></span>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding:25px 0 20px; border: 0 none!important;">
                <table align="center" id="table-content" bgcolor="#ffffff" cellpadding="0"
                       style="font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important;"
                       width="700">
                    <tbody>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">The ideal case for a startup is to have full-time
                                employees who work on the project from the beginning and are accountable for it. But
                                where do you find these people?</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">
                                First, you can hire an in-house recruiting team. While that is a good affordable option,
                                the market is overcrowded with external recruiting agencies who day and night scout
                                Linkedin, Xing, and other professional networks. This creates a fierce competition over
                                talent.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">
                                External recruiters can also be a source of talent but they also face the same problem
                                of a competitive market with a high demand for talent. And when you do find talent,
                                external agencies often charge up to 25% of the employee&rsquo;s salary which makes this
                                route not very cost effective. In Germany, an average recruiter will make 20,000 EURO on
                                a one-time payment for placing a candidate.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">
                                Multiply that by 5 which is your average development team size and you just spent
                                100,000 EURO just to hire a team that may end up leaving 6-12 months later anyway for a
                                better offer.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px; border: 0 none!important;">
                            <p style="margin:0; color:#636363">
                                Or even worse, that employee leaves your company to work as a contractor...
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr align="center">
            <td style="padding-bottom:14px; border: 0 none!important;">
                <table bgcolor="#f2cd32" style="border-radius:5px" width="160">
                    <tbody>
                    <tr align="center">
                        <td style="border: 0 none!important;"><a
                                href="https://develandoo.rocks/article/the-state-of-tech-talent-in-central-and-eastern-europe"
                                style="color:#fff; text-decoration:none!important; line-height:28px"
                                target="_blank">FIND OUT MORE</a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border: 0 none!important;">
                <table align="center" bgcolor="#6e6b6b" height="30"
                       style="border-spacing:0 10px; font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important;"
                       width="100%">
                    <tbody>
                    <tr>
                        <td align="right" style="color:#dbdada; border: 0 none!important;" width="100%"><a
                                href="[unsubscribe]" id="unsubscribe"
                                style="text-decoration: underline!important; color: #b8b8b8!important;font-size:12px;margin-right:20px;">Click
                            here if you don&#39;t want to receive emails from us </a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
