<?php
/*Template Name: Forum page Template*/

get_header();

?>
<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>
<div class="container-fluid padding50">
    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <div class="row">

        </div>
    </div>
    <div class="article-area col-lg-9 col-md-9 col-lg-offset-1 col-md-offset-1 col-sm-10 col-xs-12">
        <h1 class="page_title"> <?php the_title(); ?></h1>

        <?php
        $tags = get_tags();
        $html = '<div class="post_tags">';
        foreach ($tags as $tag) {
            $tag_link = get_tag_link($tag->term_id);

            $html .= "<a href='{$tag_link}' title='{$tag->name}' class='{$tag->slug}'>";
            $html .= "{$tag->name}</a>"; ?>
        <?php }
        $html .= '</div>';
        echo $html;
        ?>

        <ul>
            <?php
            $posts = get_posts('numberposts=-1&term_id=' . $tags->name);
            foreach ($posts as $post) {
                ?>
                <li>
                    <div class="images_releted">
                        <a class="post_thumbnail" href="<? the_permalink() ?>" rel="bookmark"
                           title="<?php the_title(); ?>">
                            <?php echo the_post_thumbnail('thumbnail') ?>
                        </a>
                    </div>
                    <a class="post_thumbnail_link" href="<? the_permalink(); ?>" rel="bookmark"
                       title="<?php the_title(); ?>">
                        <?php the_title(); ?></a>
                </li>

            <?php } ?>
        </ul>

    </div>
</div><!-- .content-area -->

<?php get_footer(); ?>
