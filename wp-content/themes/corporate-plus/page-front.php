<?php
/*Template Name: Front page Template*/

get_header();

?>
<?php
$args = array('numberposts' => '1');
$recent_posts = wp_get_recent_posts($args);
foreach ($recent_posts as $recent){

?>

<div class="container-fluid yellow_space">
    <div class="container padding0">
        <h2 class="text-center top-text padding0">HOT STORY:
            <a href="<?php echo $recent["guid"]; ?>">
                <small><?php echo $recent["post_title"]; ?></small>
            </a>
            <?php }
            wp_reset_query(); ?>
        </h2>
    </div>
</div>
<div class="container m-top50">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <div class="shadowed">
                <h3 class="col-lg-4 col-md-4 hidden-sm hidden-xs">WATCH NOW</h3>
                <?php
                $arg = array(
                    'post_type' => 'video',
                    'posts_per_page' => 1
                );
                $obituar_query = new WP_Query($arg);
                while ($obituar_query->have_posts()) :
                    $obituar_query->the_post(); ?>

                    <h3 class="col-lg-8 col-md-8 hidden-sm hidden-xs viseos_title">
                        <div class="row"><?php the_title(); ?></div>
                    </h3>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30">
                        <iframe width="100%" height="275" src="
                            <?php echo get_post_meta(get_the_ID(), 'url', true); ?>"
                                frameborder="0"></iframe>
                    </div>
                    <h3 class="hidden-lg hidden-md col-sm-4 col-xs-4 font-mobile">WATCH NOW</h3>
                    <h3 class="hidden-lg hidden-md col-sm-8 col-xs-8 viseos_title font-mobile">
                        <div class="row"><?php the_title(); ?>
                        </div>
                    </h3>
                <?php endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="col-lg-7 col-md-7 hidden-sm hidden-xs">
                <h3>FEATURED ARTICLES</h3>
                <?php
                $args = array('posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date');
                $postslist = get_posts($args);
                foreach ($postslist as $post) :
//                setup_postdata($post);
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row">
                                  <a href="<?php echo $post->guid; ?>">
                                    <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>"
                                         class="img-responsive center-block featured-thumb" alt=""/>
                                         </a>
                                </div>
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <div class="text_part fleft">
                                    <h3 class="art_title"><a
                                            href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a></h3>
                                    <?php $authr = get_the_author();
                                    if ($authr != "Develandoo Admin") {
                                        ?>
                                        <span class="dates">by <?php echo get_the_author(); ?></span>
                                    <?php } ?>
                                    <p class="dates text-right">
                                        <?php $the_date = mysql2date(get_option('date_format'), $post->post_date);
                                        echo $the_date; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                endforeach;
                wp_reset_postdata();
                ?>

            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" id="books">
                <h3>BOOKS TO READ</h3>
                <?php $args1 = array(
                    'post_type' => 'ebook',
                    'posts_per_page' => 3
                );
                $obituary_query = new WP_Query($args1);
                // Loop through the obituaries:
                while ($obituary_query->have_posts()) : $obituary_query->the_post(); ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="">
                                   <a href="/ebooks"> <?php the_post_thumbnail(array(200, 250)); ?></a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <div class="row">
                                    <h3 class="art_title">
                                        <a href="/ebooks">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid yellow_space">
    <div class="container">
        <h2 class="top-text col-lg-12 col-md-12 col-xs-12">LATEST ARTICLES</h2>
    </div>
</div>

<div id="" class="container">
    <div class="row">
        <div class="article-area col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30 min-padding0">
            <?php
            $args = array('posts_per_page' => 6, 'order' => 'DESC', 'orderby' => 'date');
            $postslist = get_posts($args);
            foreach ($postslist as $post) :
                setup_postdata($post);

                ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 shadowed">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                        <a href="<?php echo $post->guid; ?>">
                            <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>"
                                 class="img-responsive center-block img-fixed-height" width="500" alt=""/>
                                 </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top10">
                        <h2 class="text-center trimed"><a
                                href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a>
                        </h2>
                        <div class="row m-top30">
                            <div class="text_exc m-bot60">
                                <?php
                                the_excerpt(); ?>
                                <p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
<div class="text-center col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a href="/category/article" class="center-block more_btn"> MORE ARTICLES </a>
</div>
<div class="container-fluid yellow_space hidden-sm hidden-xs">

</div>

<?php get_footer(); ?>
