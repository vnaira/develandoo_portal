<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AcmeThemes
 * @subpackage AcmeBlog
 */

/**
 * corporate_plus_action_before_head hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked corporate_plus_set_global -  0
 * @hooked corporate_plus_doctype -  10
 */
do_action( 'corporate_plus_action_before_head' );?>
	<head>
		<?php
		/**
		 * corporate_plus_action_before_wp_head hook
		 * @since Corporate Plus 1.0.0
		 *
		 * @hooked corporate_plus_before_wp_head -  10
		 */
		do_action( 'corporate_plus_action_before_wp_head' );

		wp_head();
		?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=385864221488229";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>

		<script src="https://apis.google.com/js/platform.js" async defer>
			{lang: 'ru'}
		</script>


        <script type='application/ld+json'>
       [{"@context": "http://schema.org",
            "@type": "LocalBusiness",
       "name": "Devalandoo",
       "url": "https://develandoo.com",
       "sameAs": [
         "https://plus.google.com/+DevelandooLLC",
         "https://www.linkedin.com/company-beta/6650958/\",",
         "https://www.facebook.com/develandoo/"
       ],
       "logo": "https://develandoo.s3.amazonaws.com/images/pressed-logo.png",
       "image": "https://develandoo.s3.amazonaws.com/images/pressed-logo.png",
       "description": "Our mission is to help you build an incredible company.\nAt Develandoo we are changing the way that companies build software products. We do this by partnering with entrepreneurs to help them build products in two unique ways.\nA lead engineer works on-site from your office\nWhile most software teams only work remotely, we do a mix of onsite and remote talent. A lead developer works from your office and is the primary point of contact with your remote development team.\n\nYou can hire your team full time \nAfter your MVP is complete, you can hire your team as full-time employees. You can relocate all team members to your office or open up subsidiaries in the countries we operate in. \n\nDevelandoo’s goal is to help you build your product, and your company, in a way that benefits you",
       "telephone": "+1 818 6499409",
       "address": {
         "@type": "PostalAddress",
         "addressLocality": "Los Angeles, USA",
         "addressRegion": "California",
         "streetAddress": "Thomson Ave, Glendale",
         "postalCode": ""
       },
       "openingHours": "Mo, Tu, We, Th, Fr 10:00-19:00"},
       {
         "@context": "http://www.schema.org",
         "@type": "person",
         "name": "Albert Stepanyan",
         "additionalName": "Cyberhulk",
         "jobTitle": "CEO",
         "url": "http://cyberhulk.net/",
         "address": {
           "@type": "PostalAddress",
           "streetAddress": "1022 Thompson Ave",
           "addressLocality": "Glendale, CA 91201",
           "addressRegion": "California"
         },
         "email": "albert@develandoo.com"
       },
       {
          "@context": "http://schema.org",
         "@type": "Person",
         "name": "Aram Harutyunyan",
         "jobTitle": "Managing Director",
         "url": "https://www.linkedin.com/in/aram-harutyunyan-b58a7232/",
         "address": {
           "@type": "PostalAddress",
           "streetAddress": "40, Sayat Nova ave.",
           "addressLocality": "Yerevan",
           "addressCountry": "Armenia"
         }
       },
       {
          "@context": "http://schema.org",
          "@type": "Person",
          "name": "Jake Jorgovan",
          "jobTitle": "Creative Strategist",
          "url": "https://jake-jorgovan.com/",
          "address": {
           "@type": "PostalAddress",
           "addressRegion": "Greater Denver Area"
          }
        }]
         </script>

	</head>
<body <?php body_class();?>>

<?php
/**
 * corporate_plus_action_before hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked corporate_plus_site_start - 20
 */
do_action( 'corporate_plus_action_before' );

/**
 * corporate_plus_action_before_header hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked corporate_plus_skip_to_content - 10
 */
do_action( 'corporate_plus_action_before_header' );


/**
 * corporate_plus_action_header hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked corporate_plus_header - 10
 */
do_action( 'corporate_plus_action_header' );


/**
 * corporate_plus_action_after_header hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked null
 */
do_action( 'corporate_plus_action_after_header' );


/**
 * corporate_plus_action_before_content hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked null
 */
do_action( 'corporate_plus_action_before_content' );