<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */


/**
 * corporate_plus_action_after_content hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked null
 */
//do_action( 'corporate_plus_action_after_content' );

/**
 * corporate_plus_action_before_footer hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked null
 */
do_action( 'corporate_plus_action_before_footer' );

/**
 * corporate_plus_action_footer hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked corporate_plus_footer - 10
 */
do_action( 'corporate_plus_action_footer' );

/**
 * corporate_plus_action_after_footer hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked null
 */
do_action( 'corporate_plus_action_after_footer' );

/**
 * corporate_plus_action_after hook
 * @since Corporate Plus 1.0.0
 *
 * @hooked corporate_plus_page_end - 10
 */
do_action( 'corporate_plus_action_after' );
?>
<?php wp_footer(); ?>

<div id="stock-mini-window">
    <button class="close-stock" type="button"><span aria-hidden="true">&times;</span></button>
    <div class="content-secondary">
        <div class="content-header">
            <p>High Five! You just read 2 awesome articles, in row. You may want to subscribe to our blog newsletter for new blog posts.</p>
        </div>
        <div class="content-footer">
            <form name="stock-win" method="post" id="stock-form">
                <input type="email" name="email" placeholder="Email Address" id="stock-email">
                <button type="submit"> Submit</button>
                <label id="stock-email-error" class="error" for="stock-email"></label>
                <div id="server-res"></div>
            </form>
        </div>
    </div>
    <div class="promo-success">

    </div>
</div>

</body>
</html>