<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
get_header();
global $corporate_plus_customizer_all_values;
?>
<div class="wrapper inner-main-title init-animate fadeInDown animated">
    <header>
        <h1 class="page-title"><?php esc_html_e('Oops! That page can&rsquo;t be found.', 'corporate-plus'); ?></h1>
    </header>
</div>
<div id="content" class="center-block">
    <?php
    if (1 == $corporate_plus_customizer_all_values['corporate-plus-show-breadcrumb']) {
        corporate_plus_breadcrumbs();
    }
    ?>


    <div class="not-found">
        <div class="col-lg-8 col-md-8 col-lg-offset-2 page_not_found col-md-8 col-sm-8 col-xs-12 col-sm-offset-2">
            <h1 class="text-left col-lg-12 col-md-12 col-sm-12 col-xs-21">
                <div class="">404
                </div>
            </h1>
            <h2 class="text-right col-lg-6 col-md-6 col-sm-6 col-xs-12">oops</h2>

            <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Page cannot be found</h3>

            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12">The page you are looking for is temporarily
                unavailable</p>
        </div>
    </div>

</div><!-- #content -->
<?php get_footer(); ?>
