<?php
/*Template Name: Ebooks page Template*/

get_header();

?>
<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>
<div class="container-fluid padding50">
    <div class=" col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="">
            <div class="left-widget">
                <?php if (is_active_sidebar('sidebar-4')) : ?>
                    <div id="secondary" class="widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar-4'); ?>
                    </div>
                <?php endif; ?>
            </div>

        </div>
    </div>
    <div class="article-area col-lg-8 col-md-8 col-sm-8 col-xs-12">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <?php
                if (have_posts()) { ?>
                    <?php
                    /* Start the Loop */
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 book_item">
                                <div class="row">

                                    <h3 class="book_t"><?php the_title(); ?></h3>
                                    <figure class="book_thumb">
                                        <?php  the_post_thumbnail( array(200,300) ); ?>
                                    </figure>
                                    <p class="book_descr"><?php echo get_the_excerpt($item->item_id); ?></p>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                        <div class="row">
                                            <a class="download_link"
                                               href="<?php echo get_post_meta(get_the_ID(), 'url_pdf', true); ?>"
                                               download="<?php the_title(); ?>">Download PDF</a>
                                            <a class="download_link"
                                               href="<?php echo get_post_meta(get_the_ID(), 'url_doc', true); ?>"
                                               download="<?php the_title(); ?>">Download Doc</a>
<!--                                            <form method="post" class="download_link_ col-lg-6"-->
<!--                                                  action="--><?php //do_add_count(); ?><!--">-->
<!--                                                <div class="row">-->
<!--                                                    <input type="hidden" name="user_id"-->
<!--                                                           value="--><?php //echo um_user('ID'); ?><!--">-->
<!--                                                    <input type="hidden" name="item_id" value="--><?php //the_ID(); ?><!--">-->
<!--                                                    --><?php //echo get_post_meta(get_the_ID(), 'url', true); ?>
<!--                                                    <input type="hidden" name="directory"-->
<!--                                                           value="--><?php //echo get_post_meta(get_the_ID(), 'url_pdf', true); ?><!--">-->
<!--                                                    <input name="go" type="submit" class="" value="Download PDF"/>-->
<!--                                                </div>-->
<!--                                            </form>-->
<!--                                            <form method="post" class="download_link_ col-lg-6"-->
<!--                                                  action="--><?php //do_add_count(); ?><!--">-->
<!--                                                <div class="row">-->
<!--                                                    <input type="hidden" name="user_id"-->
<!--                                                           value="--><?php //echo um_user('ID'); ?><!--">-->
<!--                                                    <input type="hidden" name="item_id" value="--><?php //the_ID(); ?><!--">-->
<!---->
<!--                                                    <input type="hidden" name="directory"-->
<!--                                                           value="--><?php //echo get_post_meta(get_the_ID(), 'url_doc', true); ?><!--">-->
<!--                                                    <input name="go" type="submit" class="" value="Download Doc"/>-->
<!--                                                </div>-->
<!--                                            </form>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }
                }
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>
</div><!-- .content-area -->

<?php get_footer(); ?>
