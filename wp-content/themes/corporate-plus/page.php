<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
global $corporate_plus_customizer_all_values;
get_header(); ?>

<?php
if ( get_the_title() === "Forum" ) {
?>
<div class="col-lg-12 col-md-12 col-sm-12 forum_header">
	<div class="center-block ftitle">
		<h3 class="text-center"> <?php the_title(); ?></h3>
		<h3 class="text-center">
			<small id="form_small">What are you looking for?</small>
		</h3>
		<div class="container">
			<div class="wpf-search_ center-block text-center">

				<form action="<?php echo WPFORO_BASE_URL ?>" method="get">
					<i class="fa fa-search" style="margin-top:32px;margin-left:12px;position: absolute;color:#9a9a9a"></i>
					<input class="wpf-search-field" id="forum_search" name="wpfs" placeholder="search the knowledge base.." type="text" value=""/>
				</form>

			</div>

		</div>
	</div>
</div>

<div class="container-fluid padding50">
	<div id="content" class="forum_page">
		<?php } else {
		?>

		<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>

		<div class="container-fluid padding50">

			<div id="content" class="container">
				<?php }
				?>
				<div id="" class="content-area">
					<!--            --><?php //echo get_the_title() ?>
					<main id="main" class="site-main" role="main">
						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'pages' );

							// If comments are open or we have at least one comment, load up the comment template.


						endwhile; // End of the loop.
						?>

					</main>
					<!-- #main -->
				</div>

			</div>
		</div>
		<?php get_sidebar( 'left' ); ?>

	</div><!-- #content -->
	<?php get_footer(); ?>
