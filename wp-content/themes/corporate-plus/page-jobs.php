<?php
/*Template Name: Ebooks page Template*/

get_header();

?>
<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>
<div class="container-fluid padding50">
    <div class=" col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="">
            <div class="left-widget">
                <?php if (is_active_sidebar('sidebar-20')) : ?>
                    <div id="secondary" class="widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar-20'); ?>
                    </div>
                <?php endif; ?>

            </div>

        </div>
    </div>
    <div class="article-area col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <h1 class="page_title col-lg-12 col-md-12"> <?php the_title(); ?></h1>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top_space">
            <div class="row">
                <?php
                $args1 = array(
                    'post_type' => 'job',
                    'posts_per_page' => -1
                );

                $obituary_query = new WP_Query($args1);

                while ($obituary_query->have_posts()) : $obituary_query->the_post(); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 job-itm">
                        <div class="case_icon center-block"></div>
                        <h3 class="text-center"><?php the_title(); ?></h3>
                        <?php
                        if (has_post_thumbnail()) {
                            echo '<figure class="book_thumb">';
                            the_post_thumbnail($thumbnail);
                            echo "</figure>";
                        }
                        ?>
                        <div class="job_item col-lg-12 col-md-12">

                            <p class="job_descr"><?php the_content(); ?></p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
                            <a class="apply_link pull-left"
                               href="<?php echo get_post_meta(get_the_ID(), 'url', true); ?>" target="_blank">Apply for
                                job</a>
                        </div>
                    </div>

                <?php endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>
<!-- .content-area -->

<?php get_footer(); ?>
