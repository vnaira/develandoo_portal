<?php
/**
 * The template for displaying archive pages.
 *
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
get_header();
global $corporate_plus_customizer_all_values;
$counter = 1;

$cat = get_the_category();
$parentCatName = get_cat_name($cat[0]->parent);


$args = array('numberposts' => '1');
$recent_posts = wp_get_recent_posts($args);
foreach ($recent_posts as $recent) {

    ?>
    <div class="container-fluid yellow_space">
    <div class="container">
    <h2 class="text-center top-text m-left5">HOT STORY:
    <small><?php echo $recent["post_title"]; ?></small>
<?php }
wp_reset_query(); ?>
    </h2>
    </div>
    </div>
    <div class="container m-top50">
        <div class="">
            <div class=" col-lg-6 col-md-7 col-sm-12 col-xs-12">
                <?php if (is_active_sidebar('sidebar-22')) : ?>
                <div id="secondary" class="widget-area top-dropdown text-uppercase" role="complementary">
                    <?php dynamic_sidebar('sidebar-22'); ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('sidebar-19')) : ?>
            <div id="second" class="widget-area top-dropdown text-uppercase" role="complementary">
                <?php dynamic_sidebar('sidebar-19'); ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    </div>
    </div>
    <div class="container m-top50">
        <div class="">
            <?php $args = array('numberposts' => 1);
            $postslist = get_posts($args);
            foreach ($postslist as $post) :
                setup_postdata($post);
                ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php the_permalink(); ?>">
                    <figure class="top-thumb center-block">
                        <?php
                        the_post_thumbnail(array(200, 400)); ?>
                    </figure>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h2 class="font20bold"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>

                <?php $authr = get_the_author();
                if ($authr != "Develandoo Admin") {
                    ?>
                  <a href="<?php echo get_post_meta(get_the_ID(), 'author_profile', true); ?>" target="_blank" class="dates font14 title-wout">  by <?php echo get_the_author(); ?></a>
                <?php } ?>

                    <div class="text_exc_top m-top50 m-bot40">
                        <?php the_excerpt(); ?>
                    </div>
                    <a href="<?php the_permalink(); ?>">READ THE ARTICLE</a>
                </div>
                <?php
            endforeach;
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="container-fluid yellow_space">
        <div class="container">
            <h2 class="top-text col-lg-12 col-md-12 col-xs-12">LATEST ARTICLES</h2>
        </div>
    </div>

    <div id="" class="container">
        <div class="row">
            <div class="article-area col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30 min-padding0">
                <?php
                if (have_posts()) { ?>
                    <?php
                    /* Start the Loop */
                    while (have_posts()) {
                        the_post();
                        ?>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 shadowed">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>"
                                         class="img-responsive center-block img-fixed-height" width="500" alt=""/>
                                         </a>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top10">
                                <h2 class="text-center"><a
                                        href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a>
                                </h2>
                                <div class="row m-top30">
                                    <div class="text_exc m-bot60">
                                        <?php
                                        the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                } else {

                    get_template_part('template-parts/content', 'none');

                } ?>
                <?php the_posts_pagination(array('mid_size' => 2)); ?>

            </div>
            <!-- #primary -->

        </div>
        <!-- #content -->

    </div>
<?php get_footer(); ?>