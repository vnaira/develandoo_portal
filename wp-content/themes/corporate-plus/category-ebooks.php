<?php
/*Template Name: Article Template*/

get_header();

?>
<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>
<div class="container-fluid padding50">
	<div class=" col-lg-3 col-md-3 col-sm-4 col-xs-12">eboooks category
		<div class="">
			<div class="left-widget">

				<?php if (is_active_sidebar('sidebar-22')) : ?>
					<div id="secondary" class="widget-area" role="complementary">
						<?php dynamic_sidebar('sidebar-22'); ?>
					</div>
				<?php endif; ?>

			</div>

		</div>
	</div>
	<div class="article-area col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<div class="page-filter col-lg-12 col-md-12 col-sm-12 col-xs-12" style="max-width:100%">
			<h1 class="page_title"> <?php the_title(); ?></h1>

		</div>
		<div id="response" class="col-lg-12 col-md-12 col-xs-12 response">
			<?php
			$args = array('posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date');
			$postslist = get_posts($args);
			foreach ($postslist as $post) :
				setup_postdata($post);

				?>
				<div class="front_article">

					<div class="col-lg-1 col-md-2 col-sm-2 col-xs-2">
						<div class="row">
							<div class="avatar">
								<?php echo get_avatar(get_the_author_email(), '80', 'assets/img/no_images.jpg', get_the_author()); ?>
							</div>
						</div>
					</div>

					<div class="col-lg-11 col-md-10 col-sm-10 col-xs-10">
						<div class="text_part fleft">
							<h2><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>
							<!--                            <a href="--><?php //echo um_get_core_page('user'); ?><!--">-->
							<span style="color: #ccc"><?php echo get_the_author(); ?></span>
							<!--                            </a>-->

							<div class="text_exc">
								<?php the_excerpt();?>
								<!--                                --><?php //echo $post->post_excerpt;?>
								<?php $the_date = mysql2date(get_option('date_format'), $post->post_date);
								echo $the_date; ?></p>
								<a class="read-more pull-right"
								   href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
							</div>
						</div>
					</div>
				</div>

				<?php
			endforeach;
			wp_reset_postdata();
			?>

		</div>
	</div>
</div><!-- .content-area -->

<?php get_footer(); ?>
