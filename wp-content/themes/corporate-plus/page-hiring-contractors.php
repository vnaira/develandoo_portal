<?php /*Template Name: Hiring contractors */?>

<!doctype html>
<html lang="en">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        @media screen and (max-width: 500px) {
            p {
                font-size: 12px !important;
            }

            #unsubscribe {
                margin-right: 10px !important;
            }

            #table-content tr td {
                padding-left: 10px !important;
                padding-right: 10px !important;
            }

            a:-webkit-any-link {
                font-size: 12px;
            }
        }

        @media screen and (max-width: 799px) {
            #x_parent-table {
                width: 100% !important;
            }

            #table-content, #image-section {
                width: 90% !important;
                height: auto !important;
            }
        }
    </style>
</head>
<body style="margin: 0">
<div style="font-family:'Open Sans',Helvetica,Arial,sans-serif!important">
    <table align="center" bgcolor="#dbdada" cellspacing="0" id="x_parent-table" width="800">
        <tbody>
        <tr>
            <td style="border: 0 none!important;">
                <table align="center" bgcolor="#6e6b6b" id="x_header-table"
                       style="border-spacing:0; font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important"
                       width="100%">
                    <tbody>
                    <tr>
                        <td align="right" height="20" style="border: 0 none!important; padding:7px;">
                            &nbsp;
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding:20px 0; border: 0 none!important;"><span class="sg-image"
                                                                                       data-imagelibrary="%7B%22width%22%3A%22261%22%2C%22height%22%3A%2242%22%2C%22alignment%22%3A%22center%22%2C%22src%22%3A%22https%3A//develandoo.s3.amazonaws.com/email/develandoo-logo.png%22%2C%22alt_text%22%3A%22develandoo%20logo%22%2C%22link%22%3A%22https%3A//develandoo.com%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"
                                                                                       style="float: none; display: block; text-align: center;"><a
                        href="https://develandoo.com"><img alt="develandoo logo" height="42"
                                                           src="https://develandoo.s3.amazonaws.com/email/develandoo-logo.png"
                                                           style="width: 261px; height: 42px;" width="261"/></a></span></td>
        </tr>
        <tr>
            <td align="center">
                <span class="sg-image" style="float: none; display: block;
                text-align: center;"><img alt="develandoo" id="image-section" height="388" src="https://develandoo.rocks/wp-content/uploads/2017/05/db03c52a-557d-4419-b3ff-1315d5357f3f.png" style="width: 700px;
                height: 388px;" width="700"/></span></td>
        </tr>
        <tr>
            <td align="center" style="padding:25px 0 20px; border: 0 none!important;">
                <table align="center" id="table-content" bgcolor="#ffffff" cellpadding="0"
                       style="font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important;"
                       width="700">
                    <tbody>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">As promised in the previous article, we are going to touch very sensitive topics and actual issues with recruiting and team building activities, especially in Central and Eastern Europe.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">Today, we are going to discuss sensitive issues like hiring contractors including the pluses and minuses.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">Let&rsquo;s start with the negatives. Often you hear complaints about contractors, including their lack of team attitude, lack of product knowledge and overall loss of intellectual property when there is a constant change of team members and temporary contractors. Yet, the contractor market is booming. Why is that, and why are so many companies hiring contractors?</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">I worked as a contractor in the beginning of my career and then switched to being an entrepreneur. I started by helping new and established businesses build their teams and products. In the second half of my career, I have seen a lot of changes in the contracting industry.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">First off, the contracting market is booming. For engineers it&#39;s very tempting to switch to contracting as soon as they develop a reputation in the market and have enough skills to sell themselves.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">In addition, an engineer can make 4x the amount of money as a contractor as opposed to being a full-time employee. As a professional contractor, you can make up to 120 EU an hour in Germany and even more in Switzerland.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">So what is wrong with contracting?</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">Well, nothing for the contractor, but what about the product and business owner?</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px; border: 0 none!important;">
                            <p style="margin:0; color:#636363">For some business owners, usually for those who are not coming from an engineering background, it&#39;s easy to hire contractors. Even considering the high prices, they see benefits in doing so.</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr align="center">
            <td style="padding-bottom:14px; border: 0 none!important;">
                <table bgcolor="#f2cd32" style="border-radius:5px" width="160">
                    <tbody>
                    <tr align="center">
                        <td style="border: 0 none!important;"><a
                                href="https://develandoo.rocks/"
                                style="color:#fff; text-decoration:none!important; line-height:28px"
                                target="_blank">FIND OUT MORE</a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border: 0 none!important;">
                <table align="center" bgcolor="#6e6b6b" height="30"
                       style="border-spacing:0 10px; font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important;"
                       width="100%">
                    <tbody>
                    <tr>
                        <td align="right" style="color:#dbdada; border: 0 none!important;" width="100%"><a
                                href="[unsubscribe]" id="unsubscribe"
                                style="text-decoration: underline!important; color: #b8b8b8!important;font-size:12px;margin-right:20px;">Click
                                here if you don&#39;t want to receive emails from us </a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>