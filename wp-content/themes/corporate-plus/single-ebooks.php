<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>
<div class="padding50 container-fluid">
    <div class=" col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="row">
            <div class="left-widget">

                <?php if (is_active_sidebar('sidebar-22')) : ?>
                    <div id="secondary" class="widget-area" role="complementary">aaaaaa
                        <?php dynamic_sidebar('sidebar-22'); ?>
                    </div>
                <?php endif; ?>

            </div>

        </div>
    </div>
    <div class="article-area col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <h1 class="page_title col-lg-12 col-md-12"> <?php the_title(); ?></h1>


        <?php
        // Start the loop.
        while (have_posts()) : the_post(); ?>

            <article
                id="post-<?php the_ID(); ?>" <?php post_class('init-animate fadeInDown animated'); ?>>
                <div class="single-p1">
                    <?php
                    if (has_post_thumbnail()) {
                        echo '<figure class="single_thumb">';
                        the_post_thumbnail($thumbnail);
                        echo "</figure>";
                    }
                    ?>
                </div>
                <!-- .single-feat-->
                <div class="content-wrapper">
                    <header class="entry-header1 text-center">
                        <div class="entry-meta">
                            <?php corporate_plus_posted_on(); ?>
                        </div>
                        <!-- .entry-meta -->
                    </header>
                    <!-- .entry-header -->

                    <!--post thumbnal options-->
                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php
                        wp_link_pages(array(
                            'before' => '<div class="page-links">' . esc_html__('Pages:', 'corporate-plus'),
                            'after' => '</div>',
                        ));
                        ?>
                    </div>
                    <!-- .entry-content -->

                    <footer class="entry-footer">
                        <?php corporate_plus_entry_footer(); ?>
                    </footer>
                    <!-- .entry-footer -->
                </div>

            </article><!-- #post-## -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center top_space">
                <a class="download_link"
                   href="<?php echo get_post_meta(get_the_ID(), 'url', true); ?>"
                   download="<?php the_title(); ?>">Download link</a>
            </div>

            <?php // If comments are open or we have at least one comment, load up the comment template.
            if (comments_open() || get_comments_number()) {
                comments_template();
            }

            if (is_singular('attachment')) {
                // Parent post navigation.
                the_post_navigation(array(
                    'prev_text' => _x('<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen'),
                ));
            } elseif (is_singular('post')) {
                // Previous/next post navigation.
                the_post_navigation(array(
                    'next_text' => '<span class="meta-nav" aria-hidden="true">' . __('Next', 'twentysixteen') . '</span> ' .
                        '<span class="screen-reader-text">' . __('Next post:', 'twentysixteen') . '</span> ' .
                        '<span class="post-title">%title</span>',
                    'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __('Previous', 'twentysixteen') . '</span> ' .
                        '<span class="screen-reader-text">' . __('Previous post:', 'twentysixteen') . '</span> ' .
                        '<span class="post-title">%title</span>',
                ));
            }

            // End of the loop.
        endwhile;
        ?>
        <!-- .site-main -->
    </div>
    <?php get_footer(); ?>
