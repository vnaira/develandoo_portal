<?php
/**
 * Display Social Links
 *
 * @since Corporate Plus 1.1.0
 *
 * @param null
 * @return void
 *
 */

if ( !function_exists('corporate_plus_social_links') ) :

    function corporate_plus_social_links( ) {

        global $corporate_plus_customizer_all_values;
        ?>
        <ul class="socials text-right init-animate animated fadeInRight">
            <?php
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-facebook-url'] ) ) { ?>
                <li class="facebook">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-facebook-url'] ); ?>" data-title="Facebook" target="_blank"></a>
                </li>
            <?php }
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-twitter-url'] ) ) { ?>
                <li class="twitter">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-twitter-url'] ); ?>" data-title="Twitter" target="_blank"></a>
                </li>
            <?php }
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-linkedin-url'] ) ) { ?>
                <li class="linkedin">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-linkedin-url'] ); ?>" data-title="Linkedin" target="_blank"></a>
                </li>
            <?php }
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-gplus-url'] ) ) { ?>
                <li class="gplus">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-gplus-url'] ); ?>" class="gplus" data-title="gplus" target="_blank"></a>
                </li>
            <?php }
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-angel-url'] ) ) { ?>
                <li class="angel">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-angel-url'] ); ?>" class="angel" data-title="Angel" target="_blank"></a>
                </li>
            <?php }
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-branded-url'] ) ) { ?>
                <li class="branded">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-branded-url'] ); ?>" class="branded" data-title="Branded" target="_blank"></a>
                </li>
            <?php }
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-crunchbase-url'] ) ) { ?>
                <li class="crunchbase">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-crunchbase-url'] ); ?>" class="crunchbase" data-title="Crunchbase" target="_blank"></a>
                </li>
            <?php }
            if ( !empty( $corporate_plus_customizer_all_values['corporate-plus-yelp-url'] ) ) { ?>
                <li class="yelp">
                    <a href="<?php echo esc_url( $corporate_plus_customizer_all_values['corporate-plus-yelp-url'] ); ?>" class="yelp" data-title="Yelp" target="_blank"></a>
                </li>
            <?php }?>
        </ul>
        <?php
    }

endif;

add_filter( 'corporate_plus_action_social_links', 'corporate_plus_social_links', 10 );