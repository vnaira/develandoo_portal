<?php
/**
 * Footer content
 *
 * @since Corporate Plus 1.0.0
 *
 * @param null
 * @return null
 *
 */
remove_action('wp_head', 'wp_generator');
if (!function_exists('corporate_plus_footer')) :

    function corporate_plus_footer()
    {

        global $corporate_plus_customizer_all_values;
        ?>

        <div class="clearfix"></div>
        <footer class="site-footer">
            <div class="row">

                <div class="container-fluid social-section m-top40 s-gray m-top0">
<!--orange footer-->
                    <div class="col-xs-12 col-sm-12 hidden-lg hidden-md text-center tour footer-blocks-orange">
                        <div class="row footer-tour-orange">
                            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
                        </div>
                    </div>
                    </div>

<div class="container-fluid social-section">
                    <!--social links-->
                    <div class="container text-center soc-block">
                        <ul class="list-inline list-unstyled margin0">
                            <li><a href="https://www.youtube.com/channel/UC8ykyVj5eOOexhZxE1llaVg"
                                   class="soc-icon youtube"
                                   target="_blank">
                                </a></li>
                            <li>
                                <a href="https://www.linkedin.com/company/6650958?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A6650958%2Cidx%3A1-1-1%2CtarId%3A1465221765399%2Ctas%3Adevelandoo"
                                   class="soc-icon linkin" target="_blank">

                                </a></li>
                            <li><a href="https://plus.google.com/+DevelandooLLC"
                                   class="soc-icon google"
                                   target="_blank">

                                </a></li>
                            <li><a href="https://twitter.com/develandoo" class="soc-icon twitter" target="_blank">

                                </a>
                            </li>
                            <li><a href="https://www.facebook.com/develandoo/timeline" class="soc-icon facebook"
                                   target="_blank">

                                </a></li>
                            <li><a href="https://angel.co/develandoo-3" class="soc-icon angel" target="_blank">

                                </a></li>
                            <li><a href="https://www.f6s.com/develandoollc" class="soc-icon f6s" target="_blank">

                                </a>
                            </li>
                            <li><a href="https://branded.me/develandooag" class="soc-icon branded" target="_blank">

                                </a>
                            </li>
                            <li><a href="https://www.crunchbase.com/organization/develandoo" class="soc-icon crunch"
                                   target="_blank">

                                </a></li>
                            <li><a href="https://www.yelp.com/biz/develandoo-baar" class="soc-icon yelp"
                                   target="_blank">

                                </a></li>
                        </ul>
                    </div>
                    <!--footer menu-->
                    </div>
                <div class="text-center tour footer-blocks hidden-xs hidden-sm copyright ">
                    <div class="container-fluid">
                        <div class="container">
                            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
                        </div>
                    </div>
                </div>
                <div class="container-fluid text-center m-top10"><p>&copy; Develandoo</p></div>
            </div>
        </footer>
        <?php
    }
endif;
add_action('corporate_plus_action_footer', 'corporate_plus_footer', 10);

/**
 * Page end
 *
 * @since Corporate Plus 1.1.0
 *
 * @param null
 * @return null
 *
 */
if (!function_exists('corporate_plus_page_end')) :

    function corporate_plus_page_end()
    {
        ?>
        </div><!-- #page -->
        <?php
    }
endif;
add_action('corporate_plus_action_after', 'corporate_plus_page_end', 10);
?>
