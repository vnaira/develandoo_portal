<?php
/**
 * Callback functions for comments
 *
 * @since Corporate Plus 1.0.0
 *
 * @param $comment
 * @param $args
 * @param $depth
 * @return void
 *
 */

if (!function_exists('corporate_plus_commment_list')) :

    function corporate_plus_commment_list($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        if ('div' == $args['style']) {
            $tag = 'div';
            $add_below = 'comment';
        } else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
        ?>
        <<?php echo $tag ?>
        <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
        <?php if ('div' != $args['style']) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body clearfix">
    <?php endif; ?>
        <div class="comment-author vcard">
            <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, '64'); ?>
            <?php printf(__('<cite class="fn">%s</cite>', 'corporate-plus'), get_comment_author_link()); ?>
        </div>
        <?php if ($comment->comment_approved == '0') : ?>
        <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'corporate-plus'); ?></em>
        <br/>
    <?php endif; ?>
        <div class="comment-meta commentmetadata">
            <a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>">
                <i class="fa fa-clock-o"></i>
                <?php
                /* translators: 1: date, 2: time */
                printf(__('%1$s at %2$s', 'corporate-plus'), get_comment_date(), get_comment_time());
                ?>
            </a>
            <?php edit_comment_link(__('(Edit)', 'corporate-plus'), '  ', ''); ?>
        </div>
        <?php comment_text(); ?>
        <div class="reply">
            <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
        </div>
        <?php if ('div' != $args['style']) : ?>
        </div>
    <?php endif; ?>
        <?php
    }

endif;

/**
 * Select sidebar according to the options saved
 *
 * @since Corporate Plus 1.0.0
 *
 * @param null
 * @return string
 *
 */
if (!function_exists('corporate_plus_sidebar_selection')) :

    function corporate_plus_sidebar_selection()
    {
        global $corporate_plus_customizer_all_values;
        if (
            'left-sidebar' == $corporate_plus_customizer_all_values['corporate-plus-sidebar-layout'] ||
            'no-sidebar' == $corporate_plus_customizer_all_values['corporate-plus-sidebar-layout']
        ) {
            $corporate_plus_body_global_class = $corporate_plus_customizer_all_values['corporate-plus-sidebar-layout'];
        } else {
            $corporate_plus_body_global_class = 'right-sidebar';
        }
        return $corporate_plus_body_global_class;
    }

endif;

/**
 * BreadCrumb Settings
 */
if (!function_exists('corporate_plus_breadcrumbs')):

    function corporate_plus_breadcrumbs()
    {
        wp_reset_postdata();
        global $post;
        $trans_home = "<div class='breadcrumb'><i class='fa fa-home'></i></div>";

        $search_result_text = __('Search Results for ', 'corporate-plus');

        $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
        $delimiter = '<span class="bread_arrow fa fa-angle-double-right"></span>'; // delimiter between crumbs
        $home = $trans_home; // text for the 'Home' link
        $showHomeLink = 1;

        $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
        $before = '<span class="current">'; // tag before the current crumb
        $after = '</span>'; // tag after the current crumb

        $homeLink = esc_url(home_url());
        echo "<div class='breadcrumbs init-animate fadeInDown animated'>";
        if (is_home() || is_front_page()) {

            if ($showOnHome == 1)
                echo '<div id="corporate-plus-breadcrumbs"><div class="breadcrumb-container"><a href="' . $homeLink . '">' . $home . '</a></div></div>';
        } else {
            if ($showHomeLink == 1) {
                echo '<div id="corporate-plus-breadcrumbs" class="clearfix"><div class="breadcrumb-container"><a href="' . $homeLink . '">' . $home . '</a> ' . ' ';
            } else {
                echo '<div id="corporate-plus-breadcrumbs" class="clearfix"><div class="breadcrumb-container">' . $home . ' ' . ' ';
            }

            if (is_category()) {
                $thisCat = get_category(get_query_var('cat'), false);
                if ($thisCat->parent != 0)
                    echo esc_html(get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' '));
                echo $before . esc_html(single_cat_title('', false)) . $after;
            } elseif (is_search()) {
                echo $before . $search_result_text . ' "' . esc_html(get_search_query()) . '"' . $after;
            } elseif (is_day()) {
                echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_attr(get_the_time('Y')) . '</a> ' . $delimiter . ' ';
                echo '<a href="' . esc_url(get_month_link(get_the_time('Y'), get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a> ' . $delimiter . ' ';
                echo $before . esc_html(get_the_time('d')) . $after;
            } elseif (is_month()) {
                echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a> ' . $delimiter . ' ';
                echo $before . esc_html(get_the_time('F')) . $after;
            } elseif (is_year()) {
                echo $before . esc_html(get_the_time('Y')) . $after;
            } elseif (is_single() && !is_attachment()) {
                if (get_post_type() != 'post') {
                    $post_type = get_post_type_object(get_post_type());
                    $slug = $post_type->rewrite;
                    echo '<a href="' . $homeLink . '/' . esc_html($slug['slug']) . '/">' . esc_html($post_type->labels->singular_name) . '</a>';
                    if ($showCurrent == 1)
                        echo ' ' . $delimiter . ' ' . $before . esc_html(get_the_title()) . $after;
                } else {
                    $cat = get_the_category();
                    $cat = $cat[0];
                    $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                    if ($showCurrent == 0)
                        $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
                    echo $cats;
                    if ($showCurrent == 1)
                        echo $before . esc_html(get_the_title()) . $after;
                }
            } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
                $post_type = get_post_type_object(get_post_type());
                echo $before . esc_html($post_type->labels->singular_name) . $after;
            } elseif (is_attachment()) {
                $parent = get_post($post->post_parent);
                $cat = get_the_category($parent->ID);
                $cat = $cat[0];
                echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                echo '<a href="' . esc_url(get_permalink($parent)) . '">' . esc_html($parent->post_title) . '</a>';
                if ($showCurrent == 1)
                    echo ' ' . $delimiter . ' ' . $before . esc_html(get_the_title()) . $after;
            } elseif (is_page() && !$post->post_parent) {
                if ($showCurrent == 1)
                    echo $before . esc_html(get_the_title()) . $after;
            } elseif (is_page() && $post->post_parent) {
                $parent_id = $post->post_parent;
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_post($parent_id);
                    $breadcrumbs[] = '<a href="' . esc_url(get_permalink($page->ID)) . '">' . get_the_title($page->ID) . '</a>';
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs) - 1)
                        echo ' ' . $delimiter . ' ';
                }
                if ($showCurrent == 1)
                    echo ' ' . $delimiter . ' ' . $before . esc_html(get_the_title()) . $after;
            } elseif (is_tag()) {
                echo $before . __('Posts tagged :', 'corporate-plus') . esc_html(single_tag_title('', false)) . '"' . $after;
            } elseif (is_author()) {
                global $author;
                $userdata = get_userdata($author);
                echo $before . __('Author :', 'corporate-plus') . $userdata->display_name . $after;
            } elseif (is_404()) {
                echo $before . __('Error 404 :', 'corporate-plus') . $after;
            } else {

            }
            if (get_query_var('paged')) {
                if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                    echo ' (';
                echo __('Page', 'corporate-plus') . ' ' . get_query_var('paged');
                if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                    echo ')';
            }
            echo '</div></div>';
        }
        echo "</div>";
    }

endif;

if (is_page_template('page-templates/my-template.php')) {
    include_once 'page-templates/my-template.php';
}

function wpb_widgets_init()
{

    register_sidebar(array(
        'name' => __('Footer sidebar', 'wpb'),
        'id' => 'sidebar-21',
        'description' => __('Locationes', 'wpb'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => __('Submenu widget', 'wpb'),
        'id' => 'sidebar-22',
        'description' => __('Tutorials', 'wpb'),
        'before_widget' => '<div id="right" class="dropdown">',
        'after_widget' => '</div>',
        'before_title' => '<button type="button" class="dropdown-toggle" data-toggle="dropdown">',
        'after_title' => '<b class="custom_caret"></b></button><div class="dropdown-menu">',
    ));
    register_sidebar(array(
        'name' => __('Submenu for jobs', 'wpb'),
        'id' => 'sidebar-20',
        'description' => __('Categories', 'wpb'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => __('Sidebar for video page', 'wpb'),
        'id' => 'sidebar-19',
        'description' => __('Tags', 'wpb'),
        'before_widget' => '<div id="left" class="dropdown">',
        'after_widget' => '</div>',
        'before_title' => '<button type="button" class="dropdown-toggle" data-toggle="dropdown">',
        'after_title' => '<b class="custom_caret"></b></button><div class="dropdown-menu">',
    ));
    register_sidebar(array(
        'name' => __('Submenu for ebooks', 'twentysixteen'),
        'id' => 'sidebar-4',
        'description' => __('Appears at the ebooks page.', 'twentysixteen'),
        'before_widget' => '<section id="ebooks" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => __('Last comments widget', 'twentysixteen'),
        'id' => 'sidebar-5',
        'description' => __('The sidebar for last comments', 'twentysixteen'),
        'before_widget' => '<section id="comments" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

}

add_action('widgets_init', 'wpb_widgets_init');

function my_home_category($query)
{
    if ($query->is_home() && $query->is_main_query()) {
        $query->set('cat', '5');
    }
}

add_action('pre_get_posts', 'my_home_category');

function custom_post_type()
{

    // set up labels
    $label = array(
        'name' => 'Videos',
        'singular_name' => 'video',
        'add_new' => 'Add New video',
        'add_new_item' => 'Add New video',
        'edit_item' => 'Edit video',
        'new_item' => 'New video',
        'all_items' => 'All videos',
        'view_item' => 'View video',
        'search_items' => 'Search videos',
        'not_found' => 'No videos Found',
        'not_found_in_trash' => 'No videos found in Trash',
        'parent_item_colon' => '',
        'menu_name' => 'Videos',
    );
//register post type
    register_post_type('video', array(
            'labels' => $label,
            'has_archive' => true,
            'public' => true,
            'supports' => array('title', 'editor', 'author', 'comments', 'custom-fields', 'thumbnail', 'page-attributes'),
            'taxonomies' => array('post_tag', 'category'),
            'exclude_from_search' => false,
            'capability_type' => 'post',
            'rewrite' => array('slug' => 'video'),
        )
    );
}


add_action('init', 'custom_post_type', 0);

function ps_count_user_comments($usid)
{
    global $wpdb;
    $count = $wpdb->get_var(
        'SELECT COUNT(comment_ID) FROM ' . $wpdb->comments . '
    WHERE user_id = "' . $usid . '"
    AND comment_approved = "1"
    AND comment_type IN ("comment", "")'
    );

    echo $count;
}

add_action('ps_count_user_comments', 'ps_count_user_comments');

function is_bot()
{
    /* This function will check whether the visitor is a search engine robot */

    $botlist = array("Teoma", "alexa", "froogle", "Gigabot", "inktomi",
        "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory",
        "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot",
        "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp",
        "msnbot", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz",
        "Baiduspider", "Feedfetcher-Google", "TechnoratiSnoop", "Rankivabot",
        "Mediapartners-Google", "Sogou web spider", "WebAlta Crawler", "TweetmemeBot",
        "Butterfly", "Twitturls", "Me.dium", "Twiceler");

    foreach ($botlist as $bot) {
        if (strpos($_SERVER['HTTP_USER_AGENT'], $bot) !== false)
            return true;    // Is a bot
    }

    return false;    // Not a bot
}

function do_add_count()
{
    global $wpdb;

    if (isset($_POST['user_id']) && isset($_POST['item_id']) && isset($_POST['directory'])) {
        $user_id = $_POST['user_id'];
        $item_id = $_POST['item_id'];
        $result =ABSPATH . trim(parse_url($_POST['directory'], PHP_URL_PATH), "/");

//            /* If the visitor is not a search engine, count the downoad: */
        if (!is_bot()) {

            $wpdb->insert('bitxt_book_downloads', array(
                'user_id' => $user_id,
                'item_id' => $item_id,
                'link' => $_POST['directory']
            ));
            if (file_exists($result)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($result));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($result));
                ob_clean();
                flush();
                readfile($result);

                exit;
            }
        }
    }
}


add_action('do_add_count', 'do_add_count');


//function returns count of downloads

function ps_count_user_downloads($usid)
{
    global $wpdb;
    $count = $wpdb->get_var(
        'SELECT COUNT(item_id) FROM bitxt_book_downloads
    WHERE user_id = "' . $usid . '"'
    );

    echo $count;
}

add_action('ps_count_user_downloads', 'ps_count_user_downloads');

//function returns ebooks id by user
function list_of_books($usid)
{
    global $wpdb;
    $count = $wpdb->get_results(
        'SELECT item_id FROM bitxt_book_downloads
    WHERE user_id = "' . $usid . '" GROUP BY item_id'
    );

    return $count;
}

add_action('list_of_books', 'list_of_books');

//function returns ebooks data by id
function get_book_data($bookid)
{
    global $wpdb;
    $book = $wpdb->get_results(
        'SELECT * FROM bitxt_posts
    WHERE ID = "' . $bookid . '"'
    );

    return $book;
}

add_action('get_book_data', 'get_book_data');

function my_filter_function()
{
    $args = array(
        'posts_per_page' => -1,
        'orderby' => 'date', // we will sort posts by date
        'order' => $_POST['date'] // ASC или DESC
    );

    $query = new WP_Query($args);

    $postslist = get_posts($args);
    foreach ($postslist as $post_item) :
        setup_postdata($post_item);
        $the_date = mysql2date(get_option('date_format'), $post_item->post_date);
        ?>
        <div class="front_article">

            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2">
                <div class="row">
                    <div class="avatar">
                        <?php echo get_avatar(get_the_author_email(), '80', 'assets/img/no_images.jpg', get_the_author()); ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <div class="text_part fleft">
                    <h2><a href="<?php the_permalink(); ?>"><?php echo get_the_title($post_item); ?></a></h2>
                    <!--                    <a href="--><?php //echo um_get_core_page('user');
                    ?><!--">-->
                    <span style="color: #ccc"><?php echo get_the_author(); ?></span>
                    <!--                    </a>-->

                    <div class="text_exc">
                        <?php echo get_the_excerpt($post_item); ?>
                        <p>
                            <?php echo $the_date; ?></p>
                        <a class="read-more pull-right"
                           href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
                    </div>
                </div>
            </div>
        </div>

        <?php
    endforeach;
    wp_reset_postdata();
    wp_die();
}

add_action('wp_ajax_myfilter', 'my_filter_function');
add_action('wp_ajax_nopriv_myfilter', 'my_filter_function');

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);
add_filter('show_admin_bar', '__return_false');

function mytheme_infinite_scroll_init()
{
    add_theme_support('infinite-scroll', array(
        'container' => 'response',
        'render' => 'mytheme_infinite_scroll_render',
        'footer' => 'wrapper',
    ));
}

add_action('init', 'mytheme_infinite_scroll_init');

function mytheme_infinite_scroll_render()
{
    get_template_part('loop');
}

//get counts of like for forum
function ps_count_of_likes($usid)
{
    global $wpdb;
    $sql = "SELECT COUNT(likeid) as countl FROM `bltxt_wpforo_likes` WHERE `post_userid`=" . $usid;
    $count = $wpdb->get_results($sql, OBJECT);


    return $count;
}

add_action('ps_count_of_likes', 'ps_count_of_likes');

function cut_string($str)
{
    $pos = strpos($str, "#");
    $new_string = strstr($str, '/#', true);
    return $new_string;

}

add_action('cut_string', 'cut_string');

function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

add_action('excerpt', 'excerpt');

function register_footer_menu() {
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_footer_menu' );

function custom_excerpt_length( $length ) {
	return 70;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

 