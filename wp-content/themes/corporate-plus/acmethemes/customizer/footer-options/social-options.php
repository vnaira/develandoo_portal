<?php
/*adding sections for footer social options */
$wp_customize->add_section( 'corporate-plus-footer-social', array(
    'priority'       => 20,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Social Options', 'corporate-plus' ),
    'panel'          => 'corporate-plus-footer-panel'
) );

/*facebook url*/
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-facebook-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-facebook-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-facebook-url]', array(
    'label'		=> __( 'Facebook url', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-facebook-url]',
    'type'	  	=> 'url',
    'priority'  => 10
) );

/*twitter url*/
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-twitter-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-twitter-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-twitter-url]', array(
    'label'		=> __( 'Twitter url', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-twitter-url]',
    'type'	  	=> 'url',
    'priority'  => 20
) );

/*youtube url*/
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-gplus-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-gplus-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-gplus-url]', array(
    'label'		=> __( 'G +', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-gplus-url]',
    'type'	  	=> 'url',
    'priority'  => 30
) );

/*linkedin url*/
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-linkedin-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-linkedin-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-linkedin-url]', array(
    'label'		=> __( 'Linkedin url', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-linkedin-url]',
    'type'	  	=> 'url',
    'priority'  => 40
) );
//branded url
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-branded-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-branded-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-branded-url]', array(
    'label'		=> __( 'Branded url', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-branded-url]',
    'type'	  	=> 'url',
    'priority'  => 50
) );
//angel url
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-angel-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-angel-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-angel-url]', array(
    'label'		=> __( 'Angel url', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-angel-url]',
    'type'	  	=> 'url',
    'priority'  => 60
) );
//crunchbase url
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-crunchbase-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-crunchbase-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-crunchbase-url]', array(
    'label'		=> __( 'Crunchbase url', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-crunchbase-url]',
    'type'	  	=> 'url',
    'priority'  => 70
) );
//yelp url
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-yelp-url]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-yelp-url'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-yelp-url]', array(
    'label'		=> __( 'Yelp url', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-yelp-url]',
    'type'	  	=> 'url',
    'priority'  => 80
) );

/*enable social*/
$wp_customize->add_setting( 'corporate_plus_theme_options[corporate-plus-enable-social]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['corporate-plus-enable-social'],
    'sanitize_callback' => 'corporate_plus_sanitize_checkbox',
) );
$wp_customize->add_control( 'corporate_plus_theme_options[corporate-plus-enable-social]', array(
    'label'		=> __( 'Enable social', 'corporate-plus' ),
    'section'   => 'corporate-plus-footer-social',
    'settings'  => 'corporate_plus_theme_options[corporate-plus-enable-social]',
    'type'	  	=> 'checkbox',
    'priority'  => 90
) );