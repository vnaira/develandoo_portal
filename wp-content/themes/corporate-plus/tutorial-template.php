<?php /* Template Name: Tutorial page */

$counter = 1;
global $corporate_plus_customizer_all_values;
get_header(); ?>

<div class="container-fluid">
    <div class="sublist">
        <?php if (is_active_sidebar('sidebar-22')) : ?>
            <div id="secondary" class="widget-area" role="complementary">
                <?php dynamic_sidebar('sidebar-5'); ?>
            </div>
        <?php endif; ?>
    </div>


    <div id="content" class="site-content">
        <div id="" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php
                $catquery = new WP_Query('cat=7&posts_per_page=8');
                while ($catquery->have_posts()) : $catquery->the_post();

                    if ($counter % 2 != 0) { ?>
                        <div class="front_page_article">
                            <article
                                id="<?php echo $counter; ?>" <?php post_class('init-animate fadeInDown animated'); ?>>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        echo '<figure>';
                                        the_post_thumbnail($thumbnail);
                                        echo "</figure>";
                                    }

                                    ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="content-wrapper">
                                        <header class="entry-header">
                                            <?php the_title(sprintf('<h2 class="entry-title text-center"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>

                                            <?php
                                            if ('post' === get_post_type()) : ?>
                                                <div class="entry-meta">
                                                    <?php corporate_plus_posted_on(); ?>
                                                </div><!-- .entry-meta -->
                                                <?php
                                            endif; ?>
                                        </header>
                                        <!-- .entry-header -->

                                        <div class="entry-content">
                                            <?php
                                            the_excerpt();
                                            ?>
                                            <a class="read-more"
                                               href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
                                            <?php
                                            wp_link_pages(array(
                                                'before' => '<div class="page-links">' . esc_html__('Pages:', 'corporate-plus'),
                                                'after' => '</div>',
                                            ));
                                            ?>
                                        </div>
                                        <!-- .entry-content -->
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </article>
                            <!-- #post-## -->
                        </div>
                    <?php } else { ?>
                        <div class="front_page_article">
                            <article
                                id="<?php echo $counter; ?>" <?php post_class('init-animate fadeInDown animated'); ?>>
                                <div
                                    class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12">

                                    <?php
                                    if (has_post_thumbnail()) {
                                        echo '<figure>';
                                        the_post_thumbnail($thumbnail);
                                        echo "</figure>";
                                    }
                                    ?>
                                </div>
                                <div
                                    class="col-lg-6 col-lg-pull-6 col-md-pull-6 col-md-6  col-sm-6 col-sm-pull-6 col-xs-12">
                                    <div class="content-wrapper">
                                        <header class="entry-header">
                                            <?php the_title(sprintf('<h2 class="entry-title text-center"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>

                                            <?php
                                            if ('post' === get_post_type()) : ?>
                                                <div class="entry-meta">
                                                    <?php corporate_plus_posted_on(); ?>
                                                </div><!-- .entry-meta -->
                                                <?php
                                            endif; ?>
                                        </header>
                                        <!-- .entry-header -->

                                        <div class="entry-content">
                                            <?php
                                            the_excerpt();
                                            ?>
                                            <a class="read-more"
                                               href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
                                            <?php
                                            wp_link_pages(array(
                                                'before' => '<div class="page-links">' . esc_html__('Pages:', 'corporate-plus'),
                                                'after' => '</div>',
                                            ));
                                            ?>
                                        </div>
                                        <!-- .entry-content -->
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </article>
                            <!-- #post-## -->
                        </div>
                    <?php } ?>

                    <?php $counter++; ?>
                <?php endwhile; ?>


            </main>
            <!-- #main -->
             <?php the_posts_pagination(array('mid_size' => 2)); ?>
        </div>
        <!-- #primary -->
    </div>
</div><!-- #content -->
<?php get_footer(); ?>


