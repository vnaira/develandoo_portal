<?php
/*Template Name: Article Template*/

get_header();

?>
<div class="container-fluid">
    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <div class="row">
            <div class="left-widget">

                <?php if (is_active_sidebar('sidebar-22')) : ?>
                    <div id="secondary" class="widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar-22'); ?>
                    </div>
                <?php endif; ?>
                <?php if (is_active_sidebar('sidebar-20')) : ?>
                    <div id="secondary" class="widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar-20'); ?>
                    </div>
                <?php endif; ?>
                <?php if (is_active_sidebar('sidebar-4')) : ?>
                    <div id="secondary" class="widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar-4'); ?>
                    </div>
                <?php endif; ?>
            </div>

        </div>
    </div>
    <div class="article-area col-lg-9 col-md-9 col-lg-offset-1 col-md-offset-1 col-sm-10 col-xs-12">
        <div class="page-filter" style="margin-top:30px; max-width:100%">
            <h1 class="page_title"> <?php the_title(); ?></h1>

            <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter" class="col-lg-11 col-xs-12 text-right">
                <label>
                    <input type="radio" name="date" value="ASC"/>ASC
                </label>
                <label>
                    <input type="radio" name="date" value="DESC" selected="selected"/>DESC
                </label>
                <button class="filert_by_date">Filter by date</button>
                <input type="hidden" name="action" value="myfilter">
            </form>

        </div>
        <div id="response" class="col-lg-12 col-md-12 col-xs-12">
            <?php
            $args = array('posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'title');
            $postslist = get_posts($args);
            foreach ($postslist as $post) :
                setup_postdata($post);

                ?>
                <div class="front_article">

                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-6">
                        <div class="row">
                            <div class="avatar">
                                <?php echo get_avatar(get_the_author_email(), '80', 'assets/img/no_images.jpg', get_the_author()); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-6">
                        <div class="text_part fleft">
                            <h2><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>
                            <a href="<?php echo um_get_core_page('user'); ?>">
                                <?php echo get_the_author(); ?>
                            </a>

                            <div class="text_exc">
                                <?php echo $post->post_excerpt;?>
                                    <?php $the_date = mysql2date(get_option('date_format'), $post->post_date);
                                    echo $the_date; ?></p>
                                <a class="read-more pull-right"
                                   href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
            endforeach;
            wp_reset_postdata();
            ?>

        </div>
    </div>
</div><!-- .content-area -->

<?php get_footer(); ?>
