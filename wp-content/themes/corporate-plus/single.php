<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
global $corporate_plus_customizer_all_values;

get_header();

$cat = get_the_category();
$parentCatName = get_cat_name($cat[0]->parent);
global $post;
$author_id = $post->post_author;


$args = array('numberposts' => '1');
$recent_posts = wp_get_recent_posts($args);
//foreach ($recent_posts as $recent){

?>
<?php while (have_posts()) :
the_post(); ?>
    <div class="container-fluid">
        <div class="row">
            <?php get_template_part('template-parts/content', 'single'); ?>
        </div>
    </div>

<div class="container">

    <?php endwhile;
    wp_reset_postdata();// End of the loop. ?>

</div>

<div class="container-fluid yellow_space">
    <div class="container">
        <h2 class="top-text col-lg-12 col-md-12 col-xs-12">TRENDING ARTICLES</h2>
    </div>
</div>
<div id="" class="container">
    <div class="row">
        <div class="article-area col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30 min-padding0">
            <?php
            $args = array('posts_per_page' => 6, 'order' => 'DESC', 'orderby' => 'date');
            $postslist = get_posts($args);
            foreach ($postslist as $post) :
                setup_postdata($post);

                ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 m-top50 m-bot50 shadowed">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                        <a href="<?php echo $post->guid; ?>">
                            <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>"
                                 class="img-responsive center-block img-fixed-height" width="500" alt=""/>
                                 </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top10">
                        <h2 class="text-center"><a
                                href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a>
                        </h2>
                    </div>
                </div>


                <?php
            endforeach;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
<!--single page -->

<?php get_footer(); ?>
