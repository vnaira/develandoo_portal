<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
?>

<div class="container-fluid m-top65 article-page ">
    <div class="container-fluid article-filter">
            <div class="container category-filter">
            <ul class="list-unstyled list-inline">
            <?php
            global $post;
            $category_detail=get_the_category($post->ID);
                          foreach($category_detail as $cd){?>
                      <li>
                        <a><?php  echo $cd->cat_name;?></a>
                        </li>

                         <?php }
                        ?>
                </ul>
                <div class=" m-top60">
                    <?php the_title('<h2 class="font20bold">', '</h2>'); ?>
                </div>
            </div>
        <div class="container">
            <ul class="list-unstyled list-inline about-article margin0 hidden-sm hidden-xs ">
                <li><a href="<?php the_author_meta('user_url', $author->ID);?>" target="_blank">
                <?php echo get_avatar( get_the_author_email(), '60', '/images/no_images.jpg', get_the_author() ); ?>
                <span><?php echo get_the_author_meta('first_name', $author_id) . " " . get_the_author_meta('last_name', $author_id); ?></span>
                <?php echo get_the_author_meta( 'website', $author_id );?>
                </a></li>
                <li><i class="fa fa-eye" aria-hidden="true"></i>
                <span> <?php if(function_exists('the_views')) { echo 11;the_views(); } ?></span>
                </li>
                <li><span><?php echo get_the_date(' j F, Y') ?> </span> <i class="fa fa-clock-o" aria-hidden="true"></i>
                <span><?php echo do_shortcode('[rt_reading_time label="" postfix="min read" postfix_singular="min read"]');?></span></li>
            </ul>
            <ul class="list-unstyled   margin0 about-article-mobile hidden-lg hidden-md pull-left">
                                <li><a href="<?php the_author_meta('user_url', $author->ID);?>" target="_blank">
                                        <?php echo get_avatar( get_the_author_email(), '60', '/images/no_images.jpg', get_the_author() ); ?>
                                        <span><?php echo get_the_author_meta('first_name', $author_id) . " " . get_the_author_meta('last_name', $author_id); ?></span>
                                        <?php echo get_the_author_meta( 'website', $author_id );?>
                                    </a></li>
            </ul>
            <ul class="list-unstyled   margin0 about-article-mobile hidden-lg hidden-md pull-right text-right">
                <li><i class="fa fa-eye" aria-hidden="true"></i>
                    <span> <?php if(function_exists('the_views')) { echo 11;the_views(); } ?></span>
                </li>
                <li><span><?php echo get_the_date(' j F, Y') ?> </span>
                   </li>
                <li>
                  <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span><?php echo do_shortcode('[rt_reading_time label="" postfix="min read" postfix_singular="minute"]');?></span>
                </li>
            </ul>
        </div>
    </div>
    <div class="row item-article">
        <?php
        if (has_post_thumbnail()) {
            echo '<figure>';
            the_post_thumbnail($thumbnail);
            echo "</figure>";
        }
        ?>
    </div>
</div>
<div class="container-fluid">
    <div class="container article-section">
        <div class="col-xs-12 ">
            <div class="entry-content article-content m-top60">
                <?php the_content(); ?>
            </div><!-- .entry-content -->
            <footer class="entry-footer">
                <?php corporate_plus_entry_footer(); ?>
            </footer><!-- .entry-footer -->
        </div>
            <?php dynamic_sidebar( 'corporate-plus-subscribe' ); ?>
    </div>
</div>