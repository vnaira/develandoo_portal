<?php
/**
 * Template part for displaying posts.
 *
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
$count = 1;
?>
<div class=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
    <div class="">
        <div class="left-widget">

            <?php if (is_active_sidebar('sidebar-22')) : ?>
                <div id="secondary" class="widget-area" role="complementary">
                    <?php dynamic_sidebar('sidebar-22'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('sidebar-20')) : ?>
                <div id="secondary" class="widget-area" role="complementary">
                    <?php dynamic_sidebar('sidebar-20'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('sidebar-4')) : ?>
                <div id="secondary" class="widget-area" role="complementary">
                    <?php dynamic_sidebar('sidebar-4'); ?>
                </div>
            <?php endif; ?>
        </div>

    </div>
</div>
<div class="front_page_article">
    <article id="<?php echo $count; ?>" <?php post_class('init-animate fadeInDown animated'); ?>>
    <div class=" col-lg-6 col-md-6 col-xs-12">
    <?php
    if (has_post_thumbnail()) {
        echo '<figure>';
        the_post_thumbnail($thumbnail);
        echo "</figure>";
    }
    ?>
</div>

<div class="col-lg-6 col-md-6 col-xs-12">
    <div class="content-wrapper">
        <header class="entry-header">
            <?php the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>

            <?php
            if ('post' === get_post_type()) : ?>
                <div class="entry-meta">
                    <?php corporate_plus_posted_on(); ?>
                </div><!-- .entry-meta -->
                <?php
            endif; ?>
        </header>
        <!-- .entry-header -->

        <div class="entry-content">
            <?php
            the_excerpt();
            ?>
            <a class="read-more" href="<?php the_permalink(); ?> "><?php _e('Read More', ''); ?></a>
            <?php
            wp_link_pages(array(
                'before' => '<div class="page-links">' . esc_html__('Pages:', 'corporate-plus'),
                'after' => '</div>',
            ));
            ?>
        </div>
        <!-- .entry-content -->
        <div class="clearfix"></div>
    </div>
</div>

</article><!-- #post-## -->
</div>
<?php $count++; ?>
