<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
global $corporate_plus_customizer_all_values;
global $post;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('init-animate fadeInDown animated post'); ?>>
    <div class="content-wrapper">
        <?php
        if ($corporate_plus_customizer_all_values['corporate-plus-blog-archive-layout'] == 'left-image' && has_post_thumbnail()) {
            ?>
            <!--post thumbnal options-->
            <div class="post-thumb">
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail(); ?>
                </a>
            </div><!-- .post-thumb-->
            <?php
        } else {
            if ($post->post_type == 'video') { ?>
                <div class="post-thumb">

                    <?php $url = get_post_meta($post->ID, 'link_to_go', true);

                    //  $image = preg_replace('/.+youtube\.com\/watch\?v=(.+)/', ' http://img.youtube.com/vi/$1/sddefault.jpg', $url);

                    parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
                    $image = $my_array_of_vars['v'];
                    ?>
                    <a href="<?php echo $post->post_type == 'video' ? get_post_meta($post->ID, 'link_to_go', true) : get_permalink(); ?>" <?php echo $post->post_type == 'video' ? 'target="_blank"' : '' ?>>
                        <img width="340" height="240"
                             src="http://img.youtube.com/vi/<?php echo trim($image); ?>/sddefault.jpg"
                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="">
                    </a>
                </div>
            <?php }
        }
        ?>
        <div class="container">
        <header class="entry-header search-title">
            <?php the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark" %s>', $post->post_type == 'video' ? get_post_meta($post->ID, 'link_to_go', true) : esc_url(get_permalink()), $post->post_type == 'video' ? 'target="_blank"' : ''), '</a> </h2>'); ?>
            <?php if ('post' === get_post_type()) : ?>
                <div class="entry-meta">
                    <?php corporate_plus_posted_on(); ?>
                </div><!-- .entry-meta -->
            <?php endif; ?>
        </header><!-- .entry-header -->
        <p class="date">by Develandoo admin -<span> <?php echo get_the_date('F j, Y') ?> </span></p>
        <div class="entry-summary entry-content">
            <?php echo excerpt(20); ?>
        </div><!-- .entry-summary -->
        <a href="<?php echo $post->post_type == 'video' ? get_post_meta($post->ID, 'link_to_go', true) : get_permalink(); ?>"
           class="read-more-search" <?php echo $post->post_type == 'video' ? 'target="_blank"' : '' ?>> <?php echo $post->post_type == 'video' ? 'Watch now' : 'Read More' ?> </a>
        </div>
            <footer class="entry-footer">
            <!--<?php corporate_plus_entry_footer(); ?>-->
        </footer><!-- .entry-footer -->
    </div>
    <div class="container  divider">
        <hr/>
    </div>

</article><!-- #post-## -->
