<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
global $corporate_plus_customizer_all_values;
global $post;
?>
<?php
$args = array(
    'showposts' => 3,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true
);

$recent_posts = new WP_Query($args);
?>


<?php
if (is_home() && current_user_can('publish_posts')) : ?>

    <p><?php printf(wp_kses(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'corporate-plus'), array('a' => array('href' => array()))), esc_url(admin_url('post-new.php'))); ?></p>

<?php elseif (is_search()) : ?>
    <?php
    if ($recent_posts->have_posts()) : ?>
        <?php

        /* Start the Loop */
        while ($recent_posts->have_posts()) :$recent_posts->the_post();
            /**                         *
             * Run the loop for the search to output the results.
             * If you want to overload this in a child theme then include a file
             * called content-search.php and that will be used instead.
             */
            get_template_part('template-parts/content', 'search');

        endwhile;
        wp_reset_postdata();
    endif;
else : ?>

    <div class="not-found">
        <div class="col-lg-12 search_in_page">
            <div class="row">
                <?php get_search_form(); ?>
            </div>
        </div>
        <h1 class="text-left col-lg-12">
            <div class="row">404
            </div>
        </h1>

        <h2 class="text-right">oops</h2>

        <h3>Page cannot be found</h3>

        <p>The page you are looking for is temporarily unavailable</p>
    </div>
    <?php

endif; ?>

<!-- .page-content -->
