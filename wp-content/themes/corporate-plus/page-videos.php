<?php
/*Template Name: Videos page Template*/

get_header();

$args = array('numberposts' => '1','post_type' => 'video');
$recent_posts = wp_get_recent_posts($args);
foreach ($recent_posts as $recent){
?>

<div class="container-fluid yellow_space hidden-sm hidden-xs">
    <div class="container padding0">
        <h2 class="text-center top-text padding0 m-left5">TRENDING:
            <a href="<?php echo get_post_meta(get_the_ID(), 'link_to_go', true); ?>" target="_blank">
                <small><?php echo $recent['post_title']; ?></small>
            </a>

        </h2>
    </div>
</div>
<div class="container-fluid yellow_space-mobile hidden-md hidden-lg">
        <h2 class="text-center top-text padding0 m-left5">TRENDING:
            <a href="<?php echo get_post_meta(get_the_ID(), 'link_to_go', true); ?>" target="_blank">
                <small><?php echo $recent['post_title']; ?></small>
            </a>
        </h2>
</div>

<?php }
            wp_reset_query(); ?>


<div class="container m-top50">
    <div class="row">
        <div class=" col-sm-12 col-xs-12">

            <h3 class="col-lg-2 col-md-3 hidden-sm hidden-xs">WATCH NOW</h3>
            <?php
            $arg = array(
                'post_type' => 'video',
                'posts_per_page' => 1
            );
            $obituar_query = new WP_Query($arg);
            while ($obituar_query->have_posts()) :
                $obituar_query->the_post(); ?>

                <h3 class="col-lg-8 col-md-8 col-sm-12 col-xs-12 font18bold hidden-xs hidden-sm">
                    <div class="row">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'link_to_go', true); ?>" target="_blank"
                           class="orange_text">
                            <?php the_title(); ?>
                        </a>
                    </div>
                </h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30 shadowed">
                        <div class="rw">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <iframe width="100%" height="275" src="
                            <?php echo get_post_meta(get_the_ID(), 'url', true); ?>"
                                        frameborder="0"></iframe>
                            </div>
                            <h3 class="col-xs-5 col-sm-4 hidden-lg hidden-md font-mobile">
                                <div class="m-top10">WATCH NOW</div></h3>
                            <a href="<?php echo get_post_meta(get_the_ID(), 'link_to_go', true); ?>" target="_blank"
                               class="orange_text hidden-lg hidden-md col-xs-7 mob-title col-sm-8 text-uppercase">
                                <?php the_title(); ?>
                            </a>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 font14 text-justify">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endwhile;
            wp_reset_postdata();
            ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?php
            $args = array('post_type' => 'video', 'posts_per_page' => 3, 'order' => 'ASC', 'orderby' => 'date');
            $postslist = get_posts($args);
            foreach ($postslist as $post) :
                setup_postdata($post);
                ?>
                <?php
            endforeach;
            wp_reset_postdata();
            ?>
        </div>
    </div><!-- .content-area -->
</div><!-- .content-area -->
<div class="container-fluid yellow_space">
    <div class="container">
        <h2 class="top-text col-lg-12 col-md-12 col-xs-12 center">LATEST VIDEOS</h2>
    </div>
</div>
<div class="container-fluid min-padding0">
    <div class="container text-center min-padding0">
        <div class="row">
        <?php
        $arg = array(
            'post_type' => 'video',
            'posts_per_page' => -1,
            'order' => 'ASC', 'orderby' => 'date'
        );
        $obituar_query = new WP_Query($arg);
        while ($obituar_query->have_posts()) :
            $obituar_query->the_post();
            ?>


            <div class="video-item shadowed">
                <div class="row1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <iframe width="100%" height="275" src="
                            <?php echo get_post_meta(get_the_ID(), 'url', true); ?>"
                                frameborder="0"></iframe>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30 m-bot10 padding_in_mobile">
                        <div class="row">
                            <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center font16 text-uppercase">
                                <div class="">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'link_to_go', true); ?>"
                                       target="_blank">
                                        <?php the_title(); ?>
                                    </a>
                                </div>
                            </h3>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 font16 text-justify">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile;
        wp_reset_postdata();
        ?>
        </div>
    </div>
</div>
<div class="container-fluid yellow_space hidden-sm hidden-xs">
</div>
<?php get_footer(); ?>
