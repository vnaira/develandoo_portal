(function ($) {
    $(window).bind('load', function () {
        var timers = {
            toggle: null,
            position: null
        };
        var headerHeight = 60;
        var button = $('#scroll-button');
        var scrollAnchor = $('#primary');
        var page = $('html,body');

        button.click(function () {
            if (scrollAnchor.length > 0) {
                page.animate({
                    scrollTop: (scrollAnchor.offset().top + 1) - headerHeight
                }, 'fast');
            }
        });

        function toggleBtn() {
            if ($(this).scrollTop() >= (scrollAnchor.offset().top - headerHeight)) {
                button.fadeOut(500);
            } else {
                button.fadeIn(500);
            }
        }

        function defineTimer(timer, callback) {
            if (timers[timer]) {
                clearTimeout(timers[timer]);
            }
            timers[timer] = setTimeout(callback, 1);
        }

        function onDocScroll() {
            defineTimer('toggle', toggleBtn.bind($(this)));
        }

        $(document).on('scroll', onDocScroll);
    });
})(jQuery);