jQuery(document).ready(function ($) {

    jQuery(function () {
        $(".dropdown").hover(
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                $(this).toggleClass('open');
                // $('b', this).toggleClass("custom_caret caret-up");
            },
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                $(this).toggleClass('open');
                // $('b', this).toggleClass("custom_caret caret-up");
            });
    });

    function homeFullScreen() {

        var homeSection = $('#at-banner-slider');
        var windowHeight = $(window).outerHeight();

        if (homeSection.hasClass('home-fullscreen')) {

            $('.home-fullscreen').css('height', windowHeight);
        }
    }

    //make slider full width
    homeFullScreen();

    //window resize
    $(window).resize(function () {
        homeFullScreen();
    });

    $(window).load(function () {
        $('.at-featured-text-slider').show().bxSlider({
            auto: true,
            pager: false,
            mode: 'fade',
            prevText: '<i class="fa fa-angle-left fa-3x"></i>',
            nextText: '<i class="fa fa-angle-right fa-3x"></i>'
        });
        /*parallax*/
        $('.at-parallax').each(function () {
            $(this).parallax('center', 0.2, true);
        });

        /*parallax scolling*/
        $('a[href*=#]').click(function (event) {
            $('html, body').animate({
                //scrollTop: $( $.attr(this, 'href') ).offset().top-$('.at-navbar').height()
            }, 1000);
            event.preventDefault();
        });
        /*bootstrap sroolpy*/
        $("body").scrollspy({target: ".navbar-fixed-top", offset: $('.at-navbar').height() + 50});
    });

    function stickyMenu() {

        var scrollTop = $(window).scrollTop();
        var offset = 0;

        if (scrollTop > offset) {
            $('#navbar').addClass('navbar-small');
            $('.sm-up-container').show();
            $('.scroll-wrap').hide();
        }
        else {
            $('#navbar').removeClass('navbar-small');
            $('.sm-up-container').hide();
            $('.scroll-wrap').show();
        }
    }

    //What happen on window scroll
    stickyMenu();
    $(window).on("scroll", function (e) {
        setTimeout(function () {
            stickyMenu();
        }, 300)
    });

    $('#menu-search').blur(function () {
        var val = document.getElementById('menu-search').value;

        if (val == "" || val.toString() === "") {
            $('i.glyphicon-search').show();
            $('button.icon-right').hide();
        } else {
            $('button.icon-right').css('display', 'inline-block', 'important');
        }

    });

    $('input[type=search]').focus(function () {
        $('i.glyphicon-search').hide();
        $('button.icon-right').show();
        var val = $(this).val();

        if (val.trim() == '') {
            $('.glyphicon-search').prop('disabled', true);
        } else {
            $('.glyphicon-search').prop('disabled', false);
        }
    });

    $('body').delegate('input[type=search]', 'keyup', function () {
        var val = $(this).val();

        if (val.trim() != '') {
            $('.glyphicon-search').prop('disabled', false);
        } else {
            $('.glyphicon-search').prop('disabled', true);
        }
    });

    $('form .remove-icon').on('click', function (e) {
        window.history.back();
    });
	$('#menu-search').focus(function() {
		$('#other-search').addClass('fade');
	});
});
jQuery(window).load(function () {
    jQuery('#loading_image').hide();
});
jQuery(function ($) {
    $('#filter').submit(function () {
        var filter = $(this);
        $.ajax({
            url: filter.attr('action'),
            data: filter.serialize(),
            type: filter.attr('method'),
            beforeSend: function (xhr) {
                filter.find('button').text('Processing...'); // changing the button label
            },
            success: function (data) {
                filter.find('button').text('Filter by date'); // changing the button label back
                //document.write(data);
                $('#response').html(data); // insert data
            }
        });
        return false;
    });
});

/*animation with wow*/
wow = new WOW({
        boxClass: 'init-animate',
        animateClass: 'animated', // default
    }
);
wow.init();