(function ($) {
    $(window).bind('load', function () {

        function _start() {
            var form = $('#stock-form');

            function onSubmit() {
                var submitButton = $('#stock-form button');
                submitButton.prop('disabled', true).text('Sending...');
                $('#server-res').html('');
                var params = {};
                $.each($(form).serializeArray(), function (_, kv) {
                    params[kv.name] = kv.value;
                });
                $.ajax({
                    type: 'POST',
                    url: 'http://develandoo.com/stock-subscribe',
                    data: params,
                    success: function (response) {
                        $('.content-secondary').fadeOut(500);
                        $('.promo-success').text(response.msg).fadeIn(500);
                        setTimeout(closeWindow, 3000);
                    },
                    error: function (err) {
                        if (err.responseText) {
                            err = JSON.parse(err.responseText);
                            if ($.isArray(err)) {
                                $.each(err, function (e) {
                                    $('#server-res').append('<p>' + e.msg + '</p>')
                                })
                            } else {
                                $('#server-res').html('<p>' + err.msg + '</p>')
                            }
                        } else {
                            $('#server-res').html('<p>Something went wrong, please try again. </p>');
                        }
                        submitButton.prop('disabled', false).text('Submit');
                    }
                });
                return false;
            }

            function closeWindow(delay) {
                $('#stock-mini-window').fadeOut(delay || 2000);
            }

            function showWindow(delay) {
                var passed = Cookies.get('sub-pop');
                if (!(passed ? parseInt(passed) : passed)) {
                    var timer = parseInt(sessionStorage.getItem('show-delay'));
                    if (!timer) {
                        timer = 0;
                    }
                    var timerSum = timer + 1;
                    sessionStorage.setItem('show-delay', timerSum);
                    if (timerSum === 1) { // max 1 posts
                        Cookies.set('sub-pop', 1);
                        $('#stock-mini-window').fadeIn(delay || 2000);
                    }
                }
            }

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: 'Please enter your email.'
                    }
                },

                submitHandler: onSubmit
            });


            $('.close-stock').click(closeWindow);

            (function init() {
                showWindow();
            })();
        }

        setTimeout(_start, 10 * 1000);
    });
}(jQuery));