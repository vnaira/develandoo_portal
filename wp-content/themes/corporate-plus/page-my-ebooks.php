<?php
/*Template Name: My Ebooks page Template*/

get_header();

?>
<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs white_space"></div>
<div class="container-fluid padding50">

    <div class=" col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="">
            <div class="left-widget">
            </div>
        </div>
    </div>
    <div class="article-area col-lg-8 col-md-8 col-sm-8 col-xs-12 m-top40" style="min-height: 350px">
        <h1 class="page_title col-lg-12 col-md-12"> <?php the_title(); ?></h1>
        <?php
        $user_id = um_user('ID');
        if ($user_id != 0) {
            $book_list = list_of_books($user_id);

            foreach ($book_list as $item) {
                ?>

                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 book_item">
                        <div class="row">
                            <h5 class="trimed30"><?php
                                if(strlen(get_the_title($item->item_id)) > 35){
                                    echo substr(get_the_title($item->item_id),0,35)." ...";
                                }
                                else echo get_the_title($item->item_id);
                                  ?>
                            </h5>
                            <figure class="book_thumb2">
                                <?php echo get_the_post_thumbnail($item->item_id, array(200,300)); ?>
                            </figure>
                            <p class="book_descr"><?php echo get_the_excerpt($item->item_id); ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <?php } else{?>
           <p> You must be <a href="<?php echo get_site_url().'/login'?>">logged in</a> to see your ebooks. </p>
       <?php }
        ?>

    </div>
</div><!-- .content-area -->

<?php get_footer(); ?>
