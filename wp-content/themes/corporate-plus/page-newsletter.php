<!doctype html>
<html lang="en">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        @media screen and (max-width: 500px) {
            p {
                font-size: 12px !important;
            }

            #unsubscribe {
                margin-right: 10px !important;
            }

            #table-content tr td {
                padding-left: 10px !important;
                padding-right: 10px !important;
            }
            a:-webkit-any-link{
                font-size: 12px;
            }
        }

        @media screen and (max-width: 799px) {
            #x_parent-table {
                width: 100% !important;
            }

            #table-content, #image-section {
                width: 90% !important;
                height: auto !important;
            }
        }
    </style>
</head>
<body>
<div style="font-family:'Open Sans',Helvetica,Arial,sans-serif!important">
    <table align="center" bgcolor="#dbdada" cellspacing="0" id="x_parent-table" width="800">
        <tbody>
        <tr>
            <td style="border: 0 none!important;">
                <table align="center" bgcolor="#6e6b6b" id="x_header-table"
                       style="border-spacing:0; font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important"
                       width="100%">
                    <tbody>
                    <tr>
                        <td align="right" height="20" style="border: 0 none!important; padding:7px;">
                            &nbsp;
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding:20px 0; border: 0 none!important;"><span class="sg-image"
                                                                                       data-imagelibrary="%7B%22width%22%3A%22261%22%2C%22height%22%3A%2242%22%2C%22alignment%22%3A%22center%22%2C%22src%22%3A%22https%3A//develandoo.s3.amazonaws.com/email/develandoo-logo.png%22%2C%22alt_text%22%3A%22develandoo%20logo%22%2C%22link%22%3A%22https%3A//develandoo.com%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"
                                                                                       style="float: none; display: block; text-align: center;"><a
                            href="https://develandoo.com"><img alt="develandoo logo" height="42"
                                                               src="https://develandoo.s3.amazonaws.com/email/develandoo-logo.png"
                                                               style="width: 261px; height: 42px;" width="261"/></a></span></td>
        </tr>
        <tr>
            <td align="center"><span class="sg-image"
                                     data-imagelibrary="%7B%22width%22%3A%22700%22%2C%22height%22%3A%22388%22%2C%22alignment%22%3A%22center%22%2C%22src%22%3A%22https%3A//develandoo.s3.amazonaws.com/email/steelwool-develandoo-a-text.png%22%2C%22alt_text%22%3A%22develandoo%22%2C%22link%22%3A%22%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"
                                     style="float: none; display: block; text-align: center;"><img alt="develandoo"
                                                                                                   height="388"
                                                                                                   src="https://develandoo.s3.amazonaws.com/email/steelwool-develandoo-a-text.png"
                                                                                                   style="width: 700px; height: 388px;"
                                                                                                   width="700"
                                                                                                   id="image-section"/></span>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding:25px 0 20px; border: 0 none!important;">
                <table align="center" id="table-content" bgcolor="#ffffff" cellpadding="0"
                       style="font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important;"
                       width="700">
                    <tbody>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">At <a href="https://develandoo.com/"
                                                                     style="text-decoration:none!important; color: #f2cd32!important; cursor:pointer;">Develandoo</a>
                                we are changing the way that companies build products.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363">There are two distinct ways that we have differentiated
                                ourselves from other software development firms.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363"><b>A lead engineer works on-site from your office</b></p>

                            <p style="margin:0; color:#636363">While most development agencies only work remotely, we do
                                a mix of on-site and remote talent.</p>

                            <p style="margin:0; color:#636363">A lead developer works from your office and is the
                                primary point of contact with your remote development team.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px 0; border: 0 none!important;">
                            <p style="margin:0; color:#636363"><b>You can hire your team full time after the project</b>
                            </p>

                            <p style="margin:0; color:#636363">After your project is complete, you can hire your team as
                                full-time employees.</p>

                            <p style="margin:0; color:#636363">You can relocate all team members to your office or open
                                up subsidiaries in the countries we operate in.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:25px 25px; border: 0 none!important;">
                            <p style="margin:0; color:#636363"><a href="https://develandoo.com/"
                                                                  style="text-decoration:none!important; color: #f2cd32!important; cursor:pointer;">Develandoo</a>&rsquo;s
                                goal is to help you build your product, and your company, in a way that benefits you.
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr align="center">
            <td style="padding-bottom:14px; border: 0 none!important;">
                <table bgcolor="#f2cd32" style="border-radius:5px" width="160">
                    <tbody>
                    <tr align="center">
                        <td style="border: 0 none!important;"><a href="https://develandoo.com/about"
                                                                 style="color:#fff; text-decoration:none!important; line-height:28px"
                                                                 target="_blank">FIND OUT MORE</a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border: 0 none!important;">
                <table align="center" bgcolor="#6e6b6b" height="30"
                       style="border-spacing:0 10px; font-family:'Open Sans',Helvetica,Arial,sans-serif!important; border: 0 none!important;"
                       width="100%">
                    <tbody>
                    <tr>
                        <td align="right" style="color:#dbdada; border: 0 none!important;" width="100%"><a
                                    href="[unsubscribe]" id="unsubscribe"
                                    style="text-decoration: underline!important; color: #b8b8b8!important;font-size:12px;margin-right:20px;">Click
                                here if you don&#39;t want to receive emails from us </a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
