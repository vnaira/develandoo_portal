<?php

function clone_post($post_id) {
    global $wpdb;
    // If this is just a revision, don't send the email.
    if (wp_is_post_revision($post_id)) {
        return;
    }
    $content_post = get_post($post_id);
    if ($content_post->post_status != 'publish') {
        return;
    }


    $data = array(
        "ID" => $content_post->ID,
        "post_author" => 1,
        "post_date" => $content_post->post_date,
        "post_date_gmt" => $content_post->post_date_gmt,
        "post_content" => $content_post->post_content,
        "post_title" => $content_post->post_title,
        "post_excerpt" => $content_post->post_excerpt,
        "post_status" => $content_post->post_status,
        "post_name" => $content_post->post_name,
        "post_modified" => $content_post->ost_modified,
        "post_modified_gmt" => $content_post->post_modified_gmt,
        "post_parent" => $content_post->post_parent,
        "post_type" => $content_post->post_type,
        "post_mime_type" => $content_post->post_mime_type,
        "comment_count" => $content_post->comment_count
    );
    $clonDb = new wpdb('naira_blog', 'root1', 'naira_portal', 'g-hoster.com');
    $clonDb->delete('port_posts', array('ID' => $post_id), array('%d'));
    $clonDb->insert('port_posts', $data);

    clone_relations($post_id);
    clone_post_meta($post_id);

    // Send email to admin.
}

function clone_post_meta($post_id) {
    global $wpdb;
    $clonDb = new wpdb('naira_blog', 'root1', 'naira_portal', 'g-hoster.com');
    $post_metas = get_post_meta($post_id);
    $clonDb->delete('port_postmeta', array('post_id' => $post_id), array('%d'));
    if ($post_metas) {
        foreach ($post_metas as $meta) {
            $clonDb->insert(
                    'port_postmeta', array(
                'post_id' => $post_id,
                'meta_key' => $meta->meta_key,
                'meta_value' => $meta->meta_value
                    ), array(
                '%d',
                '%s',
                '%s'
                    )
            );
        }
    }
}

function clone_relations($post_id) {
    global $wpdb;
    $clonDb = new wpdb('naira_blog', 'root1', 'naira_portal', 'g-hoster.com');
    $relations = $wpdb->get_results('SELECT * FROM bltxt_term_relationships where object_id =' . $post_id);
    $clonDb->delete('port_term_relationships', array('object_id' => $post_id), array('%d'));
    if ($relations) {
        foreach ($relations as $relation) {
            $clonDb->insert(
                    'port_term_relationships', array(
                'object_id' => $post_id,
                'term_taxonomy_id' => $relation->term_taxonomy_id,
                'term_order' => $relation->term_order,
                    ), array(
                '%d',
                '%s',
                '%s'
                    )
            );
        }
    }
}


function action_clone_term($term_id, $tt_id, $taxonomy) {
    global $wpdb;
    $clonDb = new wpdb('naira_blog', 'root1', 'naira_portal', 'g-hoster.com');
    $term = get_term($term_id, $taxonomy);
    $clonDb->insert(
            'port_terms', array(
        'term_id' => $term->term_id,
        'name' => $term->name,
        'slug' => $term->slug,
        'term_group' => $term->term_group
            ), array(
        '%d',
        '%s',
        '%s'
            )
    );
    $clonDb->insert(
            'port_term_taxonomy', array(
        'term_id' => $term->term_id,
        'term_taxonomy_id' => $term->term_taxonomy_id,
        'taxonomy' => $term->taxonomy,
        'description' => $term->description,
        'parent' => $term->parent,
        'count' => $term->count
            ), array(
        '%d',
        '%d',
        '%s',
        '%s',
        '%d',
        '%d',
            )
    );
}

add_action('created_term', 'action_clone_term', 10, 3);
add_action('add_attachment', 'clon_attachment');
add_action('save_post', 'clone_post');
 