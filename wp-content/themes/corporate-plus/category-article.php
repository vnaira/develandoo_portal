<?php
/*Template Name: Article Template*/

get_header();

?>
<?php
$args = array('numberposts' => '1');
$recent_posts = wp_get_recent_posts($args);
foreach ($recent_posts as $recent){
//echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';


?>
<div class="container-fluid yellow_space">
    <div class="container padding0">
        <h2 class="text-center top-text padding0 m-left5" >HOT STORY:
            <small><?php echo $recent["post_title"]; ?></small>
            <?php }
            wp_reset_query(); ?>
        </h2>
    </div>
</div>
<div class="container m-top50">
    <div class="">
        <div class=" col-lg-6 col-md-7 col-sm-12 col-xs-12">
            <?php if (is_active_sidebar('sidebar-22')) : ?>
            <div id="secondary" class="widget-area top-dropdown text-uppercase" role="complementary">
                <?php dynamic_sidebar('sidebar-22'); ?>
            </div>
        </div>
        <?php endif; ?>
        <?php if (is_active_sidebar('sidebar-19')) : ?>
        <div id="second" class="widget-area top-dropdown text-uppercase" role="complementary">
            <?php dynamic_sidebar('sidebar-19'); ?>
        </div>
    </div>
    <?php endif; ?>
</div>
</div>
</div>
<div class="container m-top50 min-padding0">
    <div class="">
        <?php $args = array('numberposts' => 1);
        $postslist = get_posts($args);
        foreach ($postslist as $post) :
            setup_postdata($post);
            ?>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a href="<?php the_permalink(); ?>">
                <figure class="top-thumb center-block">
                    <?php
                    the_post_thumbnail(array(200, 400)); ?>
                </figure>
            </div>
</a>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h2 class="font20bold mm-top20"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>
                <span class="dates font14 title-wout">
                <?php $authr = get_the_author();
                if ($authr != "Develandoo Admin") {
                    ?>
                    by <?php echo get_the_author(); ?>
                <?php } ?>
                    </span>
                <div class="text_exc_top m-top50 m-bot20">
                    <?php the_excerpt(); ?>
                </div>
                <a href="<?php the_permalink(); ?>">READ THE ARTICLE</a>

            </div>

            <?php
        endforeach;
        wp_reset_postdata();
        ?>
    </div>
</div>


<div class="container-fluid yellow_space">
    <div class="container">
        <h2 class="top-text col-lg-12 col-md-12 col-xs-12">LATEST ARTICLES</h2>
    </div>
</div>

<div id="" class="container">
    <div class="row">
        <div class="article-area col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top30 min-padding0">
            <?php
            $args = array('posts_per_page' => 6, 'order' => 'DESC', 'orderby' => 'date');
            $postslist = get_posts($args);
            foreach ($postslist as $post) :
                setup_postdata($post);
                ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 shadowed">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                        <a href="<?php echo $post->guid; ?>">
                            <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>"
                                 class="img-responsive center-block img-fixed-height" width="500" alt=""/>
                                 </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top10">
                        <h2 class="text-center trimed"><a
                                href="<?php echo $post->guid; ?>" class="trimed">
                                <?php $string = (strlen($post->post_title) > 70) ? substr($post->post_title, 0, 67) . '...' : $post->post_title; ?>
                                 <?php echo $string; ?>
                                <?php //echo $post->post_title; ?>
                                </a>
                        </h2>
                        <div class="row m-top30">
                            <div class="text_exc m-bot60">
                                <?php
                                the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                </div>


                <?php
            endforeach;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>

<div class="container-fluid yellow_space">
    <div class="container">
        <h2 class="top-text col-lg-12 col-md-12 col-xs-12">UP & TRENDING</h2>
    </div>
</div>

<div id="" class="container">
    <div class="row">
        <div class="article-area col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top50 min-padding0">
            <?php
            $args = array('posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date', 'meta_key' => 'trending',
                'meta_value' => 'true');
            $postslist = get_posts($args);
            foreach ($postslist as $post) :
                setup_postdata($post);
                ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 shadowed">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                        <a href="<?php echo $post->guid; ?>">
                            <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>"
                                 class="img-responsive center-block" width="500" alt=""/>
                                 </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-top10 article-item">
                        <h2 class="text-center trimed">
                            <a href="<?php echo $post->guid; ?>">
                                <?php $string = (strlen($post->post_title) > 80) ? substr($post->post_title, 0, 77) . '...' : $post->post_title; ?>
                                                                 <?php echo $string; ?>
                            </a>
                        </h2>
                        <div class="row m-top30">
                            <div class="text_exc m-bot60">
                                <?php
                                the_excerpt(10); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
