<?php
get_header();

?>
<div class="container-fluid yellow_space">
    <div class="container">
        <h2 class="text-center top-text text-uppercase mobile-text-center">book of the month :
            <small>practical c++ metaprogramming
            </small>
        </h2>
    </div>
</div>

<div class="container m-top60 m-bot40 m-top0">
    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-push-1 col-md-push-1 col-xs-pull-0">
        <div class="row">
            <?php
            $args = array(
                'numberposts' => 1,
                'post_type' => 'ebook',
                'meta_key' => 'book_month',
                'meta_value' => 'true'
            );

            $the_query = new WP_Query($args);
            ?>
            <?php if ($the_query->have_posts()): ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="font18bold orange-text">
                            <?php the_title(); ?>
                        </h3>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 m-top40 m-top0">
                        <figure class="book_thumb_month center-block">
                            <?php
                            the_post_thumbnail(array(200, 400)); ?>
                        </figure>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 m-top40 m-top0 font-mobile">
                        <?php the_content(); ?>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-7 col-lg-push-5 col-md-push-5 col-md-7 col-sm-12 col-xs-12">
                            <div class="row bbtn mobile-place">
                                 <a href="" data-id="<?php echo  get_the_ID() ?>" data-file-type="url_pdf" class="new-link">PDF</a>
                                 <a href="" data-id="<?php echo  get_the_ID() ?>" data-file-type="url_doc" class="new-link">DOC</a>
                            </div>
                            <div class="container">

                                <!-- Trigger the modal with a button -->
                                <button type="button" hidden id="email-modal" data-toggle="modal" data-target="#myModal">Open</button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal" style="z-index: 20000" role="dialog">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h5 class="modal-title" style="font-size: 14px;text-align: center;text-transform: initial;">Please enter your email to download this book.</h5>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="e-mail" style="font-size: 15px;">E-mail address:</label>
                                                    <input type="email" class="form-control" id="e-mail">
                                                </div>
                                                <div hidden class="alert alert-danger">

                                                </div>
                                                <div hidden class="alert alert-success">
                                                    Please check your email to download the book.
                                                </div>
                                                <input type="hidden" id="file-id" value="">
                                                <input type="hidden" id="file-type" value="">
                                            </div>
                                            <div class="modal-footer">
                                                <a href="" style="margin-right: 70px;outline: none" class="new-link">Send</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</div>

<div class="container-fluid yellow_space">
    <div class="container">
        <h2 class="top-text col-lg-12 col-md-12 col-xs-12 m-left30">LATEST BOOKS</h2>
    </div>
</div>
<div class="container-fluid min-padding0">

    <div class="container m-top60 min-padding0 m-top0">
        <div class="">
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            $query = new WP_Query(array(
                'post_type' => 'ebook',
                'paged' => $paged
            )); ?>
            <div class="hidden-lg hidden-md m-bot10">
                <?php wp_pagenavi(array('query' => $query)); ?>
            </div>

            <?php while ($query->have_posts()) :
                $query->the_post(); ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 min-padding0 shadowed not-shadowed">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 book_item m-bot0">
                        <div class="row">
                        <div class="row">
                            <h3 class="big-title text-center col-lg-10 col-md-10 col-sm-12 col-xs-8 col-xs-push-2 col-lg-push-1 col-md-push-1 ">
                                <div class="row">
                                    <?php echo substr(strip_tags(the_title(), 0, 30)); ?></div>
                            </h3>
                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-push-1 col-md-push-1 book_item book_item_mobile m-bot0">
                                <figure style="text-align: center">
                                    <?php
                                    the_post_thumbnail(array(200, 400)); ?>
                                </figure>
                                <div class="book_descr m-top30 book_item_mobile_content">
                                    <div class="font-mobile"><?php the_content(); ?></div>
                                </div>
                            </div>
                            <div
                                class="col-lg-10 col-md-10 col-lg-push-1 col-md-push-1 col-sm-12 col-xs-12 text-center">
                                <div class="bbtn_">
                                    <div class="row">
                                        <a href="" data-id="<?php echo  get_the_ID() ?>" data-file-type="url_pdf" class="new-link">PDF</a>
                                        <a href="" data-id="<?php echo  get_the_ID() ?>" data-file-type="url_doc" class="new-link">DOC</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <?php
            endwhile; ?>
            <div class="hidden-sm hidden-xs">
                <?php wp_pagenavi(array('query' => $query)); ?>
            </div>
            <?php wp_reset_query();
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
