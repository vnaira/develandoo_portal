<form action="<?php echo esc_url(home_url()) ?>" class="searchform pull-left" id="searchform" method="get"
             role="search">
    <i class="glyphicon glyphicon-search icon-left hidden-sm hidden-xs"></i>
    <input type="search" class="form-control search" id="menu-search" required name="s" placeholder="Search"  data-swplive="true"
           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search'"
           value="<?php echo esc_attr(get_search_query()); ?>"/>
    <button class="glyphicon glyphicon-search icon-right " hidden type="submit"
            id="searchsubmit"></button>
</form>