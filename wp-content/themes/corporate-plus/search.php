<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package AcmeThemes
 * @subpackage Corporate Plus
 */
global $corporate_plus_customizer_all_values;
get_header();
global $post;
$cat = filter_input(INPUT_GET, 'cat', FILTER_SANITIZE_SPECIAL_CHARS);
$order = filter_input(INPUT_GET, 'order', FILTER_SANITIZE_SPECIAL_CHARS) ?: 'ASC';
$date = filter_input(INPUT_GET, 'date', FILTER_SANITIZE_SPECIAL_CHARS);
$filter_cat = '';
if ($cat == 'tech') {
    $filter_cat = "25,7,6,5,23,27,26,22,12";
} elseif ($cat == 'event') {
    $filter_cat = '80';
} elseif ($cat == 'industry') {
    $filter_cat = '-80,-25,-7,-6,-5,-23,-27,-26,-22,-12';
}
?>
<?php
$s = filter_var(trim($s), FILTER_SANITIZE_STRING);
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    's' => $s,
    'posts_per_page' => 6,
    'paged' => $paged,
    'post_type' => array( 'post', 'video')
);
if ($filter_cat) {
    $args['cat'] = $filter_cat;
}
if ($order) {
    $args['orderby'] = 'date';
    $args['order'] = $order;
}

if ($date == '' || $date == 'all') {
    $args['date_query'] = array();
} elseif ($date == "week") {
    $args['date_query'] = array(
        'after' => '1 week ago'
    );
} elseif ($date == "month") {
    $args['date_query'] = array(
        'after' => '1 month ago'
    );
} elseif ($date == "year") {
    $args['date_query'] = array(
        'after' => '1 year ago'
    );
} elseif ($date == "today") {
    $args['date_query'] = array(
        array(
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d')
        ),
    );
}
wp_reset_query();
$allsearch = new WP_Query($args);
?>
    <div class="container-fluid search-page">
        <div class="container">
            <form action="<?php echo esc_url(home_url()) ?>" class="form-group" id="searchform" method="get"
                  role="search">
                <div class="pull-left">
                    <button class="glyphicon glyphicon-search icon-left hidden-md hidden-lg"></button>
                    <input type="search" class="form-control search" name="s" onfocus="this.placeholder = ''"
                           data-swplive="true" data-swpconfig="default"
                           value="<?php echo esc_attr(get_search_query()); ?>" onblur="this.placeholder = 'Searcha'"/>
                    <button class="glyphicon glyphicon-search icon-right  hidden-sm hidden-xs "></button>
                </div>
                <button type="button" class="glyphicon glyphicon glyphicon-remove pull-right remove-icon "></button>
            </form>
        </div>
    </div>
    <div class="container-fluid search-filter">
        <div class="container">
            <ul class="list-inline list-unstiled nav-filter">
                <li>Filter by:</li>
                <li><a href="/?s=<?php echo $s ?>&cat=tech" class="<?= $cat == 'tech' ? 'active' : '' ?>">
                        Technology</a></li>
                <li><a href="/?s=<?php echo $s ?>&cat=event" class="<?= $cat == 'event' ? 'active' : '' ?>"> Events</a>
                </li>
                <li><a href="/?s=<?php echo $s ?>&cat=industry" class="<?= $cat == 'industry' ? 'active' : '' ?>">Industry</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 loading">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/loading.gif" alt="" id="loading_image"
             class="center-block">
    </div>
    <div class="wrapper inner-main-title init-animate fadeInDown animated container-fluid">
        <header class="container text-left  header-section">
            <?php if ($allsearch->found_posts):
                ?>
            <form class="col-md-6 col-xs-12 col-md-push-4 search-ask-desc">
                <div class="row">
                    <select id="filter_order" class="selectpicker"
                            onchange="var о=document.getElementById('filter_order');window.location.href='?s=<?php echo $s ?><?php echo $date ? '&date=' . $date : '' ?><?php echo $cat ? '&cat=' . $cat : '' ?>&order='+о.options[о.selectedIndex].value">
                        <option value="ASC" <?php selected($order, 'ASC'); ?>>Ascending</option>
                        <option value="DESC" <?php selected($order, 'DESC'); ?>>Descending</option>
                    </select>

                    <select id="filter_date" class="selectpicker"
                            onchange="var d=document.getElementById('filter_date');window.location.href='?s=<?php echo $s ?><?php echo $order ? '&order=' . $order : '' ?><?php echo $cat ? '&cat=' . $cat : '' ?>&date='+d.options[d.selectedIndex].value">
                        <option value="all" <?php selected($date, 'all'); ?>>All-time</option>
                        <option value="year" <?php selected($date, 'year'); ?>>Last year</option>
                        <option value="month" <?php selected($date, 'month'); ?>>Last month</option>
                        <option value="week" <?php selected($date, 'week'); ?>>Last week</option>
                        <option value="today" <?php selected($date, 'today'); ?>>Today</option>
                    </select>
                </div>
            </form>
            <?php endif;?>
            <?php if ($allsearch->found_posts): ?>
                <h1 class="page-title col-md-6 col-xs-12 col-md-pull-6 "><?php printf(esc_html__('Search results for: %s', 'corporate-plus'), '<span>"' . get_search_query() . '"</span>'); ?> </h1>
            <?php else: ?>
                <h1 class="page-title title-no-result col-xs-12"><?php printf(esc_html__('Sorry, we couldn’t find matches for %s', 'corporate-plus'), '<span>"' . get_search_query() . '"</span>'); ?> </h1>
            <?php endif; ?>

            <?php if ($allsearch->found_posts): ?>
                <p class="found-posts col-xs-12 "><span><?php
                        $key = esc_html($s);
                        $count = $allsearch->found_posts;
                        _e('');
                        echo $count . ' ';
                        ?></span> posts found </p>
            <?php else: ?>

            <?php endif; ?>
        </header>
    </div>
    <div id="content" class="container">
        <?php
        if (1 == $corporate_plus_customizer_all_values['corporate-plus-show-breadcrumb']) {
            corporate_plus_breadcrumbs();
        }
        ?>
        <section id="primary" class="content-area">
            <main id="main" class="site-main" role="main"><?php
                if (!$allsearch->found_posts):?>
                    <hr/>
                    <p class="no-result">Can’t find what you are looking for? Try these awesome publications instead</p>
                <?php endif; ?>
                <?php
                if ($allsearch->have_posts()) : ?>
                    <?php

                    /* Start the Loop */
                    while ($allsearch->have_posts()) :$allsearch->the_post();
                        /**
                         * Run the loop for the search to output the results.
                         * If you want to overload this in a child theme then include a file
                         * called content-search.php and that will be used instead.
                         */
                        get_template_part('template-parts/content', 'search');

                    endwhile;
                    wp_reset_postdata();

                else :
                    wp_reset_postdata();
                    get_template_part('template-parts/content', 'none');

                endif; ?>
            </main><!-- #main -->
        </section><!-- #primary -->
        <?php dynamic_sidebar('corporate-plus-search-bar'); ?>
        <?php
        if ($allsearch->found_posts > $allsearch->post_count)
            wp_pagenavi(array('query' => $allsearch));
        wp_reset_postdata();
        ?>
    </div><!-- #content -->
<?php get_footer(); ?>