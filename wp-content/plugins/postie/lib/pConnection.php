<?php

abstract class pConnection {

    protected $username;
    protected $host;
    protected $password;
    protected $port;
    protected $secure;
    protected $timeout;

    public function __construct($type, $host, $username, $password, $port, $secure = FALSE, $timeout = NULL) {

        $valid_types = array('imap', 'pop3');
        if (!in_array($type, $valid_types)) {
            throw new fProgrammerException('The mailbox type specified, %1$s, in invalid. Must be one of: %2$s.', $type, join(', ', $valid_types));
        }

        if ($secure && !extension_loaded('openssl')) {
            throw new fEnvironmentException('A secure connection was requested, but the %s extension is not installed', 'openssl');
        }

        if ($timeout === NULL) {
            $timeout = ini_get('default_socket_timeout');
        }

        $this->type = $type;
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->port = $port;
        $this->secure = $secure;
        $this->timeout = $timeout;
    }

    abstract function read($expect = NULL);

    abstract function write($command, $expected = null);

    abstract function close();

    abstract function open();

    abstract function isPersistant();
}
