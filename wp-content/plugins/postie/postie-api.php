<?php

/*
 * These are the only official public methods for accessing postie functionality
 */

function lookup_taxonomy($termid) {
    return postie_lookup_taxonomy_name($termid);
}

function lookup_category($trial_category, $category_match) {
    return postie_lookup_category_id($trial_category, $category_match);
}

function RemoveExtraCharactersInEmailAddress($address) {
    return postie_RemoveExtraCharactersInEmailAddress($address);
}

function EchoError($v) {
    postie_log_error($v);
    do_action('postie_log_debug', $v);
}

function DebugDump($v) {
    if (defined('POSTIE_DEBUG') && true == POSTIE_DEBUG) {
        postie_log_onscreen(print_r($v, true));
    }
    do_action('postie_log_debug', print_r($v, true));
}

function DebugEcho($v, $force = false) {
    if ($force || (defined('POSTIE_DEBUG') && true == POSTIE_DEBUG)) {
        postie_log_onscreen($v);
    }
    do_action('postie_log_debug', $v);
}
