<?php
/*
  $Id: postie-functions.php 1618962 2017-03-21 22:42:36Z WayneAllen $
 */

class PostiePostModifiers {

    var $PostFormat = 'standard';

    function apply($postid) {

        if ($this->PostFormat != 'standard') {
            set_post_format($postid, $this->PostFormat);
        }
    }

}

if (!function_exists('boolval')) {

    function boolval($val) {
        return (bool) $val;
    }

}

if (!function_exists('mb_str_replace')) {
    if (function_exists('mb_split')) {

        function mb_str_replace($search, $replace, $subject, &$count = 0) {
            if (!is_array($subject)) {
                // Normalize $search and $replace so they are both arrays of the same length
                $searches = is_array($search) ? array_values($search) : array($search);
                $replacements = array_pad(is_array($replace) ? array_values($replace) : array($replace), count($searches), '');

                foreach ($searches as $key => $search) {
                    $parts = mb_split(preg_quote($search), $subject);
                    $count += count($parts) - 1;
                    $subject = implode($replacements[$key], $parts);
                }
            } else {
                // Call mb_str_replace for each subject in array, recursively
                foreach ($subject as $key => $value) {
                    $subject[$key] = mb_str_replace($search, $replace, $value, $count);
                }
            }

            return $subject;
        }

    } else {

        function mb_str_replace($search, $replace, $subject, &$count = null) {
            return str_replace($search, $replace, $subject, $count);
        }

    }
}

function postie_environment($force_display = false) {
    DebugEcho("Postie Version: " . POSTIE_VERSION, $force_display);
    DebugEcho("Wordpress Version: " . get_bloginfo('version'), $force_display);
    DebugEcho("PHP Version: " . phpversion(), $force_display);
    DebugEcho("OS: " . php_uname(), $force_display);
    DebugEcho("POSTIE_DEBUG: " . (IsDebugMode() ? "On" : "Off"), $force_display);
    DebugEcho("Error log: " . ini_get('error_log'), $force_display);
    DebugEcho("TMP dir: " . get_temp_dir(), $force_display);
    DebugEcho("Postie is in " . plugin_dir_path(__FILE__), $force_display);

    if (function_exists('curl_version')) {
        $cv = curl_version();
        DebugEcho("cURL version: " . $cv['version'], $force_display);
    }
    if (defined('ALTERNATE_WP_CRON') && ALTERNATE_WP_CRON) {
        DebugEcho("Alternate cron is enabled", $force_display);
    }

    if (defined('DISABLE_WP_CRON') && DISABLE_WP_CRON) {
        DebugEcho("WordPress cron is disabled. Postie will not run unless you have an external cron set up.", $force_display);
    }

    DebugEcho("Cron: " . (defined('DISABLE_WP_CRON') && DISABLE_WP_CRON === true ? "Off" : "On"), $force_display);
    DebugEcho("Alternate Cron: " . (defined('ALTERNATE_WP_CRON') && ALTERNATE_WP_CRON === true ? "On" : "Off"), $force_display);

    if (defined('WP_CRON_LOCK_TIMEOUT') && WP_CRON_LOCK_TIMEOUT === true) {
        DebugEcho("Cron lock timeout is:" . WP_CRON_LOCK_TIMEOUT, $force_display);
    }
}

function postie_disable_revisions($restore = false) {
    global $_wp_post_type_features, $_postie_revisions;

    if (!$restore) {
        $_postie_revisions = false;
        if (isset($_wp_post_type_features['post']) && isset($_wp_post_type_features['post']['revisions'])) {
            $_postie_revisions = $_wp_post_type_features['post']['revisions'];
            unset($_wp_post_type_features['post']['revisions']);
        }
    } else {
        if ($_postie_revisions) {
            $_wp_post_type_features['post']['revisions'] = $_postie_revisions;
        }
    }
}

/* this function is necessary for wildcard matching on non-posix systems */
if (!function_exists('fnmatch')) {

    function fnmatch($pattern, $string) {
        $pattern = strtr(preg_quote($pattern, '#'), array('\*' => '.*', '\?' => '.', '\[' => '[', '\]' => ']'));
        return preg_match('/^' . strtr(addcslashes($pattern, '/\\.+^$(){}=!<>|'), array('*' => '.*', '?' => '.?')) . '$/i', $string);
    }

}

function postie_log_onscreen($data) {
    if (php_sapi_name() == "cli") {
        print( "$data\n");
    } else {
        //flush the buffers
        while (ob_get_level() > 0) {
            ob_end_flush();
        }
        print( "<pre>" . htmlspecialchars($data) . "</pre>\n");
    }
}

function postie_log_error($v) {
    postie_log_onscreen($v);
    error_log("Postie [error]: $v");
}

function postie_log_debug($data) {
    error_log("Postie [debug]: $data");
}

function tag_Date(&$content, $message_date) {
    //don't apply any offset here as it is accounted for later
    $html = LoadDOM($content);
    if ($html !== false) {
        $es = $html->find('text');
        //DebugEcho("tag_Date: html " . count($es));
        foreach ($es as $e) {
            //DebugEcho("tag_Date: " . trim($e->plaintext));
            $matches = array();
            if (1 === preg_match("/^date:\s?(.*)$/imu", trim($e->plaintext), $matches)) {
                $possibledate = trim($matches[1]);
                DebugEcho("tag_Date: found date tag $matches[1]");
                $newdate = strtotime($possibledate);
                if (false !== $newdate) {
                    $t = date("H:i:s", $newdate);
                    DebugEcho("tag_Date: original time: $t");

                    $format = "Y-m-d";
                    if ($t != '00:00:00') {
                        $format .= " H:i:s";
                    }
                    $message_date = date($format, $newdate);
                    $content = str_replace($matches[0], '', $content);
                    break;
                } else {
                    DebugEcho("tag_Date: failed to parse '$possibledate' ($newdate)");
                }
            }
        }
    } else {
        DebugEcho("tag_Date: not html");
    }
    return $message_date;
}

function tag_CustomImageField($post_ID, $email, $config) {
    if ($config['custom_image_field']) {
        DebugEcho("Saving custom image post_meta");

        foreach (array_merge($email['attachment'], $email['inline'], $email['related']) as $attachment) {
            add_post_meta($post_ID, 'image', $attachment['wp_filename']);
            DebugEcho("Saving custom attachment '{$attachment['wp_filename']}'");
        }
    }
}

function filter_AttachmentTemplates($content, $mimeDecodedEmail, $post_id, $config) {

    $matches = array();
    $addimages = !($config['custom_image_field'] || $config['auto_gallery'] || preg_match("/\[gallery[^\[]*\]/u", $content, $matches));
    $fiid = -1;

    DebugEcho("filter_AttachmentTemplates: looking for attachments to add to post");
    $html = '';
    if (!$config['include_featured_image']) {
        //find the image to exclude
        $fiid = get_post_thumbnail_id($post_id);
    }
    DebugEcho("filter_AttachmentTemplates: featured image: $fiid");

    DebugEcho("filter_AttachmentTemplates: # attachments: " . count($mimeDecodedEmail['attachment']));
    foreach ($mimeDecodedEmail['attachment'] as $attachment) {
        DebugEcho("filter_AttachmentTemplates: image: " . $attachment['wp_filename']);
        $skip = ($fiid == $attachment['wp_id'] && !$config['include_featured_image']) || $attachment['exclude'];
        if (!$skip) {
            if (!$addimages && $attachment['primary'] == 'image') {
                DebugEcho("filter_AttachmentTemplates: skip image " . $attachment['wp_filename']);
            } else {
                $template = $attachment['template'];
                if ($config['images_append']) {
                    $template = apply_filters('postie_place_media_after', $template, $attachment['wp_id']);
                } else {
                    $template = apply_filters('postie_place_media_before', $template, $attachment['wp_id']);
                }
                $template = mb_str_replace('{CAPTION}', '', $template);
                DebugEcho("filter_AttachmentTemplates: post filter '$template'");
                $html .= $template;
            }
        } else {
            DebugEcho("filter_AttachmentTemplates: skip attachment " . $attachment['wp_filename']);
        }
    }

    DebugEcho("filter_AttachmentTemplates: # inline: " . count($mimeDecodedEmail['inline']));
    DebugEcho("filter_AttachmentTemplates: # related: " . count($mimeDecodedEmail['related']));
    foreach (array_merge($mimeDecodedEmail['inline'], $mimeDecodedEmail['related']) as $attachment) {
        DebugEcho("filter_AttachmentTemplates: image: " . $attachment['wp_filename']);
        $skip = ($fiid == $attachment['wp_id'] && !$config['include_featured_image']) || $attachment['exclude'];
        if (!$skip) {
            if (!$addimages && $attachment['primary'] == 'image') {
                DebugEcho("filter_AttachmentTemplates: skip image (alt) " . $attachment['wp_filename']);
            } else {
                $template = $attachment['template'];
                if ($config['images_append']) {
                    $template = apply_filters('postie_place_media_after', $template, $attachment['wp_id']);
                } else {
                    $template = apply_filters('postie_place_media_before', $template, $attachment['wp_id']);
                }
                $template = mb_str_replace('{CAPTION}', '', $template);
                DebugEcho("filter_AttachmentTemplates: post filter (alt) '$template'");
                $html .= $template;
            }
        } else {
            DebugEcho("filter_AttachmentTemplates: skip attachment (alt) " . $attachment['wp_filename']);
        }
    }

    if ($config['images_append']) {
        $content .= $html;
    } else {
        $content = $html . $content;
    }


    //strip featured image from html
    if ($fiid > 0 && $config['prefer_text_type'] == 'html' && !$config['include_featured_image']) {
        DebugEcho("filter_AttachmentTemplates: remove featured image from post");
        $html = str_get_html($content);
        if ($html) {
            $elements = $html->find('img[src=' . wp_get_attachment_url($fiid) . ']');
            foreach ($elements as $e) {
                DebugEcho('filter_AttachmentTemplates: outertext:' . $e->outertext);
                $e->outertext = '';
            }
            $content = $html->save();
        }
    }

    $imagecount = 0;
    foreach ($mimeDecodedEmail['attachment'] as $attachment) {
        DebugEcho("filter_AttachmentTemplates: attachment mime primary: {$attachment['primary']}");
        if ($attachment['primary'] == 'image' && $attachment['exclude'] == false) {
            $imagecount++;
        }
    }
    if ($config['prefer_text_type'] == 'plain') {
        foreach (array_merge($mimeDecodedEmail['inline'], $mimeDecodedEmail['related']) as $attachment) {
            DebugEcho("filter_AttachmentTemplates: attachment mime primary: {$attachment['primary']}");
            if ($attachment['primary'] == 'image' && $attachment['exclude'] == false) {
                $imagecount++;
            }
        }
    }
    DebugEcho("filter_AttachmentTemplates: image count $imagecount");

    if (($imagecount > 0) && $config['auto_gallery']) {
        $linktype = strtolower($config['auto_gallery_link']);
        DebugEcho("filter_AttachmentTemplates: Auto gallery: link type $linktype");
        postie_show_filters_for('postie_gallery');
        if ($linktype == 'default') {
            $imageTemplate = apply_filters('postie_gallery', '[gallery]', $post_id);
        } else {
            $imageTemplate = apply_filters('postie_gallery', "[gallery link='$linktype']", $post_id);
        }
        DebugEcho("filter_AttachmentTemplates: Auto gallery: template '$imageTemplate'");
        if ($config['images_append']) {
            $content .= "\n$imageTemplate";
            DebugEcho("filter_AttachmentTemplates: Auto gallery: append");
        } else {
            $content = "$imageTemplate\n" . $content;
            DebugEcho("filter_AttachmentTemplates: Auto gallery: prepend");
        }
    } else {
        DebugEcho("filter_AttachmentTemplates: Auto gallery: none");
    }

    return $content;
}

function postie_create_post($poster, $mimeDecodedEmail, $post_id, &$is_reply, $config, $postmodifiers) {
    DebugEcho("postie_create_post: prefer_text_type: " . $config['prefer_text_type']);

    $fulldebug = IsDebugMode();
    $fulldebugdump = false;

    if (array_key_exists('message-id', $mimeDecodedEmail['headers'])) {
        DebugEcho("Message Id is :" . htmlentities($mimeDecodedEmail['headers']['message-id']));
        if ($fulldebugdump) {
            DebugDump($mimeDecodedEmail);
        }
    }

    if ($fulldebugdump) {
        DebugDump($mimeDecodedEmail);
    }

    postie_save_attachments($mimeDecodedEmail, $post_id, $poster, $config);

    $content = postie_get_content($mimeDecodedEmail, $config);

    $subject = postie_get_subject($mimeDecodedEmail, $content, $config);

    $content = filter_RemoveSignature($content, $config);
    if ($fulldebug) {
        DebugEcho("post sig: $content");
    }

    $post_excerpt = tag_Excerpt($content, $config);
    if ($fulldebug) {
        DebugEcho("post excerpt: $content");
    }

    $postAuthorDetails = getPostAuthorDetails($mimeDecodedEmail);
    if ($fulldebug) {
        DebugEcho("post author: $content");
    }

    $message_date = NULL;
    if (array_key_exists("date", $mimeDecodedEmail['headers']) && !empty($mimeDecodedEmail['headers']['date'])) {
        DebugEcho("date header: {$mimeDecodedEmail['headers']['date']}");
        $message_date = $mimeDecodedEmail['headers']['date'];
        DebugEcho("decoded date: $message_date");
    } else {
        DebugEcho("date header missing");
    }
    $message_date = tag_Date($content, $message_date, 'html' == $config['prefer_text_type']);

    list($post_date, $post_date_gmt, $delay) = tag_Delay($content, $message_date, $config['time_offset']);
    if ($fulldebug) {
        DebugEcho("post date: $content");
    }

    //do post type before category to keep the subject line correct
    $post_type = tag_PostType($subject, $postmodifiers, $config);
    if ($fulldebug) {
        DebugEcho("post type: $content");
    }

    $default_categoryid = $config['default_post_category'];

    DebugEcho("pre postie_category_default: '$default_categoryid'");
    $default_categoryid = apply_filters('postie_category_default', $default_categoryid);
    DebugEcho("post postie_category_default: '$default_categoryid'");

    $post_categories = tag_Categories($subject, $default_categoryid, $config, $post_id);
    if ($fulldebug) {
        DebugEcho("post category: $content");
    }

    $post_tags = tag_Tags($content, $config['default_post_tags'], 'html' == $config['prefer_text_type']);
    if ($fulldebug) {
        DebugEcho("post tag: $content");
    }

    $comment_status = tag_AllowCommentsOnPost($content);
    if ($fulldebug) {
        DebugEcho("post comment: $content");
    }

    $post_status = tag_Status($content, $config['post_status']);
    if ($fulldebug) {
        DebugEcho("post status: $content");
    }

    //handle CID before linkify
    $content = filter_ReplaceImageCIDs($content, $mimeDecodedEmail);
    if ($fulldebug) {
        DebugEcho("post cid: $content");
    }

    if ($config['converturls']) {
        $content = filter_Linkify($content);
        if ($fulldebug) {
            DebugEcho("post linkify: $content");
        }
    }

    if ($config['reply_as_comment'] == true) {
        $id = postie_get_parent_post($subject);
        if (empty($id)) {
            DebugEcho("Not a reply");
            $id = $post_id;
            $is_reply = false;
        } else {
            DebugEcho("Reply detected");
            $is_reply = true;
            if (true == $config['strip_reply']) {
                // strip out quoted content
                $lines = explode("\n", $content);
                $newContents = '';
                foreach ($lines as $line) {
                    if (preg_match("/^>.*/i", $line) == 0 &&
                            preg_match("/^(from|subject|to|date):.*?/iu", $line) == 0 &&
                            preg_match("/^-+.*?(from|subject|to|date).*?/iu", $line) == 0 &&
                            preg_match("/^on.*?wrote:$/iu", $line) == 0 &&
                            preg_match("/^-+\s*forwarded\s*message\s*-+/iu", $line) == 0) {
                        $newContents .= "$line\n";
                    }
                }
                if ((strlen($newContents) <> strlen($content)) && ('html' == $config['prefer_text_type'])) {
                    DebugEcho("Attempting to fix reply html (before): $newContents");
                    $newContents = LoadDOM($newContents)->__toString();
                    DebugEcho("Attempting to fix reply html (after): $newContents");
                }
                $content = $newContents;
            }
            wp_delete_post($post_id);
        }
    } else {
        $id = $post_id;
        DebugEcho("Replies will not be processed as comments");
    }

    if ($delay > 0 && $post_status == 'publish') {
        $post_status = 'future';
    }

    $content = filter_Newlines($content, $config);
    if ($fulldebug) {
        DebugEcho("post newline: $content");
    }

    $content = filter_Start($content, $config);
    if ($fulldebug) {
        DebugEcho("post start: $content");
    }

    $content = filter_End($content, $config);
    if ($fulldebug) {
        DebugEcho("post end: $content");
    }

    $content = filter_ReplaceImagePlaceHolders($content, $mimeDecodedEmail, $config, $id, $config['image_placeholder']);
    if ($fulldebug) {
        DebugEcho("post body ReplaceImagePlaceHolders: $content");
    }

    if ($post_excerpt) {
        $post_excerpt = filter_ReplaceImagePlaceHolders($post_excerpt, $mimeDecodedEmail, $config, $id, '#eimg%#');
        DebugEcho("excerpt: $post_excerpt");
        if ($fulldebug) {
            DebugEcho("post excerpt ReplaceImagePlaceHolders: $content");
        }
    }

    //handle inline images after linkify
    if ('plain' == $config['prefer_text_type']) {
        $content = filter_ReplaceInlineImage($content, $mimeDecodedEmail, $config);
        if ($fulldebug) {
            DebugEcho("post filter_ReplaceInlineImage: $content");
        }
    }

    $content = filter_AttachmentTemplates($content, $mimeDecodedEmail, $post_id, $config);

    if (trim($subject) == "") {
        $subject = $config['default_title'];
        DebugEcho("post parsing subject is blank using: " . $config['default_title']);
    }

    $details = array(
        'post_author' => $poster,
        'comment_author' => $postAuthorDetails['author'],
        'comment_author_url' => $postAuthorDetails['comment_author_url'],
        'user_ID' => $postAuthorDetails['user_ID'],
        'email_author' => $postAuthorDetails['email'],
        'post_date' => $post_date,
        //'post_date_gmt' => $post_date_gmt,
        'post_content' => $content,
        'post_title' => $subject,
        'post_type' => $post_type, /* Added by Raam Dev <raam@raamdev.com> */
        'ping_status' => get_option('default_ping_status'),
        'post_category' => $post_categories,
        'tags_input' => $post_tags,
        'comment_status' => $comment_status,
        'post_name' => $subject,
        'post_excerpt' => $post_excerpt,
        'ID' => $id,
        'post_status' => $post_status
    );
    return $details;
}

/**
 * This is the main handler for all of the processing
 */
function PostEmail($poster, $mimeDecodedEmail, $config) {
    postie_disable_revisions();
    //extract($config);

    /* in order to do attachments correctly, we need to associate the
      attachments with a post. So we add the post here, then update it */
    $tmpPost = array('post_title' => 'tmptitle', 'post_content' => 'tmpPost', 'post_status' => 'draft');
    $post_id = wp_insert_post($tmpPost, true);
    if (!is_wp_error($post_id)) {
        DebugEcho("tmp post id is $post_id");

        $is_reply = false;
        $postmodifiers = new PostiePostModifiers();

        $mimeDecodedEmail = apply_filters('postie_post_pre', $mimeDecodedEmail);

        $details = postie_create_post($poster, $mimeDecodedEmail, $post_id, $is_reply, $config, $postmodifiers);

        $details = apply_filters('postie_post', $details);
        $details = apply_filters('postie_post_before', $details, $mimeDecodedEmail['headers']);

        DebugEcho("Post postie_post_before filter");
        DebugDump($details);

        if (empty($details)) {
            // It is possible that the filter has removed the post, in which case, it should not be posted.
            // And if we created a placeholder post (because this was not a reply to an existing post),
            // then it should be removed
            if (!$is_reply) {
                wp_delete_post($post_id);
                EchoError("postie_post filter cleared the post, not saving.");
            }
        } else {
            postie_show_post_details($details);

            $postid = postie_save_post($details, $is_reply, $config['custom_image_field'], $postmodifiers);

            if ($config['confirmation_email'] != '') {
                if ($config['confirmation_email'] == 'sender' || $config['confirmation_email'] == 'both') {
                    $recipients = array($details['email_author']);
                }
                if ($config['confirmation_email'] == 'admin' || $config['confirmation_email'] == 'both') {
                    foreach (get_users(array('role' => 'administrator')) as $user) {
                        $recipients[] = $user->user_email;
                    }
                }
                postie_email_notify($mimeDecodedEmail, $recipients, $postid);
            }
        }
    } else {
        EchoError("PostEmail wp_insert_post failed: " . $post_id->get_error_message());
        DebugDump($post_id->get_error_messages());
        DebugDump($post_id->get_error_data());
    }
    postie_disable_revisions(true);
    DebugEcho("Done");
}

/** FUNCTIONS * */
/*
 * Added by Raam Dev <raam@raamdev.com>
 * Adds support for handling Custom Post Types by adding the
 * Custom Post Type name to the email subject separated by
 * $custom_post_type_delim, e.g. "Movies // My Favorite Movie"
 * Also supports setting the Post Format.
 */
function tag_PostType(&$subject, $postmodifiers, $config) {

    $post_type = $config['post_type'];
    $custom_post_type = $config['post_format'];
    $separated_subject = array();
    $separated_subject[0] = "";
    $separated_subject[1] = $subject;

    $custom_post_type_delim = "//";
    if (strpos($subject, $custom_post_type_delim) !== FALSE) {
        // Captures the custom post type in the subject before $custom_post_type_delim
        $separated_subject = explode($custom_post_type_delim, $subject);
        $custom_post_type = trim(strtolower($separated_subject[0]));
        DebugEcho("post type: found possible type '$custom_post_type'");
    }

    $known_post_types = get_post_types();

    if (in_array($custom_post_type, array_map('strtolower', $known_post_types))) {
        DebugEcho("post type: found type '$post_type'");
        $post_type = $custom_post_type;
        $subject = trim($separated_subject[1]);
    } elseif (in_array($custom_post_type, array_keys(get_post_format_strings()))) {
        DebugEcho("post type: found format '$custom_post_type'");
        $postmodifiers->PostFormat = $custom_post_type;
        $subject = trim($separated_subject[1]);
    }

    return $post_type;
}

function filter_Linkify($text) {
    DebugEcho("begin: filter_linkify");
    require_once( ABSPATH . WPINC . '/class-oembed.php' );
    $oe = _wp_oembed_get_object();

    $al = new PostieAutolink();
    DebugEcho("begin: filter_linkify (html)");
    $text = $al->autolink($text, $oe);
    DebugEcho("begin: filter_linkify (email)");
    return $al->autolink_email($text);
}

function LoadDOM($text) {
    return str_get_html($text, true, true, DEFAULT_TARGET_CHARSET, false);
}

/* we check whether or not the email is a forwards or a redirect. If it is
 * a fwd, then we glean the author details from the body of the post.
 * Otherwise we get them from the headers    
 */

function getPostAuthorDetails($mimeDecodedEmail) {

    $theEmail = $mimeDecodedEmail['headers']['from']['mailbox'] . '@' . $mimeDecodedEmail['headers']['from']['host'];

    $regAuthor = get_user_by('email', $theEmail);
    if ($regAuthor) {
        $theAuthor = $regAuthor->user_login;
        $theUrl = $regAuthor->user_url;
        $theID = $regAuthor->ID;
    } else {
        $theAuthor = postie_get_name_from_email($theEmail);
        $theUrl = '';
        $theID = '';
    }

    $theDetails = array(
        'author' => $theAuthor,
        'comment_author_url' => $theUrl,
        'user_ID' => $theID,
        'email' => $theEmail
    );

    return $theDetails;
}

/* we check whether or not the email is a reply to a previously
 * published post. First we check whether it starts with Re:, and then
 * we see if the remainder matches an already existing post. If so,
 * then we add that post id to the details array, which will cause the
 * existing post to be overwritten, instead of a new one being
 * generated
 */

function postie_get_parent_post(&$subject) {
    global $wpdb;

    $id = NULL;
    DebugEcho("GetParentPostForReply: Looking for parent '$subject'");
    // see if subject starts with Re:
    $matches = array();
    if (preg_match("/(^Re:)(.*)/iu", $subject, $matches)) {
        DebugEcho("GetParentPostForReply: Re: detected");
        $subject = trim($matches[2]);
        // strip out category info into temporary variable
        $tmpSubject = $subject;
        if (preg_match('/(.+): (.*)/u', $tmpSubject, $matches)) {
            $tmpSubject = trim($matches[2]);
            $matches[1] = array($matches[1]);
        } else if (preg_match_all('/\[(.[^\[]*)\]/', $tmpSubject, $matches)) {
            $tmpSubject_matches = array();
            preg_match("/](.[^\[]*)$/", $tmpSubject, $tmpSubject_matches);
            $tmpSubject = trim($tmpSubject_matches[1]);
        } else if (preg_match_all('/-(.[^-]*)-/', $tmpSubject, $matches)) {
            preg_match("/-(.[^-]*)$/", $tmpSubject, $tmpSubject_matches);
            $tmpSubject = trim($tmpSubject_matches[1]);
        }
        DebugEcho("GetParentPostForReply: tmpSubject: $tmpSubject");
        $checkExistingPostQuery = "SELECT ID FROM $wpdb->posts WHERE  post_title LIKE %s AND post_status = 'publish' AND comment_status = 'open'";
        if ($id = $wpdb->get_var($wpdb->prepare($checkExistingPostQuery, $tmpSubject))) {
            if (!empty($id)) {
                DebugEcho("GetParentPostForReply: id: $id");
            } else {
                DebugEcho("GetParentPostForReply: No parent id found");
            }
        }
    } else {
        DebugEcho("GetParentPostForReply: No parent found");
    }
    return $id;
}

/**
 * This handles actually showing the form. Called by WordPress
 */
function postie_configure() {
    include(POSTIE_ROOT . DIRECTORY_SEPARATOR . "config_form.php");
}

/**
 * This function handles determining the protocol and fetching the mail
 * @return array
 */
function postie_fetch_mail($server, $port, $email, $password, $protocol, $deleteMessages, $maxemails, $connectiontype) {
    $emails = array();
    if (!$server || !$port || !$email) {
        EchoError("Missing Configuration For Mail Server");
        return $emails;
    }
    if ($server == "pop.gmail.com") {
        DebugEcho("MAKE SURE POP IS TURNED ON IN SETTING AT Gmail");
    }
    switch (strtolower($protocol)) {
        case 'imap':
        case 'imap-ssl':
            $emails = postie_getemails('imap', $server, $port, $email, $password, $protocol, $deleteMessages, $maxemails, $connectiontype);
            break;
        case 'pop3':
        case 'pop3-ssl':
        default:
            $emails = postie_getemails('pop3', $server, $port, $email, $password, $protocol, $deleteMessages, $maxemails, $connectiontype);
    }

    return $emails;
}

function postie_getemails($type, $server, $port, $email, $password, $protocol, $deleteMessages, $maxemails, $connectiontype) {

    DebugEcho("postie_getemails: Connecting to $server:$port ($protocol)");

    $emails = array();

    try {
        DebugEcho("postie_getemails: procol: $protocol");
        if ($connectiontype == 'curl') {
            $conn = new pCurlConnection($type, trim($server), $email, $password, $port, ($protocol == 'imap-ssl' || $protocol == 'pop3-ssl'));
        } else {
            $conn = new pSocketConnection($type, trim($server), $email, $password, $port, ($protocol == 'imap-ssl' || $protocol == 'pop3-ssl'));
        }

        if ($type == 'imap') {
            $srv = new pImapMailServer($conn);
        } else {
            $srv = new pPop3MailServer($conn);
        }

        DebugEcho("postie_getemails: listing messages");
        $mailbox = new fMailbox($type, $conn, $srv);
        $messages = $mailbox->listMessages($maxemails);

        foreach ($messages as $uid => $messages) {
            DebugEcho("postie_getemails: fetch $uid");

            $email = $mailbox->fetchMessage($uid);

            if (!empty($email['html'])) {
                DebugEcho("postie_getemails: html");
                $email['html'] = filter_CleanHtml($email['html']);
            } else {
                DebugEcho("postie_getemails: no html");
            }

            $emails[] = $email;
            if ($deleteMessages) {
                DebugEcho("postie_getemails: deleting $uid");
                $mailbox->deleteMessages($uid);
            }
        }

        DebugEcho("postie_getemails: closing connection");
        $mailbox->close();
    } catch (Exception $e) {
        EchoError($e->getMessage());
    }

    return $emails;
}

/**
 * This function handles putting the actual entry into the database
 */
function postie_save_post($details, $isReply, $customImageField, $postmodifiers) {
    $post_ID = 0;
    if (!$isReply) {
        $post_ID = wp_insert_post($details, true);
        if (is_wp_error($post_ID)) {
            EchoError("PostToDB Error: " . $post_ID->get_error_message());
            DebugDump($post_ID->get_error_messages());
            DebugDump($post_ID->get_error_data());
            wp_delete_post($details['ID']);
            $post_ID = null;
        }
    } else {
        $comment = array(
            'comment_author' => $details['comment_author'],
            'comment_post_ID' => $details['ID'],
            'comment_author_email' => $details['email_author'],
            'comment_date' => $details['post_date'],
            'comment_date_gmt' => $details['post_date_gmt'],
            'comment_content' => $details['post_content'],
            'comment_author_url' => $details['comment_author_url'],
            'comment_author_IP' => '',
            'comment_approved' => 1,
            'comment_agent' => '',
            'comment_type' => '',
            'comment_parent' => 0,
            'user_id' => $details['user_ID']
        );
        $comment = apply_filters('postie_comment_before', $comment);
        $post_ID = wp_new_comment($comment);
        do_action('postie_comment_after', $comment);
    }

    if ($post_ID) {
        $postmodifiers->apply($post_ID);
        do_action('postie_post_after', $details);
    }

    return $post_ID;
}

/**
 * This function determines if the mime attachment is on the BANNED_FILE_LIST
 * @param string
 * @return boolean
 */
function isBannedFileName($filename, $bannedFiles) {
    if (preg_match('/ATT\d\d\d\d\d.txt/i', $filename)) {
        return true;
    }

    if (empty($filename) || empty($bannedFiles)) {
        return false;
    }
    foreach ($bannedFiles as $bannedFile) {
        if (fnmatch($bannedFile, $filename)) {
            EchoError("Ignoring attachment: $filename - it is on the banned files list.");
            return true;
        }
    }
    return false;
}

function postie_get_content(&$email, $config) {
    DebugEcho('postie_save_attachments: ---- start');
    $meta_return = '';
    if ($config['prefer_text_type'] == 'html') {
        if (isset($email['html'])) {
            $meta_return = $email['html'];
        }
    } else {
        if (isset($email['text'])) {
            $meta_return = $email['text'];
        }
    }
    return $meta_return;
}

function postie_save_attachments(&$email, $post_id, $poster, $config) {
    DebugEcho('postie_save_attachments: ---- start');

    if (!isset($email['attachment'])) {
        $email['attachment'] = array();
    }
    if (!isset($email['inline'])) {
        $email['inline'] = array();
    }
    if (!isset($email['related'])) {
        $email['related'] = array();
    }

    DebugEcho("postie_save_attachments: [attachment]");
    postie_save_attachments_worker($email['attachment'], $post_id, $poster, $config);
    DebugEcho("postie_save_attachments: [inline]");
    postie_save_attachments_worker($email['inline'], $post_id, $poster, $config);
    DebugEcho("postie_save_attachments: [related]");
    postie_save_attachments_worker($email['related'], $post_id, $poster, $config);


    DebugEcho("postie_save_attachments: ==== end");
}

function postie_save_attachments_worker(&$attachments, $post_id, $poster, $config) {
    foreach ($attachments as &$attachment) {
        if (array_key_exists('filename', $attachment) && !empty($attachment['filename'])) {
            DebugEcho('postie_save_attachments_worker: ' . $attachment['filename']);

            if (isBannedFileName($attachment['filename'], $config['banned_files_list'])) {
                DebugEcho("postie_save_attachments_worker: skipping banned filename " . $attachment['filename']);
                break;
            }
        } else {
            DebugEcho('postie_save_attachments_worker: un-named attachment');
        }

        postie_save_attachment($attachment, $post_id, $poster, $config);

        $filename = $attachment['wp_filename'];
        $fileext = $attachment['ext'];
        $mparts = explode('/', $attachment['mimetype']);
        $mimetype_primary = $mparts[0];
        $mimetype_secondary = $mparts[1];
        DebugEcho("postie_save_attachments_worker: mime primary: $mimetype_primary");

        $attachment['primary'] = $mimetype_primary;
        $attachment['exclude'] = false;

        $file_id = $attachment['wp_id'];
        $file = wp_get_attachment_url($file_id);

        switch ($mimetype_primary) {
            case 'text':
                DebugEcho("postie_save_attachments_worker: text attachment");
                $icon = chooseAttachmentIcon($file, $mimetype_primary, $mimetype_secondary, $config['icon_set'], $config['icon_size']);
                $attachment['template'] = "<a href='$file'>" . $icon . $filename . '</a>' . "\n";
                break;

            case 'image':
                DebugEcho("postie_save_attachments_worker: image attachment");
                $attachment['template'] = parseTemplate($file_id, $mimetype_primary, $config['imagetemplate'], $filename) . "\n";
                break;

            case 'audio':
                DebugEcho("postie_save_attachments_worker: audio attachment");
                if (in_array($fileext, $config['audiotypes'])) {
                    DebugEcho("postie_save_attachments_worker: using audio template: $mimetype_secondary");
                    $audioTemplate = $config['audiotemplate'];
                } else {
                    DebugEcho("postie_save_attachments_worker: using default audio template: $mimetype_secondary");
                    $icon = chooseAttachmentIcon($file, $mimetype_primary, $mimetype_secondary, $config['icon_set'], $config['icon_size']);
                    $audioTemplate = '<a href="{FILELINK}">' . $icon . '{FILENAME}</a>';
                }
                $attachment['template'] = parseTemplate($file_id, $mimetype_primary, $audioTemplate, $filename);
                break;

            case 'video':
                DebugEcho("postie_save_attachments_worker: video attachment");
                if (in_array($fileext, $config['video1types'])) {
                    DebugEcho("postie_save_attachments_worker: using video1 template: $fileext");
                    $videoTemplate = $config['video1template'];
                } elseif (in_array($fileext, $config['video2types'])) {
                    DebugEcho("postie_save_attachments_worker: using video2 template: $fileext");
                    $videoTemplate = $config['video2template'];
                } else {
                    DebugEcho("postie_save_attachments_worker: using default template: $fileext");
                    $icon = chooseAttachmentIcon($file, $mimetype_primary, $mimetype_secondary, $config['icon_set'], $config['icon_size']);
                    $videoTemplate = '<a href="{FILELINK}">' . $icon . '{FILENAME}</a>';
                }
                $attachment['template'] = parseTemplate($file_id, $mimetype_primary, $videoTemplate, $filename);
                break;

            default :
                DebugEcho("postie_save_attachments_worker: generic attachment ($mimetype_primary)");
                $icon = chooseAttachmentIcon($file, $mimetype_primary, $mimetype_secondary, $config['icon_set'], $config['icon_size']);
                $attachment['template'] = parseTemplate($file_id, $mimetype_primary, $config['generaltemplate'], $filename, $icon) . "\n";
                break;
        }
    }
}

function postie_save_attachment(&$attachment, $post_id, $poster, $config) {

    if (isset($attachment['filename']) && !empty($attachment['filename'])) {
        $filename = $attachment['filename'];
    } else {
        DebugEcho("postie_save_attachment: generating file name");
        $filename = uniqid();
        $attachment['filename'] = $filename;
    }

    DebugEcho("postie_save_attachment: pre sanitize file name '$filename'");
    //DebugDump($part);
    $filename = sanitize_file_name($filename);
    $attachment['wp_filename'] = $filename;

    DebugEcho("postie_save_attachment: file name '$filename'");

    $mparts = explode('/', $attachment['mimetype']);
    $mimetype_primary = $mparts[0];
    $mimetype_secondary = $mparts[1];

    $fileext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    $attachment['ext'] = $fileext;
    if (empty($fileext) && $mimetype_primary == 'image') {
        $attachment['ext'] = $mimetype_secondary;
        $filename = $filename . '.' . $mimetype_secondary;
        $attachment['wp_filename'] = $filename;
        DebugEcho("postie_save_attachment: blank image extension, changed to $mimetype_secondary ($filename)");
    }
    DebugEcho("postie_save_attachment: extension '$fileext'");

    $typeinfo = wp_check_filetype($filename);
    //DebugDump($typeinfo);
    if (!empty($typeinfo['type'])) {
        DebugEcho("postie_save_attachment: secondary lookup found " . $typeinfo['type']);
        $mimeparts = explode('/', strtolower($typeinfo['type']));
        $mimetype_primary = $mimeparts[0];
        $mimetype_secondary = $mimeparts[1];
    } else {
        DebugEcho("postie_save_attachment: secondary lookup failed, checking configured extensions");
        if (in_array($fileext, $config['audiotypes'])) {
            DebugEcho("postie_save_attachment: found audio extension");
            $mimetype_primary = 'audio';
            $mimetype_secondary = $fileext;
        } elseif (in_array($fileext, array_merge($config['video1types'], $config['video2types']))) {
            DebugEcho("postie_save_attachment: found video extension");
            $mimetype_primary = 'video';
            $mimetype_secondary = $fileext;
        } else {
            DebugEcho("postie_save_attachment: found no extension");
        }
    }
    $attachment['mimetype'] = "$mimetype_primary/$mimetype_secondary";
    DebugEcho("postie_save_attachment: mimetype $mimetype_primary/$mimetype_secondary");

    switch ($mimetype_primary) {
        case 'text':
            DebugEcho("postie_save_attachment: ctype_primary: text");
            //DebugDump($part);

            DebugEcho("postie_save_attachment: text Attachement: $filename");
            $file_id = postie_media_handle_upload($attachment, $post_id);
            if (!is_wp_error($file_id)) {
                $attachment['wp_id'] = $file_id;
                DebugEcho("postie_save_attachment: text attachment: adding '$filename'");
            } else {
                EchoError($file_id->get_error_message());
            }

            break;

        case 'image':
            DebugEcho("postie_save_attachment: image Attachement: $filename");
            $file_id = postie_media_handle_upload($attachment, $post_id);
            if (!is_wp_error($file_id)) {
                $attachment['wp_id'] = $file_id;
                //set the first image we come across as the featured image
                if ($config['featured_image'] && !has_post_thumbnail($post_id)) {
                    DebugEcho("postie_save_attachment: featured image: $file_id");
                    set_post_thumbnail($post_id, $file_id);
                }
            } else {
                EchoError("postie_save_attachment image error: " . $file_id->get_error_message());
            }
            break;

        case 'audio':
            DebugEcho("postie_save_attachment: audio Attachement: $filename");
            $file_id = postie_media_handle_upload($attachment, $post_id);
            if (!is_wp_error($file_id)) {
                $attachment['wp_id'] = $file_id;
            } else {
                EchoError("postie_save_attachment audio error: " . $file_id->get_error_message());
            }
            break;

        case 'video':
            DebugEcho("postie_save_attachment: video Attachement: $filename");
            $file_id = postie_media_handle_upload($attachment, $post_id);
            if (!is_wp_error($file_id)) {
                $attachment['wp_id'] = $file_id;
            } else {
                EchoError($file_id->get_error_message());
            }
            break;

        default:
            DebugEcho("postie_save_attachment: found file type: " . $mimetype_primary);
            if (in_array($mimetype_primary, $config['supported_file_types'])) {
                //pgp signature - then forget it
                if ($mimetype_secondary == 'pgp-signature') {
                    DebugEcho("postie_save_attachment: found pgp-signature - done");
                    break;
                }
                $file_id = postie_media_handle_upload($attachment, $post_id);
                if (!is_wp_error($file_id)) {
                    $attachment['wp_id'] = $file_id;
                    $file = wp_get_attachment_url($file_id);
                    DebugEcho("postie_save_attachment: uploaded $file_id ($file)");
                } else {
                    EchoError($file_id->get_error_message());
                }
            } else {
                EchoError("$filename has an unsupported MIME type $mimetype_primary and was not added.");
                DebugEcho("postie_save_attachment: Not in supported filetype list: '$mimetype_primary'");
                DebugDump($config['supported_file_types']);
            }
            break;
    }
}

// This function cleans up HTML in the email
function filter_CleanHtml($content) {
    $html = str_get_html($content);
    if ($html) {
        DebugEcho("filter_CleanHtml: checking filter postie_cleanhtml");
        if (apply_filters('postie_cleanhtml', true)) {
            DebugEcho("filter_CleanHtml: Looking for invalid tags");
            foreach ($html->find('script, style, head') as $node) {
                DebugEcho("filter_CleanHtml: Removing: " . $node->outertext);
                $node->outertext = '';
            }
            DebugEcho("filter_CleanHtml: " . $html->save());

            $html->load($html->save());

            $b = $html->find('body');
            if ($b) {
                DebugEcho("filter_CleanHtml: replacing body with div");
                $content = "<div>" . $b[0]->innertext . "</div>\n";
            }
        } else {
            DebugEcho("filter_CleanHtml: skipping clean html due to filter");
        }
    } else {
        DebugEcho("filter_CleanHtml: No HTML found");
    }
    return $content;
}

/**
 * Determines if the sender is a valid user.
 * @return integer|NULL
 */
function ValidatePoster(&$mimeDecodedEmail, $config) {
    extract($config);
    $poster = NULL;
    $from = "";
    if (array_key_exists('headers', $mimeDecodedEmail) && array_key_exists('from', $mimeDecodedEmail['headers'])) {
        $from = $mimeDecodedEmail['headers']['from']['mailbox'] . '@' . $mimeDecodedEmail['headers']['from']['host'];
        $from = apply_filters('postie_filter_email', $from);
        DebugEcho("ValidatePoster: post postie_filter_email $from");

        $toEmail = '';
        if (isset($mimeDecodedEmail['headers']['to'])) {
            $toEmail = $mimeDecodedEmail['headers']['to'][0]['mailbox'] . '@' . $mimeDecodedEmail['headers']['to'][0]['host'];
        }

        $replytoEmail = '';
        if (isset($mimeDecodedEmail['headers']['reply-to'])) {
            $replytoEmail = $mimeDecodedEmail['headers']['reply-to']['mailbox'] . '@' . $mimeDecodedEmail['headers']['reply-to']['host'];
        }

        $from = apply_filters("postie_filter_email2", $from, $toEmail, $replytoEmail);
        DebugEcho("ValidatePoster: post postie_filter_email2 $from");
    } else {
        DebugEcho("No 'from' header found");
        DebugDump($mimeDecodedEmail['headers']);
    }

    if (array_key_exists("headers", $mimeDecodedEmail)) {
        $from = apply_filters("postie_filter_email3", $from, $mimeDecodedEmail['headers']);
        DebugEcho("ValidatePoster: post postie_filter_email3 $from");
    }

    $resentFrom = "";
    if (array_key_exists('headers', $mimeDecodedEmail) && array_key_exists('resent-from', $mimeDecodedEmail['headers'])) {
        $resentFrom = postie_RemoveExtraCharactersInEmailAddress(trim($mimeDecodedEmail['headers']['resent-from']));
    }

    //See if the email address is one of the special authorized ones
    if (!empty($from)) {
        DebugEcho("Confirming Access For $from ");
        $user = get_user_by('email', $from);
        if ($user !== false) {
            $user_ID = $user->ID;
        }
    } else {
        $user_ID = "";
    }
    if (!empty($user_ID)) {
        $user = new WP_User($user_ID);
        if ($user->has_cap("post_via_postie")) {
            DebugEcho("$user_ID has 'post_via_postie' permissions");
            $poster = $user_ID;

            DebugEcho("ValidatePoster: pre postie_author $poster");
            $poster = apply_filters("postie_author", $poster);
            DebugEcho("ValidatePoster: post postie_author $poster");
        } else {
            DebugEcho("$user_ID does not have 'post_via_postie' permissions");
            $user_ID = "";
        }
    }
    if (empty($user_ID) && ($config['turn_authorization_off'] || isEmailAddressAuthorized($from, $config['authorized_addresses']) || isEmailAddressAuthorized($resentFrom, $config['authorized_addresses']))) {
        DebugEcho("ValidatePoster: looking up default user " . $config['admin_username']);
        $user = get_user_by('login', $config['admin_username']);
        if ($user === false) {
            EchoError("Your 'Default Poster' setting '" . $config['admin_username'] . "' is not a valid WordPress user (2)");
            $poster = 1;
        } else {
            $poster = $user->ID;
            DebugEcho("ValidatePoster: pre postie_author (default) $poster");
            $poster = apply_filters("postie_author", $poster);
            DebugEcho("ValidatePoster: post postie_author (default) $poster");
        }
        DebugEcho("ValidatePoster: found user '$poster'");
    }

    if (!$poster) {
        EchoError('Invalid sender: ' . htmlentities($from) . "! Not adding email!");
        if ($config['forward_rejected_mail']) {
            postie_email_reject($mimeDecodedEmail, array(get_option("admin_email")), $config['return_to_sender']);
            EchoError("A copy of the message has been forwarded to the administrator.");
        }
        return '';
    }

    //actually log in as the user
    if ($config['force_user_login'] == true) {
        $user = get_user_by('id', $poster);
        if ($user) {
            DebugEcho("logging in as {$user->user_login}");
            wp_set_current_user($poster);
            //wp_set_auth_cookie($poster);
            do_action('wp_login', $user->user_login);
        }
    }
    return $poster;
}

/**
 * Looks at the content for the start of the message and removes everything before that
 * If the pattern is not found everything is returned
 * @param string
 * @param string
 */
function filter_Start($content, $config) {
    $start = $config['message_start'];
    if (!empty($start)) {
        $pos = stripos($content, $start);
        if ($pos === false) {
            return $content;
        }
        DebugEcho("start filter $start");
        $content = substr($content, $pos + strlen($start), strlen($content));
    }
    return $content;
}

/**
 * Looks at the content for the start of the signature and removes all text
 * after that point
 * @param string
 * @param array - a list of patterns to determine if it is a sig block
 */
function filter_RemoveSignature($content, $config) {
    DebugEcho("filter_RemoveSignature: start");
    if ($config['drop_signature']) {
        if (empty($config['sig_pattern_list'])) {
            DebugEcho("filter_RemoveSignature: no sig_pattern_list");
            return $content;
        }
        //DebugEcho("looking for signature in: $content");

        $pattern = '/^(' . implode('|', $config['sig_pattern_list']) . ')\s?$/miu';
        DebugEcho("filter_RemoveSignature: pattern: $pattern");

        $html = LoadDOM($content);
        if ($html !== false) {
            filter_RemoveSignatureWorker($html->root, $pattern);
            $content = $html->save();
        } else {
            DebugEcho("filter_RemoveSignature: non-html");
            $arrcontent = explode("\n", $content);
            $strcontent = '';

            for ($i = 0; $i < count($arrcontent); $i++) {
                $line = trim($arrcontent[$i]);
                if (preg_match($pattern, trim($line))) {
                    DebugEcho("filter_RemoveSignature: signature found: removing");
                    break;
                }

                DebugEcho("filter_RemoveSignature: keeping '$line'");
                $strcontent .= $line;
            }
            $content = $strcontent;
        }
    } else {
        DebugEcho("filter_RemoveSignature: configured to skip");
    }
    return $content;
}

function filter_RemoveSignatureWorker(&$html, $pattern) {
    $found = false;
    $matches = array();
    if (preg_match($pattern, trim($html->plaintext), $matches)) {
        DebugEcho("filter_RemoveSignatureWorker: signature '$matches[1]' found in:\n" . trim($html->plaintext));
        //DebugDump($matches);
        $found = true;
        $i = stripos($html->innertext, $matches[1]);
        $presig = substr($html->innertext, 0, $i);
        DebugEcho("filter_RemoveSignatureWorker sig new text:\n$presig");
        $html->innertext = $presig;
    } else {
        DebugEcho("filter_RemoveSignatureWorker: no matches {preg_last_error()} '$pattern' $html->plaintext");
        //DebugDump($matches);
    }

    foreach ($html->children() as $e) {
        //DebugEcho("sig: " . $e->plaintext);
        if (!$found && preg_match($pattern, trim($e->plaintext))) {
            DebugEcho("filter_RemoveSignatureWorker signature found: removing");
            $found = true;
        }
        if ($found) {
            $e->outertext = '';
        } else {
            $found = filter_RemoveSignatureWorker($e, $pattern);
        }
    }
    return $found;
}

/**
 * Looks at the content for the given tag and removes all text
 * after that point
 * @param string
 * @param filter
 */
function filter_End($content, $config) {
    $end = $config['message_end'];
    if (!empty($end)) {
        $pos = stripos($content, $end);
        if ($pos === false) {
            return $content;
        }
        DebugEcho("end filter: $end");
        $content = substr($content, 0, $pos);
    }
    return $content;
}

//filter content for new lines
function filter_Newlines($content, $config) {
    if ($config['filternewlines']) {
        DebugEcho("filter_Newlines: filternewlines");
        $search = array(
            "/\r\n\r\n/",
            "/\r\n/",
            "/\n\n/",
            "/\r/",
            "/\n/"
        );
        $replace = array(
            'PARABREAK',
            'LINEBREAK',
            'LINEBREAK',
            'LINEBREAK',
            'LINEBREAK'
        );

        $result = preg_replace($search, $replace, $content);
        DebugDump($result);

        if ($config['convertnewline']) {
            DebugEcho("filter_Newlines: converting newlines to <br>");
            $content = preg_replace('/(LINEBREAK)/', "<br />\n", $result);
            $content = preg_replace('/(PARABREAK)/', "<br />\n<br />\n", $content);
        } else {
            $content = preg_replace('/(LINEBREAK)/', " ", $result);
            $content = preg_replace('/(PARABREAK)/', "\r\n", $content);
        }
    }
    return $content;
}

//strip pgp stuff
function filter_StripPGP($content) {
    $search = array(
        '/-----BEGIN PGP SIGNED MESSAGE-----/',
        '/Hash: SHA1/'
    );
    $replace = array(
        ' ',
        ''
    );
    return preg_replace($search, $replace, $content);
}

/**
 * Checks for the comments tag
 * @return boolean
 */
function tag_AllowCommentsOnPost(&$content) {
    $comments_allowed = get_option('default_comment_status'); // 'open' or 'closed'

    $matches = array();
    if (preg_match("/comments:([0|1|2])/iu", $content, $matches)) {
        $content = preg_replace("/comments:$matches[1]/i", "", $content);
        if ($matches[1] == "1") {
            $comments_allowed = "open";
        } else if ($matches[1] == "2") {
            $comments_allowed = "registered_only";
        } else {
            $comments_allowed = "closed";
        }
    }
    return $comments_allowed;
}

function tag_Status(&$content, $currentstatus) {
    $poststatus = $currentstatus;
    $matches = array();
    if (preg_match("/status:\s*(draft|publish|pending|private|future)/iu", $content, $matches)) {
        DebugEcho("tag_Status: found status $matches[1]");
        DebugDump($matches);
        $content = preg_replace("/$matches[0]/i", "", $content);
        $poststatus = $matches[1];
    }
    return $poststatus;
}

function tag_Delay(&$content, $message_date = NULL, $offset = 0) {
    //DebugEcho("tag_Delay: $content");
    $delay = 0;
    $matches = array();
    if (preg_match("/delay:(-?[0-9dhm]+)/iu", $content, $matches) && trim($matches[1])) {
        DebugEcho("tag_Delay: found delay: " . $matches[1]);
        $days = 0;
        $hours = 0;
        $minutes = 0;
        $dayMatches = array();
        if (preg_match("/(-?[0-9]+)d/iu", $matches[1], $dayMatches)) {
            $days = $dayMatches[1];
        }
        $hourMatches = array();
        if (preg_match("/(-?[0-9]+)h/iu", $matches[1], $hourMatches)) {
            $hours = $hourMatches[1];
        }
        $minuteMatches = array();
        if (preg_match("/(-?[0-9]+)m/iu", $matches[1], $minuteMatches)) {
            $minutes = $minuteMatches[1];
        }
        $delay = (($days * 24 + $hours) * 60 + $minutes) * 60;
        DebugEcho("tag_Delay: calculated delay: $delay");
        $content = preg_replace("/delay:$matches[1]/i", "", $content);
    }

    $tzs = get_option('timezone_string');
    if (empty($tzs)) {
        $tzs = 'GMT';
    }
    DebugEcho("tag_Delay: timezone: $tzs");

    if (empty($message_date)) {
        $dateInSeconds = current_time('timestamp');
        DebugEcho("tag_Delay: using current date: " . date('Y-m-d H:i:s', $dateInSeconds));
    } else {
        DebugEcho("tag_Delay: using message date: $message_date");
        $dateInSeconds = strtotime($message_date);
    }
    $delayeddateInSeconds = $dateInSeconds + $delay;

    $delayed_date = date('Y-m-d H:i:s', $delayeddateInSeconds);
    $corrected_date = date('Y-m-d H:i:s', $delayeddateInSeconds + ($offset * 3600));
    DebugEcho("tag_Delay: delay: $delay");
    DebugEcho("tag_Delay: delayed date:  $delayed_date");
    DebugEcho("tag_Delay: delayed with offset: $corrected_date");

    return array($corrected_date, $delayed_date, $delay);
}

/**
 * This function takes the content of the message - looks for a subject at the beginning surrounded by # and then removes that from the content
 */
function tag_Subject($content, $defaultTitle) {
    DebugEcho("tag_Subject: Looking for subject in email body");
    if (substr($content, 0, 1) != "#") {
        DebugEcho("tag_Subject: No inline subject found, using supplied default [1] '$defaultTitle'");
        return(array($defaultTitle, $content));
    }
    //make sure the first line isn't #img
    if (strtolower(substr($content, 1, 3)) != "img") {
        $subjectEndIndex = strpos($content, "#", 1);
        if (!$subjectEndIndex > 0) {
            DebugEcho("tag_Subject: No subject found, using default [2]");
            return(array($defaultTitle, $content));
        }
        $subject = substr($content, 1, $subjectEndIndex - 1);
        $content = substr($content, $subjectEndIndex + 1, strlen($content));
        DebugEcho("tag_Subject: Subject found in body: $subject");
        return array($subject, $content);
    } else {
        return(array($defaultTitle, $content));
    }
}

function postie_media_handle_upload($attachment, $post_id) {

    $tmpFile = tempnam(get_temp_dir(), 'postie');
    if ($tmpFile !== false) {
        $fp = fopen($tmpFile, 'w');
        if ($fp) {
            fwrite($fp, $attachment['data']);
            fclose($fp);
        } else {
            EchoError("postie_media_handle_upload: Could not write to temp file: '$tmpFile' ");
        }
    } else {
        EchoError("postie_media_handle_upload: Could not create temp file in " . get_temp_dir());
    }

    $file_array = array(
        'name' => $attachment['wp_filename'],
        'type' => $attachment['mimetype'],
        'tmp_name' => $tmpFile,
        'error' => 0,
        'size' => filesize($tmpFile)
    );
    DebugEcho("postie_media_handle_upload: adding " . $file_array['name']);
    DebugDump($file_array);

    require_once(ABSPATH . 'wp-admin/includes/file.php');
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    require_once(ABSPATH . 'wp-admin/includes/media.php');
    $id = media_handle_sideload($file_array, $post_id);

    if (!is_wp_error($id)) {
        do_action('postie_file_added', $post_id, $id, array());
    } else {
        EchoError("There was an error adding the attachement: " . $id->get_error_message());
        DebugDump($id->get_error_messages());
        DebugDump($id->get_error_data());
    }

    return $id;
}

function postie_email_notify($email, $recipients, $postid) {
    DebugEcho("postie_email_notify: start");

    if (count($recipients) == 0) {
        DebugEcho("postie_email_notify: no recipients");
        return;
    }
    if (empty($postid)) {
        DebugEcho("postie_email_notify: no post id");
        return;
    }

    $myemailadd = get_option("admin_email");
    $blogname = get_option("blogname");
    $eblogname = "=?utf-8?b?" . base64_encode($blogname) . "?= ";
    $posturl = get_permalink($postid);
    $subject = $email['headers']['subject'];

    $sendheaders = array("From: $eblogname <$myemailadd>");

    $mailtext = "Your post '$subject' has been successfully published to $blogname <$posturl>.\n";

    $recipients = apply_filters('postie_email_notify_recipients', $recipients, $email, $postid);
    if (count($recipients) == 0) {
        DebugEcho("postie_email_notify: no recipients after postie_email_notify_recipients filter");
        return;
    }
    $subject = "Successfully posted to $blogname";
    $subject = apply_filters('postie_email_notify_subject', $subject, $email, $postid);

    $mailtext = apply_filters('postie_email_notify_body', $mailtext, $email, $postid);

    DebugEcho("postie_email_notify: To:");
    DebugDump($recipients);
    DebugEcho("postie_email_notify: $subject\n$mailtext");

    wp_mail($recipients, $subject, $mailtext, $sendheaders);
}

function postie_header_encode($value) {
    return "=?utf-8?b?" . base64_encode($value) . "?= ";
}

function postie_email_reject($email, $recipients, $returnToSender) {
    require_once(ABSPATH . 'wp-admin/includes/file.php'); //wp_tempnam()
    DebugEcho("postie_email_reject: start");

    if (count($recipients) == 0) {
        DebugEcho("postie_email_reject: no recipients");
        return;
    }

    $blogname = get_option("blogname");
    $from = $email['headers']['from']['mailbox'] . '@' . $email['headers']['from']['host'];

    $subject = $email['headers']['subject'];
    if ($returnToSender) {
        DebugEcho("postie_email_reject: return to sender $from");
        array_push($recipients, $from);
    }

    $eblogname = postie_header_encode($blogname);
    $adminemail = get_option("admin_email");

    $headers = array();
    $headers[] = "From: $eblogname <$adminemail>";

    DebugEcho("postie_email_reject: To:");
    DebugDump($recipients);
    DebugEcho("postie_email_reject: header:");
    DebugDump($headers);

    $message = "An unauthorized message has been sent to $blogname.\n";
    $message .= "Sender: $from\n";
    $message .= "Subject: $subject\n";
    $message .= "\n\nIf you wish to allow posts from this address, please add " . $from . " to the registered users list and manually add the content of the email found below.";
    $message .= "\n\nOtherwise, the email has already been deleted from the server and you can ignore this message.";
    $message .= "\n\nIf you would like to prevent postie from forwarding mail in the future, please change the FORWARD_REJECTED_MAIL setting in the Postie settings panel";
    $message .= "\n\nThe original content of the email has been attached.\n\n";

    $recipients = apply_filters('postie_email_reject_recipients', $recipients, $email);
    if (count($recipients) == 0) {
        DebugEcho("postie_email_reject: no recipients after postie_email_reject_recipients filter");
        return;
    }

    $subject = $blogname . ": Unauthorized Post Attempt from $from";
    $subject = apply_filters('postie_email_reject_subject', $subject, $email);

    $message = apply_filters('postie_email_reject_body', $message, $email);

    $attachTxt = wp_tempnam() . '.txt';
    file_put_contents($attachTxt, $email['text']);

    $attachHtml = wp_tempnam() . '.htm';
    file_put_contents($attachHtml, $email['html']);

    wp_mail($recipients, $subject, $message, $headers, array($attachTxt, $attachHtml));

    unlink($attachTxt);
    unlink($attachHtml);
}

/**
 * This compares the current address to the list of authorized addresses
 * @param string - email address
 * @return boolean
 */
function isEmailAddressAuthorized($address, $authorized) {
    $r = false;
    if (is_array($authorized)) {
        $a = strtolower(trim($address));
        if (!empty($a)) {
            $r = in_array($a, array_map('strtolower', $authorized));
        }
    }
    return $r;
}

/**
 * This method works around a problem with email address with extra <> in the email address
 * @param string
 * @return string
 */
function postie_RemoveExtraCharactersInEmailAddress($address) {
    $matches = array();
    if (preg_match('/^[^<>]+<([^<> ()]+)>$/', $address, $matches)) {
        $address = $matches[1];
        DebugEcho("RemoveExtraCharactersInEmailAddress: $address (1)");
        DebugDump($matches);
    } else if (preg_match('/<([^<> ()]+)>/', $address, $matches)) {
        $address = $matches[1];
        DebugEcho("RemoveExtraCharactersInEmailAddress: $address (2)");
    }

    return $address;
}

/**
 * This function gleans the name from an email address if available. If not
 * it just returns the username (everything before @)
 */
function postie_get_name_from_email($address) {
    $name = "";
    $matches = array();
    if (preg_match('/^([^<>]+)<([^<> ()]+)>$/', $address, $matches)) {
        $name = $matches[1];
    } else if (preg_match('/<([^<>@ ()]+)>/', $address, $matches)) {
        $name = $matches[1];
    } else if (preg_match('/(.+?)@(.+)/', $address, $matches)) {
        $name = $matches[1];
    }

    return trim($name);
}

/**
 * Choose an appropriate file icon based on the extension and mime type of
 * the attachment
 */
function chooseAttachmentIcon($file, $primary, $secondary, $iconSet = 'silver', $size = '32') {
    if ($iconSet == 'none') {
        return('');
    }
    $fileName = basename($file);
    $parts = explode('.', $fileName);
    $ext = $parts[count($parts) - 1];
    $docExts = array('doc', 'docx');
    $docMimes = array('msword', 'vnd.ms-word', 'vnd.openxmlformats-officedocument.wordprocessingml.document');
    $pptExts = array('ppt', 'pptx');
    $pptMimes = array('mspowerpoint', 'vnd.ms-powerpoint', 'vnd.openxmlformats-officedocument.');
    $xlsExts = array('xls', 'xlsx');
    $xlsMimes = array('msexcel', 'vnd.ms-excel', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    $iWorkMimes = array('zip', 'octet-stream');
    $mpgExts = array('mpg', 'mpeg', 'mp2');
    $mpgMimes = array('mpg', 'mpeg', 'mp2');
    $mp3Exts = array('mp3');
    $mp3Mimes = array('mp3', 'mpeg3', 'mpeg');
    $mp4Exts = array('mp4', 'm4v');
    $mp4Mimes = array('mp4', 'mpeg4', 'octet-stream');
    $aacExts = array('m4a', 'aac');
    $aacMimes = array('m4a', 'aac', 'mp4');
    $aviExts = array('avi');
    $aviMimes = array('avi', 'x-msvideo');
    $movExts = array('mov');
    $movMimes = array('mov', 'quicktime');
    if ($ext == 'pdf' && $secondary == 'pdf') {
        $fileType = 'pdf';
    } else if ($ext == 'pages' && in_array($secondary, $iWorkMimes)) {
        $fileType = 'pages';
    } else if ($ext == 'numbers' && in_array($secondary, $iWorkMimes)) {
        $fileType = 'numbers';
    } else if ($ext == 'key' && in_array($secondary, $iWorkMimes)) {
        $fileType = 'key';
    } else if (in_array($ext, $docExts) && in_array($secondary, $docMimes)) {
        $fileType = 'doc';
    } else if (in_array($ext, $pptExts) && in_array($secondary, $pptMimes)) {
        $fileType = 'ppt';
    } else if (in_array($ext, $xlsExts) && in_array($secondary, $xlsMimes)) {
        $fileType = 'xls';
    } else if (in_array($ext, $mp4Exts) && in_array($secondary, $mp4Mimes)) {
        $fileType = 'mp4';
    } else if (in_array($ext, $movExts) && in_array($secondary, $movMimes)) {
        $fileType = 'mov';
    } else if (in_array($ext, $aviExts) && in_array($secondary, $aviMimes)) {
        $fileType = 'avi';
    } else if (in_array($ext, $mp3Exts) && in_array($secondary, $mp3Mimes)) {
        $fileType = 'mp3';
    } else if (in_array($ext, $mpgExts) && in_array($secondary, $mpgMimes)) {
        $fileType = 'mpg';
    } else if (in_array($ext, $aacExts) && in_array($secondary, $aacMimes)) {
        $fileType = 'aac';
    } else {
        $fileType = 'default';
    }
    $fileName = "/icons/$iconSet/$fileType-$size.png";
    if (!file_exists(POSTIE_ROOT . $fileName)) {
        $fileName = "/icons/$iconSet/default-$size.png";
    }
    $iconHtml = "<img src='" . POSTIE_URL . $fileName . "' alt='$fileType icon' />";
    DebugEcho("icon: $iconHtml");
    return $iconHtml;
}

function parseTemplate($fileid, $type, $template, $orig_filename, $icon = "") {
    DebugEcho("parseTemplate: before '$template'");

    $attachment = get_post($fileid);
    $uploadDir = wp_upload_dir();
    $fileName = basename($attachment->guid);
    $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
    $absFileName = $uploadDir['path'] . '/' . $fileName;
    $relFileName = str_replace(ABSPATH, '', $absFileName);
    $fileLink = wp_get_attachment_url($fileid);
    $pageLink = get_attachment_link($fileid);

    $template = str_replace('{TITLE}', $attachment->post_title, $template);
    $template = str_replace('{ID}', $fileid, $template);

    if ($type == 'image') {
        $widths = array();
        $heights = array();
        $img_urls = array();

        postie_show_filters_for('image_downsize'); //possible overrides for image_downsize()

        /*
         * Possible enhancement: support all sizes returned by get_intermediate_image_sizes()
         */
        $sizes = array('thumbnail', 'medium', 'large');
        for ($i = 0; $i < count($sizes); $i++) {
            $iinfo = image_downsize($fileid, $sizes[$i]);
            if (false !== $iinfo) {
                $img_urls[$i] = $iinfo[0];
                $widths[$i] = $iinfo[1];
                $heights[$i] = $iinfo[2];
            }
        }
        DebugEcho('parseTemplate: Sources');
        DebugDump($img_urls);
        DebugEcho('parseTemplate: Heights');
        DebugDump($heights);
        DebugEcho('parseTemplate: Widths');
        DebugDump($widths);

        $template = str_replace('{THUMBNAIL}', $img_urls[0], $template);
        $template = str_replace('{THUMB}', $img_urls[0], $template);
        $template = str_replace('{MEDIUM}', $img_urls[1], $template);
        $template = str_replace('{LARGE}', $img_urls[2], $template);
        $template = str_replace('{THUMBWIDTH}', $widths[0] . 'px', $template);
        $template = str_replace('{THUMBHEIGHT}', $heights[0] . 'px', $template);
        $template = str_replace('{MEDIUMWIDTH}', $widths[1] . 'px', $template);
        $template = str_replace('{MEDIUMHEIGHT}', $heights[1] . 'px', $template);
        $template = str_replace('{LARGEWIDTH}', $widths[2] . 'px', $template);
        $template = str_replace('{LARGEHEIGHT}', $heights[2] . 'px', $template);
    }

    $template = str_replace('{FULL}', $fileLink, $template);
    $template = str_replace('{FILELINK}', $fileLink, $template);
    $template = str_replace('{FILETYPE}', $fileType, $template);
    $template = str_replace('{PAGELINK}', $pageLink, $template);
    $template = str_replace('{FILENAME}', $orig_filename, $template);
    $template = str_replace('{IMAGE}', $fileLink, $template);
    $template = str_replace('{URL}', $fileLink, $template);
    $template = str_replace('{RELFILENAME}', $relFileName, $template);
    $template = str_replace('{ICON}', $icon, $template);
    $template = str_replace('{FILEID}', $fileid, $template);

    DebugEcho("parseTemplate: after: '$template<br />'");
    return $template . '<br />';
}

/**
 * When sending in HTML email the html refers to the content-id(CID) of the image - this replaces
 * the cid place holder with the actual url of the image sent in
 * @param string - text of post
 * @param array - array of HTML for images for post
 */
function filter_ReplaceImageCIDs($content, &$email) {
    DebugEcho('filter_ReplaceImageCIDs: start');
    $content = preg_replace("/\[(cid:\S+)\]/i", '<img src="$1"/>', $content); //fix "[cid:xxx-xxx-xx]" image references gmail adds to plain text
    if (count($email['related'])) {
        DebugEcho("filter_ReplaceImageCIDs: # CID attachments: " . count($email['related']));
        foreach ($email['related'] as $cid => &$attachment) {
            if (false !== stripos($content, $cid)) {
                DebugEcho("filter_ReplaceImageCIDs: CID: $cid");
                $fileid = $attachment['wp_id'];
                $url = wp_get_attachment_url($fileid);
                $content = str_replace($cid, $url, $content);
                $attachment['exclude'] = true;
            } else {
                DebugEcho("filter_ReplaceImageCIDs: CID not found: $cid");
            }
        }
    } else {
        DebugEcho("filter_ReplaceImageCIDs: no cid attachments");
    }
    return $content;
}

function filter_ReplaceInlineImage($content, &$email, $config) {
    DebugEcho('filter_ReplaceInlineImage: start');
    $i = 1;
    foreach ($email['inline'] as &$inlineImage) {
        if (($inlineImage['primary'] == 'image' && $config['auto_gallery']) ||
                $config['custom_image_field'] ||
                $config['featured_image'] && !$config['include_featured_image'] && $i == 1) {
            //remove inline placeholder if we're not showing the image here
            DebugEcho('filter_ReplaceInlineImage: do not add inline due to config');
            $template = '';
        } else {
            $template = $inlineImage['template'];
        }
        $inlinemarker = '<:inline ' . $inlineImage['filename'] . ' inline:>'; //use the original non-sanitized name
        if (false !== stripos($content, $inlinemarker)) {
            DebugEcho('filter_ReplaceInlineImage: ' . $inlineImage['filename']);
            $content = str_ireplace($inlinemarker, $template, $content);
            $inlineImage['exclude'] = !empty($template); //don't exclude if we didn't add
        } else {
            DebugEcho('filter_ReplaceInlineImage: not found: ' . $inlineImage['filename']);
        }
        $i++;
    }
    return $content;
}

/**
 * This function handles replacing image place holder #img1# with the HTML for that image
 */
function filter_ReplaceImagePlaceHolders($content, &$email, $config, $post_id, $image_pattern) {
    DebugEcho("filter_ReplaceImagePlaceHolders: start");
    if (!$config['custom_image_field']) {
        $startIndex = $config['start_image_count_at_zero'] ? 0 : 1;

        $images = get_posts(array(
            'post_parent' => $post_id,
            'post_type' => 'attachment',
            'numberposts' => -1,
            'post_mime_type' => 'image',));
        DebugEcho("filter_ReplaceImagePlaceHolders: images in post: " . count($images));
        DebugDump($images);

        $i = 0;

        //TODO only call the worker if the attachment matches one of the images
        foreach ($email['attachment'] as &$attachment) {
            $content = filter_ReplaceImagePlaceHolders_worker($content, $attachment, $image_pattern, $startIndex, $i);
            $i++;
        }
        foreach ($email['inline'] as &$attachment) {
            $content = filter_ReplaceImagePlaceHolders_worker($content, $attachment, $image_pattern, $startIndex, $i);
            $i++;
        }
        foreach ($email['related'] as &$attachment) {
            $content = filter_ReplaceImagePlaceHolders_worker($content, $attachment, $image_pattern, $startIndex, $i);
            $i++;
        }
    } else {
        DebugEcho("filter_ReplaceImagePlaceHolders: Custom image field, not adding images");
    }
    DebugEcho("filter_ReplaceImagePlaceHolders: end");
    return $content;
}

function filter_ReplaceImagePlaceHolders_worker($content, &$attachment, $imagePattern, $startIndex, $currentIndex) {
    DebugEcho("filter_ReplaceImagePlaceHolders_worker: " . $attachment['wp_filename']);

    if (empty($attachment['template'])) {
        DebugEcho("filter_ReplaceImagePlaceHolders_worker: no template");
        return $content;
    }

    $matches = array();
    $pparts = explode('%', $imagePattern);
    if (count($pparts) != 2) {
        DebugEcho("filter_ReplaceImagePlaceHolders_worker: invalid image pattern: $imagePattern");
        return $content;
    }
    $pattern = '/' . $pparts[0] . (string) ($startIndex + $currentIndex) . '\s?(caption=[\'"]?(.*?)[\'"]?)?' . $pparts[1] . '/iu';
    DebugEcho("filter_ReplaceImagePlaceHolders_worker: pattern: $pattern");
    preg_match_all($pattern, $content, $matches, PREG_SET_ORDER);
    //DebugEcho($content);
    DebugDump($matches);
    foreach ($matches as $match) {
        $attachment['exclude'] = true;
        $imageTemplate = $attachment['template'];

        $caption = '';
        if (count($match) > 2) {
            $caption = trim($match[2]);
            DebugEcho("filter_ReplaceImagePlaceHolders_worker: caption: '$caption'");
        }
        DebugEcho("filter_ReplaceImagePlaceHolders_worker: parameterize templete: $imageTemplate");
        $imageTemplate = mb_str_replace('{CAPTION}', htmlspecialchars($caption, ENT_QUOTES), $imageTemplate);
        DebugEcho("filter_ReplaceImagePlaceHolders_worker: captioned template: $imageTemplate");

        $imageTemplate = apply_filters('postie_place_media', $imageTemplate, $attachment['wp_id']);
        DebugEcho("filter_ReplaceImagePlaceHolders_worker: post filter: '$imageTemplate'");

        $content = str_ireplace($match[0], $imageTemplate, $content);
        DebugEcho("filter_ReplaceImagePlaceHolders_worker: post replace: $content");
    }
    return $content;
}

/**
 * This function handles finding and setting the correct subject
 * @return array - (subject,content)
 */
function postie_get_subject(&$mimeDecodedEmail, &$content, $config) {
    //assign the default title/subject
    if (empty($mimeDecodedEmail['headers']['subject'])) {
        DebugEcho("postie_get_subject: No subject in email");
        if ($config['allow_subject_in_mail']) {
            list($subject, $content) = tag_Subject($content, $config['default_title']);
        } else {
            DebugEcho("postie_get_subject: Using default subject");
            $subject = $config['default_title'];
        }
        $mimeDecodedEmail['headers']['subject'] = $subject;
    } else {
        $subject = $mimeDecodedEmail['headers']['subject'];
        DebugEcho(("postie_get_subject: Predecoded subject: $subject"));

        if ($config['allow_subject_in_mail']) {
            list($subject, $content) = tag_Subject($content, $subject);
        }
    }
    if (!$config['allow_html_in_subject']) {
        DebugEcho("postie_get_subject: subject before htmlentities: $subject");
        $subject = htmlentities($subject, ENT_COMPAT, $config['message_encoding']);
        DebugEcho("postie_get_subject: subject after htmlentities: $subject");
    }

    //This is for ISO-2022-JP - Can anyone confirm that this is still neeeded?
    // escape sequence is 'ESC $ B' == 1b 24 42 hex.
    if (strpos($subject, "\x1b\x24\x42") !== false) {
        // found iso-2022-jp escape sequence in subject... convert!
        DebugEcho("postie_get_subject: extra parsing for ISO-2022-JP");
        $subject = iconv("ISO-2022-JP", "UTF-8//TRANSLIT", $subject);
    }
    DebugEcho("postie_get_subject: '$subject'");
    return $subject;
}

function tag_Tags(&$content, $defaultTags, $isHtml) {
    $post_tags = array();

    $matches = array();
    $rx = '/>\s*tags:\s?(.*?)</isu';
    if (!$isHtml) {
        $rx = '/tags:\s?(.*)/iu';
    }
    if (preg_match($rx, $content, $matches)) {
        if (!empty($matches[1])) {
            DebugEcho("Found tags: $matches[1]");
            if ($isHtml) {
                $content = str_replace($matches[0], "><", $content);
            } else {
                $content = str_replace($matches[0], "", $content);
            }
            $post_tags = preg_split("/,\s*/", trim($matches[1]));
        }
    }

    if (count($post_tags) == 0 && is_array($defaultTags)) {
        $post_tags = $defaultTags;
    }
    return $post_tags;
}

function tag_Excerpt(&$content, $config) {
    $post_excerpt = '';
    $matches = array();
    if (preg_match('/:excerptstart ?(.*):excerptend/is', $content, $matches)) {
        $content = str_replace($matches[0], "", $content);
        $post_excerpt = $matches[1];
        DebugEcho("excerpt found: $post_excerpt");
        if ($config['filternewlines']) {
            DebugEcho("filtering newlines from excerpt");
            $post_excerpt = filter_Newlines($post_excerpt, $config);
        }
    }
    return $post_excerpt;
}

/**
 * This function determines the categories ids for the post
 * @return array
 */
function tag_Categories(&$subject, $defaultCategoryId, $config, $post_id) {
    $category_match = $config['category_match'];
    $original_subject = $subject;
    $found = false;
    $post_categories = array();
    $matchtypes = array();
    $matches = array();

    if ($config['category_bracket']) {
        if (preg_match_all('/\[(.[^\[]*)\]/', $subject, $matches)) { // [<category1>] [<category2>] <Subject>
            $matchtypes[] = $matches;
        }
    }

    if ($config['category_dash']) {
        if (preg_match_all('/-(.[^-]*)-/', $subject, $matches)) { // -<category>- -<category2>- <Subject>
            $matchtypes[] = $matches;
        }
    }

    if ($config['category_colon']) {
        if (preg_match('/(.+):\s?(.*)/', $subject, $matches)) { // <category>: <Subject>
            $matchtypes[] = array(array(0 => $matches[1] . ':'), array(1 => $matches[1]));
        }
    }

    DebugEcho("tag_Categories: found categories");
    DebugDump($matchtypes);
    foreach ($matchtypes as $matches) {
        if (count($matches)) {
            $i = 0;
            foreach ($matches[1] as $match) {
                DebugEcho("tag_Categories: checking: $match");

                DebugEcho("tag_Categories: looking up: $defaultCategoryId");
                $defaultcat_name = '';
                $defaultcat = get_term_by('id', $defaultCategoryId, postie_lookup_taxonomy_name($defaultCategoryId));
                if (false !== $defaultcat) {
                    $defaultcat_name = $defaultcat->name;
                    DebugEcho("tag_Categories: default: $defaultcat_name");
                } else {
                    DebugEcho("tag_Categories: default not found");
                }
                $trial_category = apply_filters('postie_category', trim($match), $category_match, $defaultcat_name);
                DebugEcho("tag_Categories: post filter: $trial_category");

                $categoryid = postie_lookup_category_id($trial_category, $category_match);
                if (!empty($categoryid)) {
                    $found = true;
                    $subject = str_replace($matches[0][$i], '', $subject);
                    DebugEcho("tag_Categories: subject: $subject");
                    $tax = postie_lookup_taxonomy_name($categoryid);
                    if ('category' == $tax) {
                        $post_categories[] = $categoryid;
                    } else {
                        DebugEcho("tag_Categories: custom taxonomy $tax");
                        wp_set_object_terms($post_id, $categoryid, $tax, true);
                    }
                }
                $i++;
            }
        }
    }
    if (!$found || !$config['category_remove']) {
        DebugEcho("tag_Categories: using default: $defaultCategoryId");
        $post_categories[] = $defaultCategoryId;
        $subject = $original_subject;
    }
    $subject = trim($subject);
    return $post_categories;
}

function postie_lookup_taxonomy_name($termid) {
    global $wpdb;
    $tax_sql = 'SELECT taxonomy FROM ' . $wpdb->term_taxonomy . ' WHERE term_id = ' . $termid;
    $tax = $wpdb->get_var($tax_sql);
    DebugEcho("lookup_taxonomy: $termid is in taxonomy $tax");
    return $tax;
}

function postie_lookup_category_id($trial_category, $category_match = true) {
    global $wpdb;
    DebugEcho("lookup_category: $trial_category");

    $term = get_term_by('name', esc_attr($trial_category), 'category');
    if (!empty($term)) {
        DebugEcho("lookup_category: found by name $trial_category");
        //DebugDump($term);
        //then category is a named and found 
        return $term->term_id;
    } else {
        DebugEcho("lookup_category: not found by name $trial_category");
    }

    $term = get_term_by('slug', esc_attr($trial_category), 'category');
    if (!empty($term)) {
        DebugEcho("lookup_category: found by slug $trial_category");
        return $term->term_id;
    } else {
        DebugEcho("lookup_category: not found by slug $trial_category");
    }

    if (is_numeric($trial_category)) {
        DebugEcho("lookup_category: looking for id '$trial_category'");
        $cat_id = intval($trial_category);
        $term = get_term_by('id', $cat_id, 'category');
        if (!empty($term) && $term->term_id == $trial_category) {
            DebugEcho("lookup_category: found by id '$cat_id'");
            DebugDump($term);
            //then category was an ID and found 
            return $term->term_id;
        } else {
            DebugEcho("lookup_category: not found by id '$cat_id'");
        }
    }

    $found_category = NULL;
    if ($category_match) {
        DebugEcho("category wildcard lookup: $trial_category");
        $sql_sub_name = 'SELECT term_id FROM ' . $wpdb->terms . ' WHERE name LIKE \'' . addslashes(esc_attr($trial_category)) . '%\' limit 1';
        $found_category = $wpdb->get_var($sql_sub_name);
        DebugEcho("lookup_category: wildcard found: $found_category");
    } else {
        DebugEcho("lookup_category: wildcard not found: $found_category");
    }

    return intval($found_category); //force to integer
}

/**
 * This function just outputs a simple html report about what is being posted in
 */
function postie_show_post_details($details) {
    //DebugDump($details);
    // Report
    DebugEcho('Post Author: ' . $details['post_author']);
    DebugEcho('Date: ' . $details['post_date']);
    foreach ($details['post_category'] as $category) {
        DebugEcho('Category: ' . $category);
    }
    DebugEcho('Ping Status: ' . $details['ping_status']);
    DebugEcho('Comment Status: ' . $details['comment_status']);
    DebugEcho('Subject: ' . $details['post_title']);
    DebugEcho('Postname: ' . $details['post_name']);
    DebugEcho('Post Id: ' . $details['ID']);
    DebugEcho('Post Type: ' . $details['post_type']); /* Added by Raam Dev <raam@raamdev.com> */
    //DebugEcho('Posted content: '.$details["post_content"]);
}

/**
 * Takes a value and builds a simple simple yes/no select box
 * @param string
 * @param string
 * @param string
 * @param string
 */
function BuildSelect($label, $id, $current_value, $options, $recommendation = NULL) {

    $html = "<tr>
	<th scope='row'><label for='$id'>$label</label>";

    $html .= "</th><td><select name='$id' id='$id'>";
    foreach ($options as $value) {
        $html .= "<option value='$value' " . ($value == $current_value ? "selected='selected'" : "") . ">" . __($value, 'postie') . '</option>';
    }
    $html .= '</select>';
    if (!empty($recommendation)) {
        $html .= '<p class = "description">' . $recommendation . '</p>';
    }
    $html .= "</td>\n</tr>";

    return $html;
}

/**
 * Takes a value and builds a simple simple yes/no select box
 * @param string
 * @param string
 * @param string
 * @param string
 */
function BuildBooleanSelect($label, $id, $current_value, $recommendation = NULL, $options = null) {

    $html = "<tr>
	<th scope='row'><label for='$id'>$label</label>";


    if (!(is_array($options) && count($options) == 2)) {
        $options = Array('Yes', 'No');
    }

    $html .= "</th>
	<td><select name='$id' id='$id'>
            <option value='1'>" . __($options[0], 'postie') . "</option>
            <option value='0' " . (!$current_value ? "selected='selected'" : "") . ">" . __($options[1], 'postie') . '</option>
    </select>';
    if (!empty($recommendation)) {
        $html .= '<p class = "description">' . $recommendation . '</p>';
    }
    $html .= "</td>\n</tr>";

    return $html;
}

/**
 * This takes an array and display a text box for editing
 * @param string
 * @param string
 * @param array
 * @param string
 */
function BuildTextArea($label, $id, $current_value, $recommendation = NULL) {
    $html = "<tr><th scope='row'><label for='$id'>$label</label>";

    $html .= "</th>";

    $html .= "<td><br /><textarea cols=40 rows=3 name='$id' id='$id'>";
    $current_value = preg_split("/[\r\n]+/", esc_attr(trim($current_value)));
    if (is_array($current_value)) {
        foreach ($current_value as $item) {
            $html .= "$item\n";
        }
    }
    $html .= "</textarea>";
    if ($recommendation) {
        $html .= "<p class='description'>" . $recommendation . "</p>";
    }
    $html .= "</td></tr>";
    return $html;
}

/**
 * This function resets all the configuration options to the default
 */
function config_ResetToDefault() {
    $newconfig = config_GetDefaults();
    $config = get_option('postie-settings');
    $save_keys = array('mail_password', 'mail_server', 'mail_server_port', 'mail_userid', 'input_protocol', 'input_connection');
    foreach ($save_keys as $key) {
        $newconfig[$key] = $config[$key];
    }
    update_option('postie-settings', $newconfig);
    config_Update($newconfig);
    return $newconfig;
}

/**
 * This function used to handle updating the configuration.
 * @return boolean
 */
function config_Update($data) {
    UpdatePostiePermissions($data['role_access']);
    // We also update the cron settings
    postie_cron($data['interval']);
}

/**
 * return an array of the config defaults
 */
function config_GetDefaults() {
    include('templates/audio_templates.php');
    include('templates/image_templates.php');
    include('templates/video1_templates.php');
    include('templates/video2_templates.php');
    include 'templates/general_template.php';
    return array(
        'add_meta' => 'no',
        'admin_username' => 'admin',
        'allow_html_in_body' => true,
        'allow_html_in_subject' => true,
        'allow_subject_in_mail' => true,
        'audiotemplate' => $simple_link,
        'audiotypes' => array('m4a', 'mp3', 'ogg', 'wav', 'mpeg'),
        'authorized_addresses' => array(),
        'banned_files_list' => array(),
        'confirmation_email' => '',
        'convertnewline' => false,
        'converturls' => true,
        'custom_image_field' => false,
        'default_post_category' => NULL,
        'category_match' => true,
        'default_post_tags' => array(),
        'default_title' => "Live From The Field",
        'delete_mail_after_processing' => true,
        'drop_signature' => true,
        'filternewlines' => true,
        'forward_rejected_mail' => true,
        'icon_set' => 'silver',
        'icon_size' => 32,
        'auto_gallery' => false,
        'image_new_window' => false,
        'image_placeholder' => "#img%#",
        'images_append' => true,
        'imagetemplate' => $wordpress_default,
        'imagetemplates' => $imageTemplates,
        'input_protocol' => "pop3",
        'input_connection' => 'sockets',
        'interval' => 'twiceperhour',
        'mail_server' => NULL,
        'mail_server_port' => 110,
        'mail_userid' => NULL,
        'mail_password' => NULL,
        'maxemails' => 0,
        'message_start' => ":start",
        'message_end' => ":end",
        'message_encoding' => "UTF-8",
        'message_dequote' => true,
        'post_status' => 'publish',
        'prefer_text_type' => "plain",
        'return_to_sender' => false,
        'role_access' => array(),
        'selected_audiotemplate' => 'simple_link',
        'selected_imagetemplate' => 'wordpress_default',
        'selected_video1template' => 'vshortcode',
        'selected_video2template' => 'simple_link',
        'shortcode' => false,
        'sig_pattern_list' => array('--\s?[\r\n]?', '--\s', '--', '---'),
        'smtp' => array(),
        'start_image_count_at_zero' => false,
        'supported_file_types' => array('application'),
        'turn_authorization_off' => false,
        'time_offset' => get_option('gmt_offset'),
        'video1template' => $simple_link,
        'video1types' => array('mp4', 'mpeg4', '3gp', '3gpp', '3gpp2', '3gp2', 'mov', 'mpeg', 'quicktime'),
        'video2template' => $simple_link,
        'video2types' => array('x-flv'),
        'video1templates' => $video1Templates,
        'video2templates' => $video2Templates,
        'wrap_pre' => 'no',
        'featured_image' => false,
        'include_featured_image' => true,
        'email_tls' => false,
        'post_format' => 'standard',
        'post_type' => 'post',
        'generaltemplates' => $generalTemplates,
        'generaltemplate' => $postie_default,
        'selected_generaltemplate' => 'postie_default',
        'generate_thumbnails' => true,
        'reply_as_comment' => true,
        'force_user_login' => false,
        'auto_gallery_link' => 'default',
        'ignore_mail_state' => false,
        'strip_reply' => true,
        'postie_log_error' => true,
        'postie_log_debug' => false,
        'category_colon' => true,
        'category_dash' => true,
        'category_bracket' => true,
        'prefer_text_convert' => true,
        'category_remove' => true
    );
}

/**
 * =======================================================
 * the following functions are only used to retrieve the old (pre 1.4) config, to convert it
 * to the new format
 */
function config_GetListOfArrayConfig() {
    return(array('SUPPORTED_FILE_TYPES', 'AUTHORIZED_ADDRESSES',
        'SIG_PATTERN_LIST', 'BANNED_FILES_LIST', 'VIDEO1TYPES',
        'VIDEO2TYPES', 'AUDIOTYPES', 'SMTP'));
}

function config_Read() {
    $config = get_option('postie-settings');
    return config_ValidateSettings($config);
}

/**
 * This function retrieves the old-format config (pre 1.4) from the database
 * @return array
 */
function config_ReadOld() {
    $config = array();
    global $wpdb;
    $wpdb->query("SHOW TABLES LIKE '" . $GLOBALS['table_prefix'] . "postie_config'");
    if ($wpdb->num_rows > 0) {
        $data = $wpdb->get_results("SELECT label,value FROM " . $GLOBALS['table_prefix'] . "postie_config;");
        if (is_array($data)) {
            foreach ($data as $row) {
                if (in_array($row->label, config_GetListOfArrayConfig())) {
                    $config[$row->label] = unserialize($row->value);
                } else {
                    $config[$row->label] = $row->value;
                }
            }
        }
    }
    return $config;
}

/**
 * This function processes the old-format config (pre 1.4) for conversion to the 1.4 format
 * @return array
 * @access private
 */
function config_UpgradeOld() {
    $config = config_ReadOld();
    if (!isset($config['ADMIN_USERNAME'])) {
        $config['ADMIN_USERNAME'] = 'admin';
    }
    if (!isset($config['PREFER_TEXT_TYPE'])) {
        $config['PREFER_TEXT_TYPE'] = "plain";
    }
    if (!isset($config['DEFAULT_TITLE'])) {
        $config['DEFAULT_TITLE'] = "Live From The Field";
    }
    if (!isset($config["INPUT_PROTOCOL"])) {
        $config["INPUT_PROTOCOL"] = "pop3";
    }
    if (!isset($config["IMAGE_PLACEHOLDER"])) {
        $config["IMAGE_PLACEHOLDER"] = "#img%#";
    }
    if (!isset($config["IMAGES_APPEND"])) {
        $config["IMAGES_APPEND"] = true;
    }
    if (!isset($config["ALLOW_SUBJECT_IN_MAIL"])) {
        $config["ALLOW_SUBJECT_IN_MAIL"] = true;
    }
    if (!isset($config["DROP_SIGNATURE"])) {
        $config["DROP_SIGNATURE"] = true;
    }
    if (!isset($config["MESSAGE_START"])) {
        $config["MESSAGE_START"] = ":start";
    }
    if (!isset($config["MESSAGE_END"])) {
        $config["MESSAGE_END"] = ":end";
    }
    if (!isset($config["FORWARD_REJECTED_MAIL"])) {
        $config["FORWARD_REJECTED_MAIL"] = true;
    }
    if (!isset($config["RETURN_TO_SENDER"])) {
        $config["RETURN_TO_SENDER"] = false;
    }
    if (!isset($config["CONFIRMATION_EMAIL"])) {
        $config["CONFIRMATION_EMAIL"] = '';
    }
    if (!isset($config["ALLOW_HTML_IN_SUBJECT"])) {
        $config["ALLOW_HTML_IN_SUBJECT"] = true;
    }
    if (!isset($config["ALLOW_HTML_IN_BODY"])) {
        $config["ALLOW_HTML_IN_BODY"] = true;
    }
    if (!isset($config["START_IMAGE_COUNT_AT_ZERO"])) {
        $config["START_IMAGE_COUNT_AT_ZERO"] = false;
    }
    if (!isset($config["MESSAGE_ENCODING"])) {
        $config["MESSAGE_ENCODING"] = "UTF-8";
    }
    if (!isset($config["MESSAGE_DEQUOTE"])) {
        $config["MESSAGE_DEQUOTE"] = true;
    }
    if (!isset($config["TURN_AUTHORIZATION_OFF"])) {
        $config["TURN_AUTHORIZATION_OFF"] = false;
    }
    if (!isset($config["CUSTOM_IMAGE_FIELD"])) {
        $config["CUSTOM_IMAGE_FIELD"] = false;
    }
    if (!isset($config["CONVERTNEWLINE"])) {
        $config["CONVERTNEWLINE"] = false;
    }
    if (!isset($config["SIG_PATTERN_LIST"])) {
        $config["SIG_PATTERN_LIST"] = array('--', '---');
    }
    if (!isset($config["BANNED_FILES_LIST"])) {
        $config["BANNED_FILES_LIST"] = array();
    }
    if (!isset($config["SUPPORTED_FILE_TYPES"])) {
        $config["SUPPORTED_FILE_TYPES"] = array("application");
    }
    if (!isset($config["AUTHORIZED_ADDRESSES"])) {
        $config["AUTHORIZED_ADDRESSES"] = array();
    }
    if (!isset($config["MAIL_SERVER"])) {
        $config["MAIL_SERVER"] = NULL;
    }
    if (!isset($config["MAIL_SERVER_PORT"])) {
        $config["MAIL_SERVER_PORT"] = NULL;
    }
    if (!isset($config["MAIL_USERID"])) {
        $config["MAIL_USERID"] = NULL;
    }
    if (!isset($config["MAIL_PASSWORD"])) {
        $config["MAIL_PASSWORD"] = NULL;
    }
    if (!isset($config["DEFAULT_POST_CATEGORY"])) {
        $config["DEFAULT_POST_CATEGORY"] = NULL;
    }
    if (!isset($config["DEFAULT_POST_TAGS"])) {
        $config["DEFAULT_POST_TAGS"] = NULL;
    }
    if (!isset($config["TIME_OFFSET"])) {
        $config["TIME_OFFSET"] = get_option('gmt_offset');
    }
    if (!isset($config["WRAP_PRE"])) {
        $config["WRAP_PRE"] = 'no';
    }
    if (!isset($config["CONVERTURLS"])) {
        $config["CONVERTURLS"] = true;
    }
    if (!isset($config["SHORTCODE"])) {
        $config["SHORTCODE"] = false;
    }
    if (!isset($config["ADD_META"])) {
        $config["ADD_META"] = 'no';
    }
    $config['ICON_SETS'] = array('silver', 'black', 'white', 'custom', 'none');
    if (!isset($config["ICON_SET"])) {
        $config["ICON_SET"] = 'silver';
    }
    $config['ICON_SIZES'] = array(32, 48, 64);
    if (!isset($config["ICON_SIZE"])) {
        $config["ICON_SIZE"] = 32;
    }

    //audio
    include('templates/audio_templates.php');
    if (!isset($config["SELECTED_AUDIOTEMPLATE"])) {
        $config['SELECTED_AUDIOTEMPLATE'] = 'simple_link';
    }
    $config['AUDIOTEMPLATES'] = $audioTemplates;
    if (!isset($config["SELECTED_VIDEO1TEMPLATE"])) {
        $config['SELECTED_VIDEO1TEMPLATE'] = 'simple_link';
    }
    if (!isset($config["AUDIOTEMPLATE"])) {
        $config["AUDIOTEMPLATE"] = $simple_link;
    }

    //video1
    if (!isset($config["VIDEO1TYPES"])) {
        $config['VIDEO1TYPES'] = array('mp4', 'mpeg4', '3gp', '3gpp', '3gpp2', '3gp2', 'mov', 'mpeg', 'quicktime');
    }
    if (!isset($config["AUDIOTYPES"])) {
        $config['AUDIOTYPES'] = array('m4a', 'mp3', 'ogg', 'wav', 'mpeg');
    }
    if (!isset($config["SELECTED_VIDEO2TEMPLATE"])) {
        $config['SELECTED_VIDEO2TEMPLATE'] = 'simple_link';
    }
    include('templates/video1_templates.php');
    $config['VIDEO1TEMPLATES'] = $video1Templates;
    if (!isset($config["VIDEO1TEMPLATE"])) {
        $config["VIDEO1TEMPLATE"] = $simple_link;
    }

    //video2
    if (!isset($config["VIDEO2TYPES"])) {
        $config['VIDEO2TYPES'] = array('x-flv');
    }
    if (!isset($config["POST_STATUS"])) {
        $config["POST_STATUS"] = 'publish';
    }
    if (!isset($config["IMAGE_NEW_WINDOW"])) {
        $config["IMAGE_NEW_WINDOW"] = false;
    }
    if (!isset($config["FILTERNEWLINES"])) {
        $config["FILTERNEWLINES"] = true;
    }
    include('templates/video2_templates.php');
    $config['VIDEO2TEMPLATES'] = $video2Templates;
    if (!isset($config["VIDEO2TEMPLATE"])) {
        $config["VIDEO2TEMPLATE"] = $simple_link;
    }

    //image
    if (!isset($config["SELECTED_IMAGETEMPLATE"])) {
        $config['SELECTED_IMAGETEMPLATE'] = 'wordpress_default';
    }
    if (!isset($config["SMTP"])) {
        $config["SMTP"] = array();
    }
    include('templates/image_templates.php');
    if (!isset($config["IMAGETEMPLATE"])) {
        $config["IMAGETEMPLATE"] = $wordpress_default;
    }
    $config['IMAGETEMPLATES'] = $imageTemplates;

    //general
    include('templates/general_template.php');
    if (!isset($config["GENERALTEMPLATE"])) {
        $config["GENERALTEMPLATE"] = $postie_default;
    }

    return $config;
}

/**
 * This function returns the old
  -format config (pre 1.4)
 * @return array
 */
function config_GetOld() {
    $config = config_UpgradeOld();
    //These should only be modified if you are testing
    $config["DELETE_MAIL_AFTER_PROCESSING"] = true;
    $config["POST_TO_DB"] = true;
    $config["TEST_EMAIL"] = false;
    $config["TEST_EMAIL_ACCOUNT"] = "blogtest";
    $config["TEST_EMAIL_PASSWORD"] = "yourpassword";
    if (file_exists(POSTIE_ROOT . '/postie_test_variables.php')) {
        include(POSTIE_ROOT . '/postie_test_variables.php');
    }
    //include(POSTIE_ROOT . "/../postie-test.php");
    // These are computed
    $config["TIME_OFFSET"] = get_option('gmt_offset');
    $config["POSTIE_ROOT"] = POSTIE_ROOT;
    for ($i = 0; $i < count($config["AUTHORIZED_ADDRESSES"]); $i++) {
        $config["AUTHORIZED_ADDRESSES"][$i] = strtolower($config["AUTHORIZED_ADDRESSES"][$i]);
    }
    return $config;
}

/**
 * end of functions used to retrieve the old (pre 1.4) config
 * =======================================================
 */

/**
 * Returns a list of config keys that should be arrays
 * @return array
 */
function config_ArrayedSettings() {
    return array(
        ', ' => array('audiotypes', 'video1types', 'video2types', 'default_post_tags'),
        "\n" => array('smtp', 'authorized_addresses', 'supported_file_types', 'banned_files_list', 'sig_pattern_list'));
}

function HasMbStringInstalled() {
    $function_list = array("mb_detect_encoding");
    return(HasFunctions($function_list));
}

function HasIconvInstalled($display = true) {
    $function_list = array("iconv");
    return(HasFunctions($function_list, $display));
}

/**
 * Handles verifying that a list of functions exists
 * @return boolean
 * @param array
 */
function HasFunctions($function_list, $display = true) {
    foreach ($function_list as $function) {
        if (!function_exists($function)) {
            if ($display) {
                EchoError("Missing $function");
            }
            return false;
        }
    }
    return true;
}

/**
 * This function looks for markdown which causes problems with postie
 */
function isMarkdownInstalled() {
    if (in_array("markdown.php", get_option("active_plugins"))) {
        return true;
    }
    return false;
}

/**
 * validates the config form output, fills in any gaps by using the defaults,
 * and ensures that arrayed items are stored as such
 */
function config_ValidateSettings($in) {
    //DebugEcho("config_ValidateSettings");

    $out = array();

    //DebugDump($in);
    // use the default as a template: 
    // if a field is present in the defaults, we want to store it; otherwise we discard it
    $allowed_keys = config_GetDefaults();
    foreach ($allowed_keys as $key => $default) {
        if (is_array($in)) {
            $out[$key] = array_key_exists($key, $in) ? $in[$key] : $default;
        } else {
            $out[$key] = $default;
        }
    }

    // some fields are always forced to lower case:
    $lowercase = array('authorized_addresses', 'smtp', 'supported_file_types', 'video1types', 'video2types', 'audiotypes');
    foreach ($lowercase as $field) {
        $out[$field] = ( is_array($out[$field]) ) ? array_map("strtolower", $out[$field]) : strtolower($out[$field]);
    }
    $arrays = config_ArrayedSettings();

    foreach ($arrays as $sep => $fields) {
        foreach ($fields as $field) {
            if (!is_array($out[$field])) {
                $out[$field] = explode($sep, trim($out[$field]));
            }
            foreach ($out[$field] as $key => $val) {
                $tst = trim($val);
                if
                (empty($tst)) {
                    unset($out[$field][$key]);
                } else {
                    $out[$field][$key] = $tst;
                }
            }
        }
    }

    config_Update($out);
    return $out;
}

/**
 * This function handles setting up the basic permissions
 */
function UpdatePostiePermissions($role_access) {
    global $wp_roles;
    if (is_object($wp_roles)) {
        $admin = $wp_roles->get_role("administrator");
        $admin->add_cap("config_postie");
        $admin->add_cap("post_via_postie");

        if (!is_array($role_access)) {
            $role_access = array();
        }
        foreach ($wp_roles->role_names as $roleId => $name) {
            $role = $wp_roles->get_role($roleId);
            if ($roleId != "administrator") {
                if (array_key_exists($roleId, $role_access)) {
                    $role->add_cap("post_via_postie");
                    //DebugEcho("added $roleId");
                } else {
                    $role->remove_cap("post_via_postie");
                    //DebugEcho("removed $roleId");
                }
            }
        }
    }
}

function IsDebugMode() {
    return (defined('POSTIE_DEBUG') && POSTIE_DEBUG == true);
}

function postie_save_email_debug($raw, $email) {
    if (IsDebugMode()) {
        //DebugDump($email);
        //DebugDump($mimeDecodedEmail);

        $dname = POSTIE_ROOT . DIRECTORY_SEPARATOR . 'test_emails' . DIRECTORY_SEPARATOR;
        if (is_dir($dname)) {
            $mid = $email['headers']['message-id'];
            if (empty($mid)) {
                $mid = uniqid();
            }
            $fname = $dname . sanitize_file_name($mid);

            file_put_contents($fname . '-raw.txt', $raw);
            file_put_contents($fname . '.txt', $email['text']);
            file_put_contents($fname . '.html', $email['html']);
        }
    }
}

function postie_show_filters_for($hook = '') {
    global $wp_filter;
    if (empty($hook) || !isset($wp_filter[$hook])) {
        DebugEcho("No registered filters for $hook");
        return;
    }
    DebugEcho("Registered filters for $hook");
    DebugDump($wp_filter[$hook]);
}

function postie_test_config() {

    wp_get_current_user();

    if (!current_user_can('manage_options')) {
        DebugEcho("non-admin tried to set options");
        echo "<h2> Sorry only admin can run this file</h2>";
        exit();
    }

    $config = config_Read();
    if (true == $config['postie_log_error'] || (defined('POSTIE_DEBUG') && true == POSTIE_DEBUG)) {
        add_action('postie_log_error', 'postie_log_error');
    }
    if (true == $config['postie_log_debug'] && !defined('POSTIE_DEBUG')) {
        define('POSTIE_DEBUG', true);
    }
    if (true == $config['postie_log_debug'] || (defined('POSTIE_DEBUG') && true == POSTIE_DEBUG)) {
        add_action('postie_log_debug', 'postie_log_debug');
    }
    ?>
    <div class="wrap"> 
        <h1>Postie Configuration Test</h1>
        <?php
        postie_environment(true);
        ?>

        <h2>Clock</h2>
        <p>This shows what time it would be if you posted right now</p>
        <?php
        $wptz = get_option('gmt_offset');
        $wptzs = get_option('timezone_string');
        DebugEcho("Wordpress timezone: $wptzs ($wptz)", true);
        DebugEcho("Current time: " . current_time('mysql'), true);
        DebugEcho("Current time (gmt): " . current_time('mysql', 1), true);
        DebugEcho("Postie time correction: {$config['time_offset']}", true);
        $offsetdate = strtotime(current_time('mysql')) + $config['time_offset'];
        DebugEcho("Post time: " . date('Y-m-d H:i:s', $offsetdate), true);
        ?>
        <h2>Encoding</h2>
        <?php
        $default_charset = ini_get('default_charset');
        if (version_compare(phpversion(), '5.6.0', '<')) {
            if (empty($default_charset)) {
                DebugEcho("default_charset: WARNING not default_charset set see http://php.net/manual/en/ini.core.php#ini.default-charset", true);
            } else {
                DebugEcho("default_charset: $default_charset", true);
            }
        } else {
            if (empty($default_charset)) {
                DebugEcho("default_charset: UTF-8 (default)", true);
            } else {
                DebugEcho("default_charset: $default_charset", true);
            }
        }

        if (defined('DB_CHARSET')) {
            DebugEcho("DB_CHARSET: " . DB_CHARSET, true);
        } else {
            DebugEcho("DB_CHARSET: undefined (utf8)", true);
        }

        if (defined('DB_COLLATE')) {
            $db_collate = DB_COLLATE;
            if (empty($db_collate)) {
                DebugEcho("DB_COLLATE: database default", true);
            } else {
                DebugEcho("DB_COLLATE: " . DB_COLLATE, true);
            }
        }

        DebugEcho("WordPress encoding: " . esc_attr(get_option('blog_charset')), true);
        DebugEcho("Postie encoding: " . $config['message_encoding'], true);
        ?>
        <h2>Images</h2>
        <?php
        DebugEcho("registered image sizes");
        DebugDump(get_intermediate_image_sizes());
        ?>
        <h2>Connect to Mail Host</h2>
        <?php
        DebugEcho("Postie connection: " . $config['input_connection'], true);
        DebugEcho("Postie protocol: " . $config['input_protocol'], true);
        DebugEcho("Postie server: " . $config['mail_server'], true);
        DebugEcho("Postie port: " . $config['mail_server_port'], true);

        if (!$config['mail_server'] || !$config['mail_server_port'] || !$config['mail_userid']) {
            EchoError("FAIL - server settings not complete");
        }

        $conninfo = postie_get_conn_info($config);
        if (IsDebugMode()) {
            fCore::enableDebugging(true);
            fCore::registerDebugCallback('DebugEcho');
        }

        switch (strtolower($config['input_protocol'])) {
            case 'imap':
            case 'imap-ssl':
                try {
                    if ($config['input_connection'] == 'curl') {
                        $conn = new pCurlConnection('imap', $conninfo['mail_server'], $conninfo['mail_user'], $conninfo['mail_password'], $conninfo['mail_port'], ($conninfo['mail_protocol'] == 'imap-ssl' || $conninfo['mail_protocol'] == 'pop3-ssl'));
                    } else {
                        $conn = new pSocketConnection('imap', $conninfo['mail_server'], $conninfo['mail_user'], $conninfo['mail_password'], $conninfo['mail_port'], ($conninfo['mail_protocol'] == 'imap-ssl' || $conninfo['mail_protocol'] == 'pop3-ssl'));
                    }
                    $srv = new pImapMailServer($conn);
                    $mailbox = new fMailbox('imap', $conn, $srv);
                    $m = $mailbox->countMessages();
                    DebugEcho("Successful " . strtoupper($config['input_protocol']) . " connection on port {$config['mail_server_port']}", true);
                    DebugEcho("# of waiting messages: $m", true);
                } catch (Exception $e) {
                    EchoError("Unable to connect. The server said: " . $e->getMessage());
                }
                break;

            case 'pop3':
            case 'pop3-ssl':
                try {
                    if ($config['input_connection'] == 'curl') {
                        $conn = new pCurlConnection('pop3', $conninfo['mail_server'], $conninfo['mail_user'], $conninfo['mail_password'], $conninfo['mail_port'], ($conninfo['mail_protocol'] == 'imap-ssl' || $conninfo['mail_protocol'] == 'pop3-ssl'), 30);
                    } else {
                        $conn = new pSocketConnection('pop3', $conninfo['mail_server'], $conninfo['mail_user'], $conninfo['mail_password'], $conninfo['mail_port'], ($conninfo['mail_protocol'] == 'imap-ssl' || $conninfo['mail_protocol'] == 'pop3-ssl'));
                    }
                    $srv = new pPop3MailServer($conn);
                    $mailbox = new fMailbox('pop3', $conn, $srv);
                    $m = $mailbox->countMessages();
                    DebugEcho("Successful " . strtoupper($config['input_protocol']) . " connection on port {$config['mail_server_port']}", true);
                    DebugEcho("# of waiting messages: $m", true);
                } catch (Exception $e) {
                    EchoError("Unable to connect. The server said:");
                    EchoError($e->getMessage());
                }
                break;
            default:
                EchoError("Unable to connect. Invalid setup");
                break;
        }
        ?>
    </div>
    <?php
}

function postie_get_conn_info($config) {
    $conninfo = array();
    $conninfo['mail_server'] = $config['mail_server'];
    $conninfo['mail_port'] = $config['mail_server_port'];
    $conninfo['mail_user'] = $config['mail_userid'];
    $conninfo['mail_password'] = $config['mail_password'];
    $conninfo['mail_protocol'] = $config['input_protocol'];
    $conninfo['mail_tls'] = $config['email_tls'];
    $conninfo['email_delete_after_processing'] = $config['delete_mail_after_processing'];
    $conninfo['email_max'] = $config['maxemails'];
    $conninfo['email_ignore_state'] = $config['ignore_mail_state'];

    return apply_filters('postie_preconnect', $conninfo);
}

function postie_get_mail() {
    if (!function_exists('file_get_html')) {
        require_once (plugin_dir_path(__FILE__) . 'simple_html_dom.php');
    }

    $config = config_Read();
    if (true == $config['postie_log_error'] || (defined('POSTIE_DEBUG') && true == POSTIE_DEBUG)) {
        add_action('postie_log_error', 'postie_log_error');
    }
    if (true == $config['postie_log_debug'] && !defined('POSTIE_DEBUG')) {
        define('POSTIE_DEBUG', true);
    }
    if (true == $config['postie_log_debug'] || (defined('POSTIE_DEBUG') && true == POSTIE_DEBUG)) {
        add_action('postie_log_debug', 'postie_log_debug');
    }

    do_action('postie_session_start');
    DebugEcho('Starting mail fetch');
    DebugEcho('WordPress datetime: ' . current_time('mysql'));

    postie_environment();
    $wp_content_path = dirname(dirname(dirname(__FILE__)));
    DebugEcho("wp_content_path: $wp_content_path");
    if (file_exists($wp_content_path . DIRECTORY_SEPARATOR . 'filterPostie.php')) {
        DebugEcho("found filterPostie.php in $wp_content_path");
        include_once ($wp_content_path . DIRECTORY_SEPARATOR . 'filterPostie.php');
    }

    if (function_exists('memory_get_usage')) {
        DebugEcho(__("memory at start of email processing: ", 'postie') . memory_get_usage());
    }

    if (has_filter('postie_post')) {
        echo "Postie: filter 'postie_post' is depricated in favor of 'postie_post_before'";
    }

    if (!array_key_exists('maxemails', $config)) {
        $config['maxemails'] = 0;
    }

    $conninfo = postie_get_conn_info($config);

    $emails = postie_fetch_mail($conninfo['mail_server'], $conninfo['mail_port'], $conninfo['mail_user'], $conninfo['mail_password'], $conninfo['mail_protocol'], $conninfo['email_delete_after_processing'], $conninfo['email_max'], $config['input_connection']);

    $message = 'Done.';

    DebugEcho(sprintf(__("There are %d messages to process", 'postie'), count($emails)));

    //don't output the password
    $tmp_config = $config;
    unset($tmp_config['mail_password']);
    DebugDump($tmp_config);

    //loop through messages
    $message_number = 0;
    foreach ($emails as $email) {
        $message_number++;
        DebugEcho("$message_number: ------------------------------------");
        //DebugDump($email);
        //sanity check to see if there is any info in the message
        if ($email == NULL) {
            $message = __('Dang, message is empty!', 'postie');
            EchoError("$message_number: $message");
            continue;
        } else if ($email == 'already read') {
            $message = __("Message is already marked 'read'.", 'postie');
            DebugEcho("$message_number: $message");
            continue;
        }

        $tmp_config = $config;
        if ($config['prefer_text_convert']) {
            if ($config['prefer_text_type'] == 'plain' && trim($email['text']) == '' && trim($email['html']) != '') {
                DebugEcho('postie_get_mail: switching to html');
                $tmp_config['prefer_text_type'] = 'html';
            }
            if ($config['prefer_text_type'] == 'html' && trim($email['html']) == '' && trim($email['text']) != '') {
                DebugEcho('postie_get_mail: switching to plain');
                $tmp_config['prefer_text_type'] = 'plain';
            }
        }

        //Check poster to see if a valid person
        $poster = ValidatePoster($email, $tmp_config);
        if (!empty($poster)) {
            PostEmail($poster, $email, $tmp_config);
            DebugEcho("$message_number: processed");
        } else {
            EchoError('Ignoring email - not authorized.');
        }
        flush();
    }
    DebugEcho("Mail fetch complete, $message_number emails");
    do_action('postie_session_end');

    if (function_exists('memory_get_usage')) {
        DebugEcho('memory at end of email processing: ' . memory_get_usage());
    }
}
