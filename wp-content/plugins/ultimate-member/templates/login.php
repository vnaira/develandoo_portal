<div class="um <?php echo $this->get_class($mode); ?> um-<?php echo $form_id; ?>">
    <div class="col-lg-12 col-md-12 col-xs-12 m-top40 m-bot70">
        <p class="custom-title small-title-mobile">Log In your Free account</p>
        <p class="font24 m-top10 hidden-xs hidden-sm">Enter your details below</p>
    </div>
    <div class="um-form">

        <form method="post" action="" autocomplete="off">

            <?php

            do_action("um_before_form", $args);

            do_action("um_before_{$mode}_fields", $args);

            do_action("um_main_{$mode}_fields", $args);

            do_action("um_after_form_fields", $args);

            do_action("um_after_{$mode}_fields", $args);

            do_action("um_after_form", $args);

            ?>

        </form>

    </div>

</div>