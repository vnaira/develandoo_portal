<?php
/**
 * @package download-access
 */
/*
Plugin Name: Download Access
Plugin URI: https://develadoo.rocks/
Description: Access your download file
Version: 1.0
Author: Arzin Nikolyan
Text Domain: Download Access
*/
define('DA_DIR', rtrim(plugin_dir_path(__FILE__), '/'));
include(DA_DIR . '/includes/DaDbManager.php');
include(DA_DIR . '/helper/mailHelper.php');
include(ABSPATH . "wp-includes/pluggable.php");
if (!defined('ABSPATH')) exit;

class DownloadAccess
{
    private static $instance;
    private $dbDa;

    private function __construct()
    {
        add_action('plugins_loaded', array(&$this, 'pluginsLoaded'), 14.5);
    }

    function pluginsLoaded()
    {
        $this->dbDa = new DaDbManager();
        $this->dbDa->createTables();
        add_action('wp_enqueue_scripts', array($this, '_scripts_incloud'), 211);
        add_action('admin_enqueue_scripts', array($this, 'adminScript'), 221);
        add_action('wp_ajax_add_address_action', array(&$this, 'addTemporary'));
        add_action('wp_ajax_get_url', array(&$this, 'getValidEmail'));
        add_action('wp_ajax_nopriv_get_url', array(&$this, 'getValidEmail'));
        add_action('wp_ajax_send_mail', array(&$this, 'sendEmail'));
        add_action('wp_ajax_nopriv_send_mail', array(&$this, 'sendEmail'));
        add_action('admin_menu', array(&$this, 'addDaMenu'));
    }

    public function addDaMenu()
    {
        add_menu_page('Download Access', 'Download Access', 'manage_options', 'download-access', array($this, 'da_admin_page'));
    }

    public function da_admin_page()
    {
        include_once 'view/da-admin-page.php';
    }

    public function addTemporary()
    {
        $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
        if ($address && !$this->dbDa->tempreryExists($address)) {
            $this->dbDa->insertTempreryAddress($address);
            wp_die(json_encode(['added'=>1]));
        }
        wp_die(json_encode(['added'=>0]));
    }

    public function downloadFile($hash, $type)
    {
        $this->dbDa = new DaDbManager();
        if ($hash && $type) {

            $customer = $this->dbDa->getFromHash($hash, $type);

            if (empty($customer)) {
                wp_redirect(home_url("ebooks/"));
            }
            $root = "/var/www/html";
            $id = $customer[0]->book_id;
            $urlArray = get_post_meta($id, $type);

            $url = $fullUrl = $email = '';
            if (isset($urlArray[0])) {
                $fullUrl = $urlArray[0];
                $url = parse_url($fullUrl);
                $email = $customer[0]->email;
            }

            $download = $root . $url['path'];
            $fileName = basename($fullUrl);
            if (file_exists($download)) {
                $this->dbDa->downloads($id, $email);
                $file = file_get_contents($download);
                header("Content-type: application/force-download");
                header("Content-Disposition: attachment; filename=\"" . str_replace(" ", "_", $fileName) . "\"");
                echo $file;
                exit();
            }
        }
    }

    public function getTemprery()
    {
        $tempreryList = $this->dbDa->getTempreryList();
        if (!is_null($tempreryList)) {
            return $tempreryList;
        }
        return [];
    }

    public function isValidEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $tempereryData = $this->dbDa->temprery($email);
            if (empty($tempereryData)) {
                return true;
            }
        }
        return false;
    }

    public function getValidEmail()
    {
        $error = [];
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        if ($email) {
            $email = wp_slash(trim(esc_attr($_POST["email"])));
            if (!$this->isValidEmail($email)) {
                $error['error'] = 1;
                wp_die(json_encode($error));
            }
        }
    }

    function _scripts_incloud()
    {
        // Register the script like this for a plugin:
        wp_register_script('main-script', plugins_url('/js/script.js', __FILE__));
        wp_localize_script('main-script', 'get_link', array('url' => admin_url('admin-ajax.php'), 'action' => 'get_url',));
        wp_localize_script('main-script', 'send', array('url' => admin_url('admin-ajax.php'), 'action' => 'send_mail',));
        // For either a plugin or a theme, you can then enqueue the script:
        wp_enqueue_script('main-script');
    }

    public function adminScript()
    {
        wp_register_script('admin-script', plugins_url('/js/admin-script.js', __FILE__));
        wp_localize_script('admin-script', 'add_address', array('url' => admin_url('admin-ajax.php'), 'action' => 'add_address_action',));
        wp_enqueue_script('admin-script');
    }

    public function sendEmail()
    {
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $type = filter_input(INPUT_POST, 'file_type', FILTER_SANITIZE_STRING);
        $id = filter_input(INPUT_POST, 'book_id', FILTER_SANITIZE_NUMBER_INT);
        $minute=15;
        if ($email && $type && $id) {
            $emailObject = new mailHelper();
            $hash = md5($email . time());
            $insert=$this->dbDa->insertDownloadDetails($email, $id, $type, $hash);
            $minute=$insert['time'];
            if ($insert['inserted']) {
                $emailObject->sendMail($email, $type, $hash);
                wp_die(json_encode(['isSend'=>1,'time'=>15]));
            }
        }
        wp_die(json_encode(['isSend'=>0,'time'=>$minute]));
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new DownloadAccess();
        }
        return self::$instance;
    }
}

$instance = DownloadAccess::getInstance();

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$hash = filter_input(INPUT_GET, 'hash', FILTER_SANITIZE_STRING);
$type = filter_input(INPUT_GET, 'type', FILTER_SANITIZE_STRING);

if ($action == 'download' && $hash && $type) {
    $instance->downloadFile($hash, $type);
}
