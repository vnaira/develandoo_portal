(function ($) {
    $(document).delegate('.bbtn .new-link, .bbtn_ .new-link','click', function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        var type = $(this).data('file-type');
        $("#email-modal").trigger("click");
        $("#file-id").val(id);
        $("#file-type").val(type);
        $(".alert-success").hide();
        $(".alert-danger").hide();
        $("#e-mail").val('');
    });
    $(document).on('keyup', '#e-mail', function () {
        $(".alert-danger").hide();
        $(".alert-success").hide();
    });
    $(document).delegate('.modal-footer a', 'click', function (event) {
        event.preventDefault();
        var data = {
            id: $("#file-id").val(),
            type: $("#file-type").val(),
            email: $("#e-mail").val()
        };

        sendLink(data);
    });
    function validateEmail(email) {
        var emailReg = /^([\w-\.]{3,}@([\w-]+\.)+[\w-]{2,5})?$/;
        return emailReg.test(email);
    }

    function sendLink(filedata) {

        if (filedata.email == '') {
            $(".alert-danger").text("Please complete this mandatory field.");
            $(".alert-danger").show();
            $(".alert-success").hide();
            return false;
        }
        if (!validateEmail(filedata.email)) {
            $(".alert-danger").text("Email must be formatted correctly.");
            $(".alert-danger").show();
            $(".alert-success").hide();
            return false;
        }
        $.ajax({
            type: "post",
            url: get_link.url,
            data: {
                action: get_link.action,
                email: filedata.email

            },
            success: function (response) {
                var res = $.parseJSON(response);
                if (res.error) {
                    $(".alert-danger").text("The email is not valid.");
                    $(".alert-danger").show();
                    $(".alert-success").hide();
                }
                else {

                    $.ajax({
                        type: "post",
                        url: send.url,
                        data: {
                            action: send.action,
                            book_id: filedata.id,
                            file_type: filedata.type,
                            email: filedata.email
                        },
                        success: function (response) {
                            var res = $.parseJSON(response);

                            if(res.isSend==1){
                                $(".alert-danger").hide();
                                $(".alert-success").show();
                            }else {

                                var min=" minutes";
                                if(res.time==1 || res.time==0){
                                    res.time=1;
                                    min=" minute";
                                }
                                $(".alert-danger").text("Please try again in "+res.time+min+"!");
                                $(".alert-danger").show();
                                $(".alert-success").hide();
                            }
                        },

                    })
                }
            },
            error: function (response) {

            }
        });
    }
})(jQuery);