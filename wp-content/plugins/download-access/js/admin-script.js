(function ($) {

    $(document).delegate('#add_address', 'click', function (event) {

        var address = $.trim($("#temp_address").val());

        if (address && address != "") {
            if (!validateTemp(address)) {
                $("#alert-danger").text("Invalid Temprery address");
                $("#alert-danger").show();
                $("#alert-success").hide();
                return false;
            }
            $.ajax({
                type: "post",
                url: add_address.url,
                data: {
                    action: add_address.action,
                    address: address
                },
                success: function (response) {
                    var res = $.parseJSON(response);
                    if (res.added == 1) {
                        $("#alert-danger").hide();
                        $("#alert-success").show();
                        $("#alert-success").text("Temprery address added to list");
                        $("#temp_address").val("");
                    } else {
                        $("#alert-danger").text("Temprery exist");
                        $("#alert-danger").show();
                        $("#alert-success").hide();
                    }
                },

            })

        } else {
            $("#alert-danger").text("Temprery empty");
            $("#alert-danger").show();
            $("#alert-success").hide();
        }

    });
    function validateTemp(temp) {
        var tempReg = /^(([\w-]+\.)+[\w-]{2,5})?$/;
        return tempReg.test(temp);
    }

})(jQuery);