<h1>Add new temporary email address</h1>

<form method="post">
    <table class="form-table">
        <tbody>
        <tr valign="top">
            <th scope="row">Enter temporary email address</th>
            <td width="200px">
                <div>
                    <input type="text" id="temp_address"/>

                </div>
            </td>
            <td width="20px">
                <input type="button" class="button button-primary" id="add_address" value="Add">
            </td>
            <td>
                <small hidden style="color:red" id="alert-danger">

                </small>
                <small hidden style="color:green" id="alert-success">

                </small>
            </td>
        </tr>
        </tbody>
    </table>

</form>
