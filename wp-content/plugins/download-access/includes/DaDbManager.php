<?php

class DaDbManager
{

    private $db;
    private $dbprefix;
    private $da_user_file_download;
    private $da_temporary_email_address;
    private $da_downloads_book;

    const  MINUTE=15;
    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
        $this->dbprefix = $wpdb->prefix;
        $this->da_user_file_download = $this->dbprefix . 'da_user_file_download';
        $this->da_temporary_email_address = $this->dbprefix . 'da_temporary_email_address';
        $this->da_downloads_book = $this->dbprefix . 'da_downloads_book';
    }

    function createTables()
    {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        if (!$this->isTableExists($this->da_user_file_download)) {
            $sql = "CREATE TABLE `" . $this->da_user_file_download . "`  ( `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `email` varchar(255),
                                  `hash` varchar(255),
                                  `type` varchar(10),
                                  `book_id` varchar(10),                                  
                                  `date` int(11) DEFAULT 0,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            dbDelta($sql);
        }

        if (!$this->isTableExists($this->da_temporary_email_address)) {
            $sql = "CREATE TABLE `" . $this->da_temporary_email_address . "`  ( `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `address` varchar(32),
                                  PRIMARY KEY (`id`)
                                ) ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            dbDelta($sql);
            $this->LoadTemprery("https://gist.github.com/adamloving/4401361.js");
        }

        if (!$this->isTableExists($this->da_downloads_book)) {
            $sql = "CREATE TABLE `" . $this->da_downloads_book . "`  ( `id` int(11) NOT NULL AUTO_INCREMENT,                                   
                                  `email` varchar(255),
                                  `book_id` int(11),
                                  `count` int(11),
                                  `date` int(11),
                                  PRIMARY KEY (`id`)
                                ) ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            dbDelta($sql);
        }
    }

    public function isTableExists($tableName)
    {
        return $this->db->get_var("SHOW TABLES LIKE '$tableName'") == $tableName;
    }

    public function LoadTemprery($url)
    {
        $re = '@<td.+?>([^<]+)<\\\\/td>@';
        $str = file_get_contents($url);
        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

        foreach ($matches as $match) {
            $this->insertTempreryAddress($match[1]);
        }
    }

    public function temprery($email)
    {
        $ad = explode("@", $email);
        $result = [];
        if (isset($ad[1])) {
            $result = $this->db->get_results("SELECT id FROM `" . $this->da_temporary_email_address . "` WHERE `address`='" . $ad[1] . "'");
        }
        return $result;
    }

    public function insertTempreryAddress($address)
    {
        $sql = $this->db->prepare("INSERT INTO  `" . $this->da_temporary_email_address . "` (`address`)
                      VALUES('%s')", $address);
        $this->db->query($sql);
    }

    public function tempreryExists($address)
    {
        $result = $this->db->get_results("SELECT * FROM `" . $this->da_temporary_email_address . "` WHERE `address`='" . trim($address) . "'");
        if (!empty($result[0])) {
            return true;
        }
        return false;
    }

    public function insertDownloadDetails($email, $id, $type, $hash)
    {
        $current_time = time();
        $next_time = self::MINUTE * 60;
        $duration_minute=self::MINUTE;
        $result = $this->db->get_results("SELECT * FROM `" . $this->da_user_file_download . "` WHERE `email`='" . $email . "' AND `book_id`=" . $id . " AND `type`='" . $type . "'");
        if (empty($result[0])) {
            $sql = $this->db->prepare("INSERT INTO  `" . $this->da_user_file_download . "` (`email`,`hash`,`type`,`book_id`,`date`)
                      VALUES('%s','%s','%s',$id,$current_time)", $email, $hash, $type);
            $this->db->query($sql);
            return ['inserted'=>true,"time"=>$duration_minute];
        } else {
            $duration=intval($current_time) - intval($result[0]->date);
            $duration_minute=self::MINUTE-floor($duration / 60);
            if ($duration >= $next_time) {
                if($duration_minute!=self::MINUTE){
                    $delete_sql = $this->db->prepare("DELETE FROM `" . $this->da_user_file_download . "` WHERE `id`=%d", $result[0]->id);
                    $this->db->query($delete_sql);
                    $sql = $this->db->prepare("INSERT INTO  `" . $this->da_user_file_download . "` (`email`,`hash`,`type`,`book_id`,`date`)
                      VALUES('%s','%s','%s',$id,$current_time)", $email, $hash, $type);
                    $this->db->query($sql);
                    return ['inserted'=>true,"time"=>$duration_minute];
                }
            }
            if($duration_minute==15){
                $duration_minute=1;
            }
            if(floor($duration / 60)==0){
                $duration_minute=15;
            }
            return ['inserted'=>false,"time"=>$duration_minute];
        }
    }

    public function downloads($id, $email)
    {
        if ($id && $email) {
            $curet_time = time();
            $result = $this->db->get_results("SELECT * FROM `" . $this->da_downloads_book . "` WHERE `email`='" . $email . "' AND `book_id`=" . $id);
            if (!empty($result[0])) {
                $updateData = array('count' => $result[0]->count + 1, 'date' => $curet_time);
                $this->db->update($this->da_downloads_book, $updateData, array('id' => $result[0]->id));
            } else {
                $sql = $this->db->prepare("INSERT INTO  `" . $this->da_downloads_book . "` (`email`,`book_id`, `count`,`date`)
                      VALUES('%s',$id,1,$curet_time)", $email);
                $this->db->query($sql);
            }
        }
    }

    public function deleteOldLiveViews($hash)
    {
        $sql = $this->db->prepare("DELETE FROM `" . $this->da_user_file_download . "` WHERE `hash`=%s", $hash);
        $this->db->query($sql);
    }

    public function getFromHash($hash, $type)
    {
        $result = $this->db->get_results("SELECT * FROM `" . $this->da_user_file_download . "` WHERE `hash`='" . $hash . "' AND `type`='" . $type . "'");
        $this->deleteOldLiveViews($hash);
        return $result;
    }

}