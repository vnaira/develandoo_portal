<?php

class mailHelper
{
    private $url;

    public function sendMail($email, $type, $hash)
    {
        if ($email) {
            $this->url = home_url('?action=download&hash=');
            $fullUrl = $this->url . $hash . "&type=" . $type;
            $headers_users = array();
            $content_type = apply_filters('wp_mail_content_type', 'text/html');
            $from_name = apply_filters('wp_mail_from_name', get_option('blogname'));
            $headers_users[] = "Content-Type:  $content_type; charset=UTF-8";
            $message_user = $this->getTemplate($fullUrl);
            $subject = "Your Ebook is ready";
            if ($email) {
                wp_mail($email, $subject, $message_user, $headers_users);
            }

        }
    }

    private function getTemplate($url)
    {
        return '
                 <table width="100%" align="center" >
                    <tr>
                        <td align="center">
                            <table bgcolor="#FFFFFF" width="100%" >
                                <tr>
                                    <td style="padding: 30px 20px" align="center">
                                        <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:center">Click the download button to get your ebook</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table border="0" cellpadding="0" cellspacing="0" width="200px" class="emailButton" style="background-color: #f2cd32;">
                                            <tr>
                                                <td align="center" valign="middle" class="buttonContent" style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
                                                    <a style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;" href="'.$url.'" target="_blank">Download </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 30px 0" align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" class="textContent">
                                                    <p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:center">or open the address below in your browser</p>
                                                    <div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>     
                                <tr>
                                    <td style="padding: 30px 0" align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" class="textContent">
                                                    <p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:center">'.$url.'</p>
                                                    <div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
        
        ';
    }

}