<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<style>
	.menu-item-125 a{
		color: #f2cd32 !important;font-size: 16px!important;
	}
	.site-footer li.menu-item-125 a:before{background: transparent!important;}
	.menu-item-125 > a:before{content: "";
		height: 1px;
		left: 0;
		margin: auto;
		position: absolute;
		right: 0;
		bottom: 0;
		width: 100%;background: #f2cd32 !important}
</style>
<div class="wpfl-1">
	<div class="wpforo-topichead col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="greyed col-lg-6 col-md-6 col-sm-6 col-xs-8"><?php wpforo_phrase( 'Topic title' ) ?></div>
		<div class="greyed col-lg-1 col-md-1 col-sm-1 col-xs-2 text-left"><?php wpforo_phrase( 'Posts' ) ?></div>
		<div class="greyed col-lg-1 col-md-1 col-sm-1 col-xs-2 text-left"><?php wpforo_phrase( 'Views' ) ?></div>
		<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs new_pos">
			<div class="row">
				<div class="aside margin30left">
					<div class="col-lg-12 col-md-12 col-sm-12 greyed text-center no-border">
						<p class="recent_title text-left">Recent topics</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php foreach ( $topics as $key => $topic ) : ?>

		<?php
		$member = $wpforo->member->get_member( $topic['userid'], true );
		if ( isset( $topic['last_post'] ) && $topic['last_post'] != 0 ) {
			$last_post   = $wpforo->post->get_post( $topic['last_post'] );
			$last_poster = ( ! empty( $last_post ) ) ? $wpforo->member->get_member( $last_post['userid'], true ) : array( 'ID'           => 0,
			                                                                                                              'display_name' => wpforo_phrase( 'Guest' )
			);
		}
		if ( isset( $topic['first_postid'] ) && $topic['first_postid'] != 0 ) {
			$first_post  = $wpforo->post->get_post( $topic['first_postid'] );
			$intro_posts = $wpforo->post_options['layout_extended_intro_posts_count'];
			if ( $intro_posts < 1 ) {
				$intro_posts = null;
			} else {
				$intro_posts = ( $intro_posts > 1 ) ? ( $intro_posts - 1 ) : $intro_posts = 0;
			}
			$first_poster = ( ! empty( $first_post ) ) ? $wpforo->member->get_member( $first_post['userid'], true ) : array( 'display_name' => wpforo_phrase( 'Guest' ) );
			$posts        = $wpforo->post->get_posts( array( 'topicid'   => $topic['topicid'],
			                                                 'exclude'   => $topic['first_postid'],
			                                                 'order'     => 'DESC',
			                                                 'row_count' => $intro_posts
			) );
			$posts        = array_reverse( $posts );
		}
		$topic_url   = $wpforo->topic->get_topic_url( $topic, $forum_data );
		$post_toglle = $wpforo->post_options['layout_extended_intro_posts_toggle'];
		?>

		<div class="forum-wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="wpforo-forum col-lg-8 col-md-8 col-sm-8 col-xs-12 new_pos no-border">

				<div class="wpforo-topic-info">
					<p class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 10px"><a
							href="<?php echo esc_url( $topic_url ) ?>"><?php echo esc_html( $topic['title'] ); ?></a>
					</p>
				</div>
				<div class="wpforo-topic-stat-views col-xs-2"><?php echo intval( $topic['views'] ) ?></div>
				<div class="wpforo-topic-stat-posts col-xs-1"><?php echo intval( $topic['posts'] ) ?></div>
				<div class="wpf-clear"></div>
			</div><!-- wpforo-topic -->
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wpforo-last-posts<?php echo intval( $topic['topicid'] ) ?>"
			     style="display: <?php echo( $post_toglle == 1 ? 'block' : 'none' ); ?>">
				<div class="aside margin15left greye">
					<ul>
						<li>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-left: -7px">&nbsp;
								<?php echo ltrim( esc_html( wpforo_text( $first_post['body'], 38, false ) ) ) ?>

							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-1 col-sm-1 col-xs-1 conv"></div>
								<div class="col-lg-10 col-sm-10 col-xs-10 text-left small_text">
									<div class="row">
										<?php wpforo_date( $post['created'] ); ?>
										<?php echo sprintf( wpforo_phrase( 'by %s', false ), esc_html( wpforo_text( $poster['display_name'], 9, false ) ) ) ?>
									</div>
								</div>

							</div>

						</li>
						<?php if ( ! empty( $posts ) && is_array( $posts ) ) : ?>
							<?php foreach ( $posts as $post ) : ?>
								<?php $poster = $wpforo->member->get_member( $post['userid'], true ); ?>
								<li>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<?php echo( ( $post['body'] ) ? esc_html( wpforo_text( $post['body'], 40, false ) ) : esc_html( $post['title'] ) ) ?>
									</div>

									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;
										<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 conv"></div>
										<div class="col-lg-10 col-md-10 col-sm-10 col-s-10 text-left small_text">
											<div class="row">
												<?php wpforo_date( $post['created'] ); ?>
												<?php echo sprintf( wpforo_phrase( 'by %s', false ), esc_html( wpforo_text( $poster['display_name'], 9, false ) ) ) ?>
											</div>
										</div>

									</div>
								</li>
							<?php endforeach ?>
							<?php if ( intval( $topic['posts'] ) > ( $intro_posts -1 ) ): ?>
								<li style="text-align:right; margin-right: 30px; padding-bottom: 10px">

									<a
										href="<?php echo esc_url( $topic_url ) ?>"><?php wpforo_phrase( 'view all posts', true, 'lower' ); ?>
									</a>
									<br/>
								</li>
								<br/>
							<?php endif ?>
						<?php endif ?>
					</ul>
				</div><!-- wpforo-last-posts-list -->
			</div><!-- wpforo-last-posts -->
		</div>

		<?php do_action( 'wpforo_loop_hook', $key ) ?>

	<?php endforeach; ?>
</div> <!-- wpfl-1 -->
