<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *
 * @layout: Extended
 * @url: http://gvectors.com/
 * @version: 1.0.0
 * @author: gVectors Team
 * @description: Extended layout displays one level deeper information in advance.
 *
 */
?>

<div class="wpfl-1">
	<div class="wpforo-category col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="cat-title1 greyed col-lg-6 col-md-6 col-sm-6 co-xs-6"><?php echo esc_html( $cat['title'] ); ?></div>
		<div
			class="cat-stat-posts1 greyed col-lg-1 col-md-1 col-sm-1 co-xs-1 text-right"><?php wpforo_phrase( 'Topics' ); ?></div>
		<div
			class="cat-stat-topics1 greyed col-lg-1 col-md-1 col-sm-1 co-xs-1 text-center"><?php wpforo_phrase( 'Posts' ); ?></div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 cat-title1">
			<div class="row">
				<div class="aside margin30left">
					<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs greyed text-center new_pos no-border">
						<p class="recent_title text-left">Recent topics</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- wpforo-category -->

	<?php foreach ( $forums as $forum ) :
		if ( ! $wpforo->perm->forum_can( $forum['forumid'], 'vf' ) ) {
			continue;
		}
		$sub_forums     = $wpforo->forum->get_forums( array( "parentid" => $forum['forumid'], "type" => 'forum' ) );
		$has_sub_forums = ( is_array( $sub_forums ) && ! empty( $sub_forums ) ? true : false );

		$data = array();
		$wpforo->forum->get_childs( $forum['forumid'], $data );
		$counts = $wpforo->forum->get_counts( $data );
		$topics = $wpforo->topic->get_topics( array( "forumids"  => $data,
		                                             "orderby"   => "type, modified",
		                                             "order"     => "DESC",
		                                             "row_count" => $wpforo->forum_options['layout_extended_intro_topics_count']
		) );

		$has_topics = ( is_array( $topics ) && ! empty( $topics ) ? true : false );

		$forum_url    = $wpforo->forum->get_forum_url( $forum );
		$topic_toglle = $wpforo->forum_options['layout_extended_intro_topics_toggle'];
		?>
		<div class="forum-wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="wpforo-forum col-lg-8 col-md-8 col-sm-8 col-xs-12 new_po">
				<div class="wpforo-forum-info col-lg-8 col-md-8 col-sm-8">
					<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a
							href="<?php echo esc_url( $forum_url ) ?>"><?php echo esc_html( $forum['title'] ); ?></a>
					</h3>
					<!--                    <p class="wpforo-forum-description">-->
					<?php //echo esc_html( wpforo_text( $forum['description'], 1000, false ));
					?><!--</p>-->

					<?php if ( $has_sub_forums ) : ?>

						<div class="wpforo-subforum">
							<ul>
								<li class="first wpfcl-2"><?php wpforo_phrase( 'Subforums' ); ?>:</li>

								<?php foreach ( $sub_forums as $sub_forum ) :
									if ( ! $wpforo->perm->forum_can( $sub_forum['forumid'], 'vf' ) ) {
										continue;
									} ?>

									<li><i class="fa fa-comments wpfcl-0"></i>&nbsp;<a
											href="<?php echo esc_url( $wpforo->forum->get_forum_url( $sub_forum ) ) ?>"><?php echo esc_html( $sub_forum['title'] ); ?></a>
									</li>

								<?php endforeach; ?>

							</ul>
							<br class="wpf-clear"/>
						</div><!-- wpforo-subforum -->

					<?php endif; ?>
				</div><!-- wpforo-forum-info -->

				<div
					class="wpforo-forum-stat-posts text-center col-lg-2 col-md-2 col-sm-2 col-xs-1"><?php echo wpforo_print_number( $counts['topics'] ) ?></div>
				<div
					class="wpforo-forum-stat-topics text-center col-lg-1 col-md-1 col-sm-1 col-xs-2"><?php echo wpforo_print_number( $counts['posts'] ) ?></div>

				<br class="wpf-clear"/>
			</div><!-- wpforo-forum -->

			<?php if ( $has_topics ) : ?>

				<div
					class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wpforo-last-topics<?php echo intval( $forum['forumid'] ) ?>"
					style="display: <?php echo( $topic_toglle == 1 ? 'block' : 'none' ) ?>;">
					<div class="aside margin22left greye">
						<ul>
							<?php
							foreach ( $topics as $topic ) :
								 ?>
									<?php $last_post = $wpforo->post->get_post( $topic['last_post'] ); ?>
									<?php $member = $wpforo->member->get_member( $last_post['userid'], true ); ?>
									<li>
										<div class="wpforo-last-topic-title"
										     title="<?php $icon_title = $wpforo->tpl->icon( 'topic', $topic, false, 'title' );
										     if ( $icon_title )
											     echo esc_html( $icon_title ) ?>">

											<?php echo esc_html( wpforo_text( $topic['title'], 36, false ) ) ?>

										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 conv"></div>
											<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 text-left small_text">
												<div class="row">
													<?php wpforo_date( $topic['modified'] ); ?>
													<?php wpforo_phrase( 'by ' ); ?>
													<?php echo esc_html( wpforo_text( $member['display_name'], 9, false ) ) ?></a>
												</div>
											</div>
										</div>
										<div class="wpf-clear"></div>
									</li>
									<?php
							endforeach;
							?>
							<?php if ( intval( $forum['topics'] ) > $wpforo->forum_options['layout_extended_intro_topics_count'] ): ?>
								<li>
									<div class="wpforo-last-topic-user wpf-vat">
										<a href="<?php echo esc_url( $forum_url ) ?>"><?php wpforo_phrase( 'view all topics', true, 'lower' ); ?>
											<i class="fa fa-angle-right" aria-hidden="true"></i></a>
									</div>
									<br class="wpf-clear"/>
								</li>
							<?php endif ?>
						</ul>
					</div>
					<br class="wpf-clear"/>
				</div><!-- wpforo-last-topics -->

			<?php endif; ?>

		</div><!-- forum-wrap -->

	<?php endforeach; ?> <!-- $forums as $forum -->
</div><!-- wpfl-1 -->