<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<style>
	.menu-item-125 a{
		color: #f2cd32 !important;font-size: 16px!important;
	}
	.site-footer li.menu-item-125 a:before{background: transparent!important;}
	.menu-item-125 > a:before{content: "";
		height: 1px;
		left: 0;
		margin: auto;
		position: absolute;
		right: 0;
		bottom: 0;
		width: 100%;background: #f2cd32 !important}
</style>
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
	<div class="row">
		<?php

		foreach ( $posts as $key => $post ) : ?>

			<?php $member = $wpforo->member->get_member( $post['userid'], true ); ?>
			<div id="post-<?php echo intval( $post['postid'] ) ?>" class="post-wrap">
				<div class="wpforo-post wpfcl-1">

					<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12" style="margin-top: 20px">
						<?php if ( wpforo_feature( 'avatars', $wpforo ) ): ?>
							<div
								class="author-avata col-lg-1 col-md-2 col-sm-2 col-xs-6"><?php echo $wpforo->member->avatar( $member, 'alt="' . esc_attr( $member['display_name'] ) . '"', 80, true ) ?></div>
						<?php endif ?>
						<div class="author-dat col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="author-name">
									<!--						<span>-->
									<?php //$wpforo->member->show_online_indicator($member['userid']) ?><!--</span>&nbsp;-->
									<a href="<?php echo esc_url( $member['profile_url'] ) ?>"><?php echo esc_html( $member['display_name'] ) ?></a>
								</div>
							</div>
							<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
								<div class="author-title">
									<?php wpforo_member_title( $member ) ?>
								</div>
							</div>
							<div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 text-right">
								<div class="row">

									<span class="slike sicons"></span>
									<?php

									$count = ps_count_of_likes( $member['userid'] );
									echo $count[0]->countl ?>

									<span class="sicons_cont"><span
											class="scomments sicons"></span> <?php echo intval( $member['posts'] ) ?>
										posts</span>

									<span class="sicons_cont"><span
											class="stime sicons"></span><?php wpforo_date( $member['user_registered'] ); ?></span>

								</div>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
								<div class="row">
									<div class="wpforo-post-content_ col-lg-12 col-md-12 col-sm-12">
										<?php echo wpforo_kses( $post['body'], 'post' ) ?>
									</div>
								</div><!-- right -->
							</div>
							<?php $buttons = array( 'reply', 'like' );
							$wpforo->tpl->buttons( $buttons, $forum_data['forumid'], $topic['topicid'], $post['postid'] ); ?>

						</div>
						<div class="wpf-clear"></div>
					</div><!-- left -->

				</div><!-- wpforo-post -->

			</div><!-- post-wrap -->

			<?php do_action( 'wpforo_loop_hook', $key ) ?>

		<?php endforeach; ?>
	</div>
</div><!-- wpfl-1 -->
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 wpforo-post-content_">
	<div class="col-lg-12 col-md-12 col-xs-12">
		<p class="recent_title">Recent topics</p>
		<div class="row">
			<div class="post_recent">
				<?php
				//			$limit = count($posts) - 6;
				//			for ($i = count($posts); $i> $limit; $i--){
				//
				//			}

				foreach ( $posts as $post ) {
					?>
					<?php $poster = $wpforo->member->get_member( $post['userid'], true ); ?>
					<p>
						<!--					<a href="-->
						<?php //echo esc_url( $wpforo->post->get_post_url( $post['postid'] ) ) ?><!--">-->
					<div class="wpforo-last-post-title">&nbsp;
						<?php echo( ( $post['body'] ) ? esc_html( wpforo_text( $post['body'], 37, false ) ) : esc_html( $post['title'] ) ) ?>

					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 conv"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-s-10 text-left small_text">

							<div class="row">
								<?php wpforo_date( $post['created'] ); ?>
								<?php echo sprintf( wpforo_phrase( 'by %s', false ), esc_html( wpforo_text( $poster['display_name'], 15, false ) ) ) ?>
							</div>
						</div>
					</div>

					</p>

				<?php }
				?>
			</div>
		</div>
	</div>
</div>