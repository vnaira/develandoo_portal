<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $_GET['wpfpaged'] ) ) {
	$paged = intval( $_GET['wpfpaged'] );
}

$args = array(
	'offset'    => ( $paged - 1 ) * $wpforo->post_options['posts_per_page'],
	'row_count' => $wpforo->post_options['posts_per_page'],
);

if ( ! empty( $_GET['wpfs'] ) ) {
	$args['needle'] = sanitize_text_field( $_GET['wpfs'] );
}
if ( ! empty( $_GET['wpff'] ) ) {
	$args['forumids'] = $_GET['wpff'];
}
if ( ! empty( $_GET['wpfd'] ) ) {
	$args['date_period'] = sanitize_text_field( $_GET['wpfd'] );
}
if ( ! empty( $_GET['wpfin'] ) ) {
	$args['type'] = sanitize_text_field( $_GET['wpfin'] );
}
if ( ! empty( $_GET['wpfob'] ) ) {
	$args['orderby'] = sanitize_text_field( $_GET['wpfob'] );
}
if ( ! empty( $_GET['wpfo'] ) ) {
	$args['order'] = sanitize_text_field( $_GET['wpfo'] );
}

$items_count = 0;
$posts       = $wpforo->post->search( $args, $items_count );
$wpfs        = ( isset( $_GET['wpfs'] ) ) ? sanitize_text_field( $_GET['wpfs'] ) : '';
?>


<p id="wpforo-search-title" class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><?php wpforo_phrase( 'Search result for' ) ?>:
	<span class="wpfcl-5"><?php echo esc_html( $wpfs ) ?></span></p>

<div class="wpforo-search-wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<!--   here was wpf-search-bar-->

	<hr/>
	<div class="wpf-snavi"><?php $wpforo->tpl->pagenavi( $paged, $items_count, false ); ?></div>
	<div class="wpforo-search-content">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
		       class="table table-striped table-responsive table-hover">
			<tr class="warning" style="background: #eaeaea">
				<td class="wpf-shead-title"><?php wpforo_phrase( 'Post Title' ) ?></td>
				<td class="wpf-shead-date"><?php wpforo_phrase( 'Date' ) ?></td>
				<td class="wpf-shead-user"><?php wpforo_phrase( 'User' ) ?></td>
				<td class="wpf-shead-forum"><?php wpforo_phrase( 'Forum' ) ?></td>
			</tr>

			<?php foreach ( $posts as $post ) : extract( $post, EXTR_OVERWRITE ); ?>

				<tr class="wpf-ttr">
					<td class="wpf-spost-title border_bottom"><a
							href="<?php echo cut_string( esc_url( $wpforo->post->get_post_url( $postid ) ) ) ?>"
							title="<?php wpforo_phrase( 'View entire post' ) ?>"><?php echo esc_html( $title ) ?> &nbsp;<i
								class="fa fa-chevron-right" style="font-weight:100; font-size:11px;"></i></a></td>
					<td class="wpf-spost-date border_bottom"><?php wpforo_date( $created ); ?></td>
					<td class="wpf-spost-user border_bottom"><?php $user = $wpforo->member->get_member( $userid, true );
						echo( $user['display_name'] ? esc_html( $user['display_name'] ) : esc_html( $user['user_nicename'] ) ); ?></td>
					<td class="wpf-spost-forum border_bottom"><?php $forum = $wpforo->forum->get_forum( $forumid );
						echo esc_html( $forum['title'] ); ?></td>
				</tr>
				<tr class="wpf-ptr">
					<td colspan="5" class="wpf-stext border_bottom">
						<?php
						$body = wpforo_content_filter( $body );
						$body = preg_replace( '#\[attach\][^\[\]]*\[\/attach\]#is', '', strip_tags( $body ) );
						if ( ! empty( $_GET['wpfs'] ) ) {
							$words = explode( ' ', trim( $_GET['wpfs'] ) );
							if ( ! empty( $words ) ) {
								$body_len = 564;
								$pos      = mb_stripos( $body, " " . trim( $words[0] ), 0, get_option( 'blog_charset' ) );
								if ( strlen( $body ) > $body_len && $pos !== false ) {
									if ( $pos > ( $body_len / 2 ) ) {
										$bef_body = "... ";
										$start    = mb_stripos( $body, " ", ( $body_len / 2 ), get_option( 'blog_charset' ) );;
									} else {
										$bef_body = "";
										$start    = 0;
									}
									if ( ( mb_strlen( $body, get_option( 'blog_charset' ) ) - $start ) > $body_len ) {
										$aft_body = " ...";
									} else {
										$aft_body = "";
									}
									$body = $bef_body . mb_substr( $body, $start, $body_len, get_option( 'blog_charset' ) ) . $aft_body;
								}
								foreach ( $words as $word ) {
									$word = trim( $word );
									$body = str_ireplace( ' ' . esc_html( $word ), ' <span class="sword wpfcl-b">' . esc_html( $word ) . '</span>', $body );
								}
							}
						}
						echo $body;
						?>
					</td>
				</tr>

			<?php endforeach ?>

		</table>
	</div>
	<div class="wpf-snavi"><?php $wpforo->tpl->pagenavi( $paged, $items_count, false ); ?></div>
</div>

<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>